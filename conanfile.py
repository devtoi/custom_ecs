from conan import ConanFile
from conan.tools.cmake import CMakeToolchain


class CECS(ConanFile):
    settings = "os", "compiler", "build_type", "arch"
    generators = "CMakeDeps"

    def requirements(self):
        self.requires("glm/cci.20230113")
        self.requires("nlohmann_json/3.11.2")
        self.requires("sdl/2.26.1")

    def generate(self):
        tc = CMakeToolchain(self, generator="Ninja")
        tc.generate()

    def build(self):
        cmake = CMake(self)
        cmake.configure()
        cmake.build()

    # def build_requirements(self):
    #     self.tool_requires("cmake/3.22.6")f.copy("implot.cpp", dst="../include/imgui", src="res/bindings")
    default_options = {"sdl/*:pulse": False}
