#!/bin/sh

export SYSTEM_NAME=$1
export SYSTEM_NAME_SNAKE_CASE=`echo ${SYSTEM_NAME} | sed 's/\([A-Z]\)/_\L\1/g;s/^_//'`
export SYSTEM_H_FILE_NAME=${SYSTEM_NAME_SNAKE_CASE}_system.h
export SYSTEM_CPP_FILE_NAME=${SYSTEM_NAME_SNAKE_CASE}_system.cpp
export SYSTEM_NAME=${1}System
export SYSTEM_FILE_LOCATION=$2
GIT_ROOT=`git rev-parse --show-toplevel`
export SYSTEM_HEADER_FILE_LOCATION=${GIT_ROOT}/src/ecs/system/${SYSTEM_FILE_LOCATION}/${SYSTEM_H_FILE_NAME}
export SYSTEM_CPP_FILE_LOCATION=${GIT_ROOT}/src/ecs/system/${SYSTEM_FILE_LOCATION}/${SYSTEM_CPP_FILE_NAME}
CMAKE_FILE=${GIT_ROOT}/CMakeLists.txt
echo "Writing header file to ${SYSTEM_HEADER_FILE_LOCATION}"
echo "Writing cpp file to ${SYSTEM_CPP_FILE_LOCATION}"
envsubst < ${GIT_ROOT}/file_templates/system.h.template > ${SYSTEM_HEADER_FILE_LOCATION}
envsubst < ${GIT_ROOT}/file_templates/system.cpp.template > ${SYSTEM_CPP_FILE_LOCATION}
SYSTEM_HEADER_FILE_LOCATION=`echo ${SYSTEM_HEADER_FILE_LOCATION} | sed "s#${GIT_ROOT}/##"`
SYSTEM_CPP_FILE_LOCATION=`echo ${SYSTEM_CPP_FILE_LOCATION} | sed "s#${GIT_ROOT}/##"`
sed -i "s#AUTO INSERT SYSTEM HERE#AUTO INSERT SYSTEM HERE\n\t\"${SYSTEM_HEADER_FILE_LOCATION}\"\n\t\"${SYSTEM_CPP_FILE_LOCATION}\"#" ${CMAKE_FILE}

SYSTEM_HEADER_FILE_LOCATION=`echo ${SYSTEM_HEADER_FILE_LOCATION} | sed "s#src/##"`
sed -i "s#AUTO INSERT SYSTEM HERE#AUTO INSERT SYSTEM HERE\n\#include \<${SYSTEM_HEADER_FILE_LOCATION}\>#" ${GIT_ROOT}/src/ecs/system/core/menu_system.cpp
sed -i "s#AUTO INSERT SYSTEM CLASS HERE#AUTO INSERT SYSTEM CLASS HERE\n\t\tpCollection->AddSystem<${SYSTEM_NAME}>(SystemsBank);#" ${GIT_ROOT}/src/ecs/system/core/menu_system.cpp
		
