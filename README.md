# Guiding principles
* Optimize iteration performance
* Multi threading
* Existence based
* Archetypes from data or script
* Performance very important
* Relatively easy to use
* Dependency tree possible
* Multiple worlds possible

