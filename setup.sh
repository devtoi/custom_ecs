#!/bin/sh
mkdir build
conan install conanfile.py -s build_type=Debug --build=missing --output-folder=build
cd build
cmake --preset=conan-debug ..
cd -
