#version 450
#extension GL_ARB_separate_shader_objects : enable

layout(location = 0) in vec2 inPosition;
layout(location = 1) in vec3 inColor;

layout(location = 0) out vec3 fragColor;

layout(binding = 0) uniform MapData {
    int MAP_WIDTH_PIXELS;
    int MAP_HEIGHT_PIXELS;
} mapData;

void main() {
	vec2 screenPos = vec2( ((2 * inPosition.x) / mapData.MAP_WIDTH_PIXELS) - 1.0f, ( (2 * inPosition.y) / mapData.MAP_HEIGHT_PIXELS ) - 1.0f );
    gl_Position = vec4(screenPos, 0.0, 1.0);
    fragColor = inColor;
}
