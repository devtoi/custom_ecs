#version 450
#extension GL_ARB_separate_shader_objects : enable

layout(location = 0) in vec2 inPosition;
layout(location = 1) in vec3 inColor;

layout(location = 0) out vec4 outColor;

layout(binding = 0) uniform Camera {
	vec2 Offset;
	float Zoom;
	float padding;
} camera;

void main() {
	vec2 screenPos = vec2( ((camera.Zoom * 2 * (inPosition.x - camera.Offset.x)) / 1600.0f) - 1.0f , ( ( camera.Zoom * 2 * (inPosition.y - camera.Offset.y)) / 900.0f ) - 1.0f );
	//vec2 screenPos = vec2( (( 2 * inPosition.x) / 1600.0f) - 1.0f , ( ( 2 * inPosition.y) / 900.0f ) - 1.0f );
	gl_Position = vec4(screenPos, 0.0, 1.0);
//	UV = vec2((screenPos.x + 1.0f) * 0.5f, (screenPos.y + 1.0f ) * 0.5f );
	outColor = vec4(inColor, 1.0f);
}
