#version 450
#extension GL_ARB_separate_shader_objects : enable

layout(location = 0) in vec2 inUV;

layout(location = 0) out vec4 outColor;

layout(binding = 0) uniform sampler2D texSampler;

void main() {
	vec4 clr = texture(texSampler, inUV);
	outColor = clr;
//	outColor = mix(clr, vec4(inUV.x, inUV.y, 0, 1), 0.5f );
	//outColor = vec4(1 - clr.r, 1 - clr.g, 1 - clr.b, 1.0f);
	//outColor = vec4(1.0f);
}
