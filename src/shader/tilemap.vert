#version 450
#extension GL_ARB_separate_shader_objects : enable

layout(location = 0) in vec2 inPosition;
layout(location = 1) in vec2 inUV;

layout(location = 0) out vec2 outUV;

layout( push_constant ) uniform constants
{
	vec2 Offset;
	float Zoom;
	float padding;
} camera;

//layout(binding = 0) uniform Camera {
//	vec2 Offset;
//	float Zoom;
//	float padding;
//} camera;

void main() {
	vec2 screenPos = vec2( ((camera.Zoom * 2 * (inPosition.x - camera.Offset.x)) / 1600.0f) - 1.0f , ( ( camera.Zoom * 2 * (inPosition.y - camera.Offset.y)) / 900.0f ) - 1.0f );
	//vec2 screenPos = vec2( (( 2 * inPosition.x) / 1600.0f) - 1.0f , ( ( 2 * inPosition.y) / 900.0f ) - 1.0f );
	gl_Position = vec4(screenPos, 0.0, 1.0);
	outUV = inUV;
}
