#version 450
#extension GL_ARB_separate_shader_objects : enable

layout(location = 0) in vec4 inColor;

layout(location = 0) out vec4 outColor;

//layout(binding = 1) uniform sampler2D texSampler;

void main() {
	//vec4 clr = texture(texSampler, UV);
	//outColor = vec4(1 - clr.r, 1 - clr.g, 1 - clr.b, 1.0f);
	//outColor = vec4(UV.r, UV.g, 0, 1);
//	outColor = vec4(1.0f);
	outColor = inColor;
}
