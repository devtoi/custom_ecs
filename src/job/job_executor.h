#pragma once
#include <thread>
#include "job.h"
#include "work_steal_queue.h"

class JobEngine;

class JobExecutor
{
public:
	enum class Mode
	{
		Background,
		Foreground
	};

	enum class State
	{
		Idle,
		Running,
		Stopping
	};

	using JobQueue = WorkStealQueue<Job, 4096>;
	JobExecutor(JobEngine& jobEngine, Mode mode);

	void Start();
	void Stop();
	bool IsRunning() const;
	void AddJob(Job* job);
	void WaitForJob(Job* waitJob);
	std::thread::id GetThreadID() const;

private:
	Job* GetJob();
	void JobExection();

	JobEngine& m_jobEngine;
	std::thread::id m_threadID;
	std::atomic<Mode> m_mode;
	std::atomic<State> m_state;
	JobQueue m_queue;
	std::thread m_thread;
};