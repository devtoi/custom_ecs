#include "job_executor.h"
#include "job_engine.h"
#include <chrono>

JobExecutor::JobExecutor(JobEngine& jobEngine, Mode mode)
	: m_jobEngine{ jobEngine }, m_threadID{ std::this_thread::get_id() }, m_mode{ mode },
	  m_state{ JobExecutor::State::Idle }
{
}

void JobExecutor::Start()
{
	m_state.store(JobExecutor::State::Running);
	if (m_mode == JobExecutor::Mode::Background)
	{
		m_thread = std::thread([this] { JobExection(); });
	}
}

void JobExecutor::Stop()
{
	m_state.store(JobExecutor::State::Stopping);
	if (m_mode == JobExecutor::Mode::Background)
	{
		// while (!m_thread.joinable())
		for (int i = 0; i < 100; i++)
		{
			m_jobEngine.AccessActiveJobsSemaphore().release();
		}
		m_thread.join();
	}
	int i = 0;
}

bool JobExecutor::IsRunning() const
{
	return m_state == JobExecutor::State::Running;
}

void JobExecutor::AddJob(Job* job)
{
	m_queue.Push(job);
	m_jobEngine.AccessActiveJobsSemaphore().release();
}

void JobExecutor::WaitForJob(Job* waitJob)
{
	while (!waitJob->IsFinished())
	{
		Job* job = GetJob();

		if (job != nullptr)
		{
			job->Run();
		}
		else
		{
			std::this_thread::yield();
		}
	}
}

Job* JobExecutor::GetJob()
{
	Job* job = m_queue.Pop();
	if (job != nullptr)
	{
		return job;
	}
	// Empty Queue or pop failed. Steal job

	JobExecutor* toStealFrom = m_jobEngine.GetRandomWorker();
	if (toStealFrom && toStealFrom != this)
	{
		Job* stolenJob = toStealFrom->m_queue.Steal();
		if (stolenJob)
		{
			return stolenJob;
		}

		std::this_thread::yield();
		return nullptr;
	}

	std::this_thread::yield();
	return nullptr;
}

void JobExecutor::JobExection()
{
	size_t NumberOfFails = 0;
	while (IsRunning())
	{
		Job* job = GetJob();
		if (job)
		{
			job->Run();
		}
		else if (NumberOfFails > 10)
		{
			using namespace std::chrono_literals;
			m_jobEngine.AccessActiveJobsSemaphore().try_acquire_for(1ms);
			NumberOfFails = 0;
		}
		else
		{
			NumberOfFails++;
			std::this_thread::yield();
		}
	}
}

std::thread::id JobExecutor::GetThreadID() const
{
	return m_threadID;
}
