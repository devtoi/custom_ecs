#include "job.h"

Job::Job(Job::Function jobFunction, Job* parent) : m_function{ jobFunction }, m_parent{ parent }, m_unfinishedJobs{ 1 }
{
	// Make sure parent is able to wait for us to finish
	if (parent != nullptr)
		parent->m_unfinishedJobs++;
}

void Job::Run()
{
	m_function(*this);
	Finish();
}

bool Job::IsFinished() const
{
	return m_unfinishedJobs == 0;
}

void Job::Finish()
{
	// Tell parent we are finished
	if (m_unfinishedJobs == 1 && m_parent != nullptr)
		m_parent->Finish();
	m_unfinishedJobs--;
}
