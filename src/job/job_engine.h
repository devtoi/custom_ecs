#pragma once
#include <cstdint>
#include <random>
#include <vector>
#include <thread>
#include <semaphore>
#include "job_executor.h"

class JobEngine
{
public:
	static JobEngine& GetInstance();

	JobEngine();
	~JobEngine();

	JobExecutor* GetRandomWorker();
	JobExecutor* GetForegroundExecutor();
	JobExecutor* GetThreadWorker();
	size_t GetNumberOfBackgroundWorkers() const noexcept;
	size_t GetNumberOfWorkers() const noexcept;
	std::counting_semaphore<1000>& AccessActiveJobsSemaphore();

private:
	std::vector<std::unique_ptr<JobExecutor>> m_jobExecutors;
	std::counting_semaphore<1000> _ActiveJobsSemaphore{ 0 };
};
