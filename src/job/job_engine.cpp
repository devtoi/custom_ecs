#include "job_engine.h"

const unsigned int NR_OF_EXECUTORS = std::thread::hardware_concurrency();

thread_local std::random_device t_randomDevice{};
thread_local std::mt19937 t_randomGenerator{ t_randomDevice() };
thread_local std::uniform_int_distribution<unsigned int> t_randomExecutorDistribution{ 0, NR_OF_EXECUTORS - 1 };

JobEngine& JobEngine::GetInstance()
{
	static JobEngine jobEngine;
	return jobEngine;
}

JobEngine::JobEngine()
{
	m_jobExecutors.emplace_back(std::make_unique<JobExecutor>(*this, JobExecutor::Mode::Foreground));
	for (size_t i = 1; i < NR_OF_EXECUTORS; ++i)
	{
		m_jobExecutors.emplace_back(std::make_unique<JobExecutor>(*this, JobExecutor::Mode::Background));
	}

	for (auto& jobExecutor : m_jobExecutors)
	{
		jobExecutor->Start();
	}
}

JobEngine::~JobEngine()
{
	for (auto& jobExecutor : m_jobExecutors)
	{
		jobExecutor->Stop();
	}
}

JobExecutor* JobEngine::GetRandomWorker()
{
	if (m_jobExecutors.size() != 0)
	{
		size_t rndIndex = t_randomExecutorDistribution(t_randomGenerator);
		assert(rndIndex < m_jobExecutors.size());
		JobExecutor* jobExecutor = m_jobExecutors.at(rndIndex).get();
		if (jobExecutor->IsRunning())
			return jobExecutor;
		return nullptr;
	}
	return nullptr;
}

JobExecutor* JobEngine::GetForegroundExecutor()
{
	return m_jobExecutors.at(0).get();
}

JobExecutor* JobEngine::GetThreadWorker()
{
	std::thread::id tid = std::this_thread::get_id();
	for (auto& je : m_jobExecutors)
	{
		if (je->GetThreadID() == tid)
		{
			return je.get();
		}
	}
	return nullptr;
}

size_t JobEngine::GetNumberOfBackgroundWorkers() const noexcept
{
	return m_jobExecutors.size() - 1;
}

size_t JobEngine::GetNumberOfWorkers() const noexcept
{
	return m_jobExecutors.size();
}

std::counting_semaphore<1000>& JobEngine::AccessActiveJobsSemaphore()
{
	return _ActiveJobsSemaphore;
}
