#pragma once
#include <functional>
#include <array>
#include <algorithm>
#include <atomic>
#include <cstdint>
#include <cstddef>
#include <cstring>
#include <mutex>
#include <new>

#ifdef __cpp_lib_hardware_interference_size
using std::hardware_constructive_interference_size;
using std::hardware_destructive_interference_size;
#else
// Lucky guess ? __cacheline_aligned ? L1_CACHE_BYTES ? L1_CACHE_SHIFT ? ...
constexpr std::size_t hardware_constructive_interference_size = 2 * sizeof(std::max_align_t);
constexpr std::size_t hardware_destructive_interference_size = 2 * sizeof(std::max_align_t);
#endif

class Job
{
public:
	using Function = std::function<void(Job&)>;

	Job() = default;
	Job(Function jobFunction, Job* parent = nullptr);
	template <typename Data>
	Job(Function jobFunction, const Data& data, Job* parent = nullptr)
		: Job{ jobFunction, parent }
	{
		setData(data);
	}

	template <typename Data>
	const Data& getData() const
	{
		return *reinterpret_cast<const Data*>(m_padding.data());
	}

	void Run();
	bool IsFinished() const;

private:
	void Finish();

	Function m_function;
	Job* m_parent = nullptr;
	std::atomic_size_t m_unfinishedJobs;
	static constexpr std::size_t JOB_PAYLOAD_SIZE = sizeof(Function) + sizeof(Job*) + sizeof(std::atomic_size_t);

	static constexpr std::size_t JOB_MAX_PADDING_SIZE = hardware_destructive_interference_size;
	static constexpr std::size_t JOB_PADDING_SIZE = JOB_MAX_PADDING_SIZE >= JOB_PAYLOAD_SIZE ? JOB_MAX_PADDING_SIZE - JOB_PAYLOAD_SIZE : 0;

	std::array<unsigned char, JOB_PADDING_SIZE> m_padding;

public:
	template <typename Data>
	std::enable_if_t<
		std::is_pod<Data>::value &&
		(sizeof(Data) <= JOB_PADDING_SIZE)>
	setData(const Data& data)
	{
		std::memcpy(m_padding.data(), &data, sizeof(Data));
	}
};
