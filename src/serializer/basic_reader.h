#pragma once

#include <string.h>
#include "serializer_base.h"

class BasicReader : public SerializerBase
{
public:
	BasicReader(const char* bufferToCopy, size_t sizeOfBufferToCopy);
	~BasicReader();

	bool is_reader() const override;
	bool is_writer() const override;

	void serialize(std::string_view key, uint16_t& i) override;
	void serialize(std::string_view key, uint32_t& i) override;
	void serialize(std::string_view key, uint64_t& i) override;

	void serialize(std::string_view key, int16_t& i) override;
	void serialize(std::string_view key, int32_t& i) override;
	void serialize(std::string_view key, int64_t& i) override;
	void serialize(std::string_view key, fpm::fixed_16_16& f) override;
	void serialize(std::string_view key, fpm::fixed_8_24& f) override;
	void serialize(std::string_view key, float& f) override;
	void serialize(std::string_view key, glm::vec2& v) override;
	void serialize(std::string_view key, glm::ivec2& v) override;
	void serialize(std::string_view key, bool& b) override;

private:
	template <typename T> void Memcpy_deserialize(T& dest)
	{
		memcpy(&dest, walker, sizeof(dest));
		IncreaseWalker(dest);
	}

	template <typename T> void IncreaseWalker(const T&)
	{
		walker += sizeof(T);
	}

	char* buffer = nullptr;
	char* walker = nullptr;
};
