#pragma once

#include <nlohmann/json.hpp>

#include <serializer/serializer_base.h>
#include <cstring>

class JsonTextReader : public SerializerBase
{
public:
	JsonTextReader(const nlohmann::json& Json);
	JsonTextReader(const JsonTextReader&) = default;
	JsonTextReader(JsonTextReader&&) = delete;
	JsonTextReader& operator=(const JsonTextReader&) = delete;
	JsonTextReader& operator=(JsonTextReader&&) = delete;
	virtual ~JsonTextReader() = default;

	const char* GetBuffer() const;
	size_t GetBufferSize() const;

	bool is_reader() const override;
	bool is_writer() const override;

	void push_object(std::string_view key, size_t sizeOfType) override;
	void push_element_object(size_t sizeOfType) override;
	void pop_object() override;
	void push_array(std::string_view key, size_t& count, size_t sizeOfElement) override;
	void pop_array() override;

	void serialize_variable_element(uint16_t& i) override;
	void serialize_variable_element(uint32_t& i) override;
	void serialize_variable_element(uint64_t& i) override;
	void serialize_variable_element(int16_t& i) override;
	void serialize_variable_element(int32_t& i) override;
	void serialize_variable_element(int64_t& i) override;
	void serialize_variable_element(fpm::fixed_16_16& f) override;
	void serialize_variable_element(fpm::fixed_8_24& f) override;
	void serialize_variable_element(float& f) override;
	void serialize_variable_element(glm::vec2& v) override;
	void serialize_variable_element(glm::ivec2& v) override;
	void serialize_variable_element(bool& b) override;

	void serialize_variable(std::string_view key, uint16_t& i) override;
	void serialize_variable(std::string_view key, uint32_t& i) override;
	void serialize_variable(std::string_view key, uint64_t& i) override;
	void serialize_variable(std::string_view key, int16_t& i) override;
	void serialize_variable(std::string_view key, int32_t& i) override;
	void serialize_variable(std::string_view key, int64_t& i) override;
	void serialize_variable(std::string_view key, fpm::fixed_16_16& f) override;
	void serialize_variable(std::string_view key, fpm::fixed_8_24& f) override;
	void serialize_variable(std::string_view key, float& f) override;
	void serialize_variable(std::string_view key, glm::vec2& v) override;
	void serialize_variable(std::string_view key, glm::ivec2& v) override;
	void serialize_variable(std::string_view key, bool& b) override;

private:
	const nlohmann::json* GetCurrentObject() const;
	const nlohmann::json* GetCurrentArray() const;
	size_t GetAndIncreaseCurrentArrayIndex();
	size_t GetCurrentArrayIndex() const;

	const nlohmann::json& _Json;
	std::vector<const nlohmann::json*> _ObjectStack;
	std::vector<const nlohmann::json*> _ArrayStack;
	std::vector<size_t> _ArrayIndicies;
};
