#include "json_text_reader.h"

#include <iostream>
#include <memory>

JsonTextReader::JsonTextReader(const nlohmann::json& Json) : _Json(Json)
{
	_ObjectStack.emplace_back(&_Json);
}

bool JsonTextReader::is_reader() const
{
	return true;
}

bool JsonTextReader::is_writer() const
{
	return false;
}

void JsonTextReader::push_object(std::string_view key, size_t /* sizeOfType */)
{
	_ObjectStack.emplace_back(&*GetCurrentObject()->find(key));
}

void JsonTextReader::push_element_object(size_t  /*sizeOfType*/)
{
	_ObjectStack.emplace_back(&GetCurrentArray()->at(GetAndIncreaseCurrentArrayIndex()));
}

void JsonTextReader::pop_object()
{
	_ObjectStack.pop_back();
}

void JsonTextReader::push_array(std::string_view key, size_t&  /*count*/, size_t  /*sizeOfElement*/)
{
	_ArrayStack.emplace_back(&*GetCurrentObject()->find(key));
	_ArrayIndicies.emplace_back(0);
}

void JsonTextReader::pop_array()
{
	_ArrayStack.pop_back();
	_ArrayIndicies.pop_back();
}

void JsonTextReader::serialize_variable_element(uint16_t& i)
{
	i = GetCurrentArray()->at(GetAndIncreaseCurrentArrayIndex());
}

void JsonTextReader::serialize_variable_element(uint32_t& i)
{
	i = GetCurrentArray()->at(GetAndIncreaseCurrentArrayIndex());
}

void JsonTextReader::serialize_variable_element(uint64_t& i)
{
	i = GetCurrentArray()->at(GetAndIncreaseCurrentArrayIndex());
}

void JsonTextReader::serialize_variable_element(int16_t& i)
{
	i = GetCurrentArray()->at(GetAndIncreaseCurrentArrayIndex());
}

void JsonTextReader::serialize_variable_element(int32_t& i)
{
	i = GetCurrentArray()->at(GetAndIncreaseCurrentArrayIndex());
}

void JsonTextReader::serialize_variable_element(int64_t& i)
{
	i = GetCurrentArray()->at(GetAndIncreaseCurrentArrayIndex());
}

void JsonTextReader::serialize_variable_element(fpm::fixed_16_16& f)
{
	f = fpm::fixed_16_16::from_raw_value(GetCurrentArray()->at(GetAndIncreaseCurrentArrayIndex()));
}

void JsonTextReader::serialize_variable_element(fpm::fixed_8_24& f)
{
	f = fpm::fixed_8_24::from_raw_value(GetCurrentArray()->at(GetAndIncreaseCurrentArrayIndex()));
}

void JsonTextReader::serialize_variable_element(float& f)
{
	f = GetCurrentArray()->at(GetAndIncreaseCurrentArrayIndex());
}

void JsonTextReader::serialize_variable_element(glm::vec2& v)
{
	// push_object(key, sizeof(glm::vec2));
	// serialize_variable_element("x", v.x);
	// serialize_variable_element("y", v.y);
	// pop_object();
}

void JsonTextReader::serialize_variable_element(glm::ivec2& v)
{
	// push_object(key, sizeof(glm::vec2));
	// serialize_variable_element("x", v.x);
	// serialize_variable_element("y", v.y);
	// pop_object();
}

void JsonTextReader::serialize_variable_element(bool& b)
{
	b = GetCurrentArray()->at(GetAndIncreaseCurrentArrayIndex());
}

void JsonTextReader::serialize_variable(std::string_view key, uint16_t& i)
{
	i = *GetCurrentObject()->find(key);
}

void JsonTextReader::serialize_variable(std::string_view key, uint32_t& i)
{
	i = *GetCurrentObject()->find(key);
}

void JsonTextReader::serialize_variable(std::string_view key, uint64_t& i)
{
	i = *GetCurrentObject()->find(key);
}

void JsonTextReader::serialize_variable(std::string_view key, int16_t& i)
{
	i = *GetCurrentObject()->find(key);
}

void JsonTextReader::serialize_variable(std::string_view key, int32_t& i)
{
	i = *GetCurrentObject()->find(key);
}

void JsonTextReader::serialize_variable(std::string_view key, int64_t& i)
{
	i = *GetCurrentObject()->find(key);
}

void JsonTextReader::serialize_variable(std::string_view key, fpm::fixed_16_16& f)
{
	f = fpm::fixed_16_16::from_raw_value(*GetCurrentObject()->find(key));
}

void JsonTextReader::serialize_variable(std::string_view key, fpm::fixed_8_24& f)
{
	f = fpm::fixed_8_24::from_raw_value(*GetCurrentObject()->find(key));
}

void JsonTextReader::serialize_variable(std::string_view key, float& f)
{
	f = *GetCurrentObject()->find(key);
}

void JsonTextReader::serialize_variable(std::string_view key, glm::vec2& v)
{
	push_object(key, sizeof(glm::vec2));
	serialize_variable("x", v.x);
	serialize_variable("y", v.y);
	pop_object();
}

void JsonTextReader::serialize_variable(std::string_view key, glm::ivec2& v)
{
	push_object(key, sizeof(glm::vec2));
	serialize_variable("x", v.x);
	serialize_variable("y", v.y);
	pop_object();
}

void JsonTextReader::serialize_variable(std::string_view key, bool& b)
{
	b = *GetCurrentObject()->find(key);
}

const nlohmann::json* JsonTextReader::GetCurrentObject() const
{
	return _ObjectStack.back();
}

const nlohmann::json* JsonTextReader::GetCurrentArray() const
{
	return _ArrayStack.back();
}

size_t JsonTextReader::GetAndIncreaseCurrentArrayIndex()
{
	return _ArrayIndicies.back()++;
}

size_t JsonTextReader::GetCurrentArrayIndex() const
{
	return _ArrayIndicies.back();
}
