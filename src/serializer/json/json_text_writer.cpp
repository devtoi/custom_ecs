#include "json_text_writer.h"

#include <iostream>
#include <memory>

JsonTextWriter::JsonTextWriter()
{
	_ObjectStack.emplace_back(&_Json);
}

JsonTextWriter::~JsonTextWriter()
{
	// std::cout << _Json.dump(4) << std::endl;
}

const nlohmann::json& JsonTextWriter::GetJson() const
{
	return _Json;
}

bool JsonTextWriter::is_reader() const
{
	return false;
}

bool JsonTextWriter::is_writer() const
{
	return true;
}

void JsonTextWriter::push_object(std::string_view key, size_t /* sizeOfType */)
{
	_ObjectStack.emplace_back(&*AccessCurrentObject()->emplace(key, nlohmann::json{}).first);
}

void JsonTextWriter::push_element_object(size_t  /*sizeOfType*/)
{
	_ObjectStack.emplace_back(&AccessCurrentArray()->emplace_back(nlohmann::json{}));
}

void JsonTextWriter::pop_object()
{
	_ObjectStack.pop_back();
}

void JsonTextWriter::push_array(std::string_view key, size_t&  /*count*/, size_t  /*sizeOfElement*/)
{
	_ArrayStack.emplace_back(&*AccessCurrentObject()->emplace(key, nlohmann::json{}).first);
}

void JsonTextWriter::pop_array()
{
	_ArrayStack.pop_back();
}

void JsonTextWriter::serialize_variable_element(uint16_t& i)
{
	AccessCurrentArray()->emplace_back(i);
}

void JsonTextWriter::serialize_variable_element(uint32_t& i)
{
	AccessCurrentArray()->emplace_back(i);
}

void JsonTextWriter::serialize_variable_element(uint64_t& i)
{
	AccessCurrentArray()->emplace_back(i);
}

void JsonTextWriter::serialize_variable_element(int16_t& i)
{
	AccessCurrentArray()->emplace_back(i);
}

void JsonTextWriter::serialize_variable_element(int32_t& i)
{
	AccessCurrentArray()->emplace_back(i);
}

void JsonTextWriter::serialize_variable_element(int64_t& i)
{
	AccessCurrentArray()->emplace_back(i);
}

void JsonTextWriter::serialize_variable_element(fpm::fixed_16_16& f)
{
	AccessCurrentArray()->emplace_back(f.raw_value());
}

void JsonTextWriter::serialize_variable_element(fpm::fixed_8_24& f)
{
	AccessCurrentArray()->emplace_back(f.raw_value());
}

void JsonTextWriter::serialize_variable_element(float& f)
{
	AccessCurrentArray()->emplace_back(f);
}

void JsonTextWriter::serialize_variable_element(glm::vec2& v)
{
	// push_object(key, sizeof(glm::vec2));
	// serialize_variable_element("x", v.x);
	// serialize_variable_element("y", v.y);
	// pop_object();
}

void JsonTextWriter::serialize_variable_element(glm::ivec2& v)
{
	// push_object(key, sizeof(glm::vec2));
	// serialize_variable_element("x", v.x);
	// serialize_variable_element("y", v.y);
	// pop_object();
}

void JsonTextWriter::serialize_variable_element(bool& b)
{
	AccessCurrentArray()->emplace_back(b);
}

void JsonTextWriter::serialize_variable(std::string_view key, uint16_t& i)
{
	AccessCurrentObject()->emplace(key, i);
}

void JsonTextWriter::serialize_variable(std::string_view key, uint32_t& i)
{
	AccessCurrentObject()->emplace(key, i);
}

void JsonTextWriter::serialize_variable(std::string_view key, uint64_t& i)
{
	AccessCurrentObject()->emplace(key, i);
}

void JsonTextWriter::serialize_variable(std::string_view key, int16_t& i)
{
	AccessCurrentObject()->emplace(key, i);
}

void JsonTextWriter::serialize_variable(std::string_view key, int32_t& i)
{
	AccessCurrentObject()->emplace(key, i);
}

void JsonTextWriter::serialize_variable(std::string_view key, int64_t& i)
{
	AccessCurrentObject()->emplace(key, i);
}

void JsonTextWriter::serialize_variable(std::string_view key, fpm::fixed_16_16& f)
{
	AccessCurrentObject()->emplace(key, f.raw_value());
}

void JsonTextWriter::serialize_variable(std::string_view key, fpm::fixed_8_24& f)
{
	AccessCurrentObject()->emplace(key, f.raw_value());
}

void JsonTextWriter::serialize_variable(std::string_view key, float& f)
{
	AccessCurrentObject()->emplace(key, f);
}

void JsonTextWriter::serialize_variable(std::string_view key, glm::vec2& v)
{
	push_object(key, sizeof(glm::vec2));
	serialize_variable("x", v.x);
	serialize_variable("y", v.y);
	pop_object();
}

void JsonTextWriter::serialize_variable(std::string_view key, glm::ivec2& v)
{
	push_object(key, sizeof(glm::vec2));
	serialize_variable("x", v.x);
	serialize_variable("y", v.y);
	pop_object();
}

void JsonTextWriter::serialize_variable(std::string_view key, bool& b)
{
	AccessCurrentObject()->emplace(key, b);
}

nlohmann::json* JsonTextWriter::AccessCurrentObject()
{
	return _ObjectStack.back();
}

nlohmann::json* JsonTextWriter::AccessCurrentArray()
{
	return _ArrayStack.back();
}
