#include "basic_writer.h"
#include <memory>

BasicWriter::BasicWriter(size_t initialSize) : m_Size(initialSize)
{
	m_Buffer = static_cast<char*>(malloc(initialSize));
	m_Walker = m_Buffer;
}

BasicWriter::~BasicWriter()
{
	free(m_Buffer);
}

const char* BasicWriter::GetBuffer() const
{
	return m_Buffer;
}

size_t BasicWriter::GetBufferSize() const
{
	return m_Size;
}

bool BasicWriter::is_reader() const
{
	return false;
}

bool BasicWriter::is_writer() const
{
	return true;
}

void BasicWriter::serialize(std::string_view key, uint16_t& i)
{
	memcpy_serialize(i);
}

void BasicWriter::serialize(std::string_view key, uint32_t& i)
{
	memcpy_serialize(i);
}

void BasicWriter::serialize(std::string_view key, uint64_t& i)
{
	memcpy_serialize(i);
}

void BasicWriter::serialize(std::string_view key, int16_t& i)
{
	memcpy_serialize(i);
}

void BasicWriter::serialize(std::string_view key, int32_t& i)
{
	memcpy_serialize(i);
}

void BasicWriter::serialize(std::string_view key, int64_t& i)
{
	memcpy_serialize(i);
}

void BasicWriter::serialize(std::string_view key, fpm::fixed_16_16& f)
{
	memcpy_serialize(f.raw_value());
}

void BasicWriter::serialize(std::string_view key, fpm::fixed_8_24& f)
{
	memcpy_serialize(f.raw_value());
}

void BasicWriter::serialize(std::string_view key, float& f)
{
	memcpy_serialize(f);
}

void BasicWriter::serialize(std::string_view key, glm::vec2& v)
{
	serialize(key, v.x);
	serialize(key, v.y);
}

void BasicWriter::serialize(std::string_view key, glm::ivec2& v)
{
	serialize(key, v.x);
	serialize(key, v.y);
}

void BasicWriter::serialize(std::string_view key, bool& b)
{
	memcpy_serialize(b);
}

void BasicWriter::Grow()
{
	m_Size *= 2;
	size_t diff = m_Walker - m_Buffer;
	m_Buffer = static_cast<char*>(realloc(m_Buffer, m_Size));
	m_Walker = m_Buffer + diff;
}

void BasicWriter::AccomodateSize(size_t size)
{
	if (m_Walker + size > m_Buffer + m_Size)
		Grow();
}

void BasicWriter::IncreaseWalker(size_t size)
{
	m_Walker += size;
}
