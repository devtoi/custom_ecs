#pragma once

#include "serializer_base.h"
#include <string.h>

class BasicWriter : public SerializerBase
{
public:
	BasicWriter(size_t initialSize = 128);
	~BasicWriter();

	const char* GetBuffer() const;
	size_t GetBufferSize() const;

	bool is_reader() const override;
	bool is_writer() const override;

	void serialize(std::string_view key, uint16_t& i) override;
	void serialize(std::string_view key, uint32_t& i) override;
	void serialize(std::string_view key, uint64_t& i) override;
	void serialize(std::string_view key, int16_t& i) override;
	void serialize(std::string_view key, int32_t& i) override;
	void serialize(std::string_view key, int64_t& i) override;
	void serialize(std::string_view key, fpm::fixed_16_16& f) override;
	void serialize(std::string_view key, fpm::fixed_8_24& f) override;
	void serialize(std::string_view key, float& f) override;
	void serialize(std::string_view key, glm::vec2& v) override;
	void serialize(std::string_view key, glm::ivec2& v) override;
	void serialize(std::string_view key, bool& b) override;

private:
	void Grow();
	void AccomodateSize(size_t size);
	void IncreaseWalker(size_t size);

	template <typename T> void memcpy_serialize(const T& val)
	{
		AccomodateSize(sizeof(T));
		memcpy(m_Walker, &val, sizeof(T));
		IncreaseWalker(sizeof(T));
	}

	char* m_Buffer = nullptr;
	char* m_Walker = nullptr;
	size_t m_Size;
};
