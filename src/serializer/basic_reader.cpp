#include "basic_reader.h"
#include <memory>

BasicReader::BasicReader(const char* bufferToCopy, size_t sizeOfBufferToCopy)
{
	buffer = static_cast<char*>(malloc(sizeOfBufferToCopy));
	memcpy(buffer, bufferToCopy, sizeOfBufferToCopy);
	walker = buffer;
}

BasicReader::~BasicReader()
{
	free(buffer);
}

bool BasicReader::is_reader() const
{
	return true;
}

bool BasicReader::is_writer() const
{
	return false;
}

void BasicReader::serialize(std::string_view key, uint16_t& i)
{
	Memcpy_deserialize(i);
}

void BasicReader::serialize(std::string_view key, uint32_t& i)
{
	Memcpy_deserialize(i);
}

void BasicReader::serialize(std::string_view key, uint64_t& i)
{
	Memcpy_deserialize(i);
}

void BasicReader::serialize(std::string_view key, int16_t& i)
{
	Memcpy_deserialize(i);
}

void BasicReader::serialize(std::string_view key, int32_t& i)
{
	Memcpy_deserialize(i);
}

void BasicReader::serialize(std::string_view key, int64_t& i)
{
	Memcpy_deserialize(i);
}

void BasicReader::serialize(std::string_view key, float& f)
{
	Memcpy_deserialize(f);
}

void BasicReader::serialize(std::string_view key, glm::vec2& v)
{
	serialize(key, v.x);
	serialize(key, v.y);
}

void BasicReader::serialize(std::string_view key, glm::ivec2& v)
{
	serialize(key, v.x);
	serialize(key, v.y);
}

void BasicReader::serialize(std::string_view key, bool& b)
{
	Memcpy_deserialize(b);
}
