#pragma once
#include <ecs/entity.h>
#include <glm/ext/vector_float2.hpp>
#include <glm/ext/vector_int2.hpp>
#include <string_view>
#include <vector>
#include <type_traits>
#include <span>
#include <fpm/fixed.hpp>

class SerializerBase
{
public:
	template <typename T, typename... ARGS> void serialize(std::string_view key, T& object, ARGS... ExtraArgs)
	{
		constexpr bool hasSerializeFunction = requires(T & t)
		{
			t.serialize(*this, ExtraArgs...);
		};

		if constexpr (hasSerializeFunction)
		{
			push_object(key, sizeof(T));
			object.serialize(*this, ExtraArgs...);
			pop_object();
		}
		else
		{
			serialize_variable(key, object);
		}
	}

	template <typename T, typename... ARGS> void array_element_serialize(T& object, ARGS... ExtraArgs)
	{
		constexpr bool hasSerializeFunction = requires(T & t)
		{
			t.serialize(*this, ExtraArgs...);
		};

		if constexpr (hasSerializeFunction)
		{
			push_element_object(sizeof(T));
			object.serialize(*this, ExtraArgs...);
			pop_object();
		}
		else
		{
			serialize_variable_element(object);
		}
	}

	template <typename TypeKey, typename TypeValue>
	void serialize(std::string_view key, std::unordered_map<TypeKey, TypeValue>& objects)
	{
		std::vector<std::pair<TypeKey, TypeValue>> vec;
		if (is_writer())
		{
			for (const std::pair<TypeKey, TypeValue>& kvp : objects)
			{
				vec.emplace_back(kvp);
			}
		}
		serialize(key, vec);
		if (is_reader())
		{
			for (const std::pair<TypeKey, TypeValue>& kvp : vec)
			{
				objects.emplace(kvp.first, kvp.second);
			}
		}
	}

	template <typename T, typename... ARGS> void serialize(std::string_view key, std::span<T>& objects, ARGS... Args)
	{
		size_t size = objects.size();
		push_array(key, size, sizeof(T));
		objects.resize(size);
		for (auto& object : objects)
		{
			array_element_serialize(object, Args...);
		}
		pop_array();
	}

	template <typename T, typename... ARGS> void serialize(std::string_view key, std::vector<T>& objects, ARGS... Args)
	{
		size_t size = objects.size();
		push_array(key, size, sizeof(T));
		objects.resize(size);
		for (auto& object : objects)
		{
			array_element_serialize(object, Args...);
		}
		pop_array();
	}


	virtual void push_object(std::string_view key, size_t sizeOfType) = 0;
	virtual void push_element_object(size_t sizeOfType) = 0;
	virtual void pop_object() = 0;
	virtual void push_array(std::string_view key, size_t& count, size_t sizeOfElement) = 0;
	virtual void pop_array() = 0;

	virtual bool is_reader() const = 0;
	virtual bool is_writer() const = 0;

protected:
	template <typename T, typename U> void serialize_variable(std::string_view key, std::pair<T, U>& pair)
	{
		push_object(key, sizeof(T) + sizeof(U));
		serialize_variable("first", pair.first);
		serialize_variable("second", pair.second);
		pop_object();
	}

	template <typename T, typename U> void serialize_variable_element(std::pair<T, U>& pair)
	{
		push_element_object(sizeof(T) + sizeof(U));
		serialize_variable("first", pair.first);
		serialize_variable("second", pair.second);
		pop_object();
	}

	template <typename Tag, typename _Implementation, _Implementation _InvalidValue>
	void serialize_variable(std::string_view key, Handle<Tag, _Implementation, _InvalidValue>& handle)
	{
		serialize_variable(key, handle.access_underlaying_value());
	}

	template <typename Tag, typename _Implementation, _Implementation _InvalidValue>
	void serialize_variable_element(Handle<Tag, _Implementation, _InvalidValue>& handle)
	{
		serialize_variable_element(handle.access_underlaying_value());
	}

	virtual void serialize_variable(std::string_view key, uint16_t& i) = 0;
	virtual void serialize_variable_element(uint16_t& i) = 0;

	virtual void serialize_variable(std::string_view key, uint32_t& i) = 0;
	virtual void serialize_variable_element(uint32_t& i) = 0;

	virtual void serialize_variable(std::string_view key, uint64_t& s) = 0;
	virtual void serialize_variable_element(uint64_t& s) = 0;

	virtual void serialize_variable(std::string_view key, int16_t& i) = 0;
	virtual void serialize_variable_element(int16_t& i) = 0;

	virtual void serialize_variable(std::string_view key, int32_t& i) = 0;
	virtual void serialize_variable_element(int32_t& i) = 0;

	virtual void serialize_variable(std::string_view key, int64_t& i) = 0;
	virtual void serialize_variable_element(int64_t& i) = 0;

	virtual void serialize_variable(std::string_view key, fpm::fixed_16_16& f) = 0;
	virtual void serialize_variable_element(fpm::fixed_16_16& f) = 0;

	virtual void serialize_variable(std::string_view key, fpm::fixed_8_24& f) = 0;
	virtual void serialize_variable_element(fpm::fixed_8_24& f) = 0;

	virtual void serialize_variable(std::string_view key, float& f) = 0;
	virtual void serialize_variable_element(float& f) = 0;

	virtual void serialize_variable(std::string_view key, glm::vec2& v) = 0;
	virtual void serialize_variable_element(glm::vec2& v) = 0;

	virtual void serialize_variable(std::string_view key, glm::ivec2& v) = 0;
	virtual void serialize_variable_element(glm::ivec2& v) = 0;

	virtual void serialize_variable(std::string_view key, bool& b) = 0;
	virtual void serialize_variable_element(bool& b) = 0;
};
