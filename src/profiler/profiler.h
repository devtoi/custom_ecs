#pragma once

#include <vector>
#include "profiler_entry.h"
#include "profiler_entry_collection.h"

namespace Profiler
{
	struct FrameData
	{
		std::vector<std::vector<ProfilerEntry>> PerThreadData;
	};
	void AddEntry(const ProfilerEntry& entry);
	void ManageCollection(ProfilerEntryCollection* collection);
	void NewFrame();
	void PrintEntries();
	bool IsRecording();
	void SetRecording(bool ShouldRecord);
	const std::vector<FrameData>& GetPreviousFrameCollections();
	const std::vector<std::chrono::high_resolution_clock::time_point>& GetFrameStarts();
}
