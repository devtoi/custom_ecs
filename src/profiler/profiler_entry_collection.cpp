#include "profiler_entry_collection.h"

#include "profiler.h"

ProfilerEntryCollection::ProfilerEntryCollection()
{
	Profiler::ManageCollection(this);
}

void ProfilerEntryCollection::AddProfilerEntry(const ProfilerEntry& entry)
{
	m_Entries.emplace_back(entry);
}

void ProfilerEntryCollection::Clear()
{
	m_Entries.clear();
}

const std::vector<ProfilerEntry>& ProfilerEntryCollection::GetEntries() const
{
	return m_Entries;
}
