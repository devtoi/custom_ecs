#include "profiler_scope.h"

#include "profiler_entry.h"
#include "profiler.h"

ProfilerScope::~ProfilerScope()
{
	if (Entry.End == std::chrono::high_resolution_clock::time_point::min())
	{
		Stop();
	}
}

void ProfilerScope::Stop()
{
	Entry.End = std::chrono::high_resolution_clock::now();
	Profiler::AddEntry(Entry);
}
