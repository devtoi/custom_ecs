#pragma once

#include <chrono>
#include <string/string_id.h>

struct ProfilerEntry
{
	StringID Tag;
	std::chrono::high_resolution_clock::time_point Start = std::chrono::high_resolution_clock::time_point::min();
	std::chrono::high_resolution_clock::time_point End = std::chrono::high_resolution_clock::time_point::min();
};
