#include "profiler.h"

#include <cassert>
#include <spdlog/spdlog.h>

#include "profiler_entry_collection.h"

namespace Profiler
{
	thread_local ProfilerEntryCollection Entries;
	std::vector<ProfilerEntryCollection*> Collections;
	std::vector<FrameData> PreviousFrames;
	std::vector<std::chrono::high_resolution_clock::time_point> FrameStarts;
	std::mutex AddCollectionMutex;
	size_t FrameNumber = 0;
	size_t NumberOfFramesToStore = 100;
	std::chrono::high_resolution_clock::time_point FrameStart = std::chrono::high_resolution_clock::time_point::max();

	bool Record = false;

	void ManageCollection(ProfilerEntryCollection* collection)
	{
		AddCollectionMutex.lock();
		assert(std::find(Collections.begin(), Collections.end(), collection) == Collections.end());
		Collections.emplace_back(collection);
		AddCollectionMutex.unlock();
	}

	void AddEntry(const ProfilerEntry& entry)
	{
		if (Record)
		{
			Entries.AddProfilerEntry(entry);
		}
	}

	void NewFrame()
	{
		if (!Record)
		{
			return;
		}

		if (PreviousFrames.size() < NumberOfFramesToStore || FrameStarts.size() < NumberOfFramesToStore)
		{
			PreviousFrames.resize(NumberOfFramesToStore);
			FrameStarts.resize(NumberOfFramesToStore, std::chrono::high_resolution_clock::time_point::max());
		}
		PreviousFrames[FrameNumber % NumberOfFramesToStore].PerThreadData.clear();
		for (auto* Collection : Collections)
		{
			PreviousFrames[FrameNumber % NumberOfFramesToStore].PerThreadData.emplace_back(Collection->GetEntries());
			Collection->Clear();
		}
		FrameStarts[FrameNumber % NumberOfFramesToStore] = FrameStart;
		FrameNumber++;
		FrameStart = std::chrono::high_resolution_clock::now();
	}

	void PrintEntries()
	{
		int i = 0;
		for (auto& Collection : Collections)
		{
			spdlog::info("Thread {0}", i++);
			for (const auto& Entry : Collection->GetEntries())
			{
				spdlog::info("{0} {1}ns", Entry.Tag.GetID(),
					std::chrono::duration_cast<std::chrono::nanoseconds>(Entry.End - Entry.Start).count());
			}
		}
	}

	bool IsRecording()
	{
		return Record;
	}

	void SetRecording(bool ShouldRecord)
	{
		Record = ShouldRecord;
	}

	const std::vector<FrameData>& GetPreviousFrameCollections()
	{
		return PreviousFrames;
	}

	const std::vector<std::chrono::high_resolution_clock::time_point>& GetFrameStarts()
	{
		return FrameStarts;
	}
}
