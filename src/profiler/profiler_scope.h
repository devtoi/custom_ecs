#pragma once

#include <chrono>
#include <string/string_id.h>
#include <string/string_database.h>

#include "profiler_entry.h"

#define PROFILE_SCOPE(Name) const ProfilerScope Scope(Name);

struct ProfilerScope
{
public:
	ProfilerScope(std::string_view Name)
	{
		// Entry.Tag = StringID{ Name };
		Entry.Tag = StringDatabase::AddString(Name);
		Entry.Start = std::chrono::high_resolution_clock::now();
	}

	~ProfilerScope();

	void Stop();

private:
	ProfilerEntry Entry;
};
