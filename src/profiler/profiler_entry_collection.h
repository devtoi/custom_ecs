#pragma once

#include <vector>
#include "profiler_entry.h"

class ProfilerEntryCollection
{
public:
	ProfilerEntryCollection();
	void AddProfilerEntry(const ProfilerEntry& entry);
	void Clear();
	const std::vector<ProfilerEntry>& GetEntries() const;

private:
	std::vector<ProfilerEntry> m_Entries;
};
