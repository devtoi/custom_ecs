#include <iostream>
#include <random>

#define NOMINMAX
#include <SDL2/SDL.h>
#if USE_NAKAMA
#include "nakama-cpp-c-wrapper/NakamaWrapperImpl.h"
#ifdef USE_IMGUI
#include <ecs/system/imgui/network/imgui_network_account_menu_system.h>
#include <ecs/system/imgui/network/imgui_network_lobby_system.h>
#endif
#include <ecs/system/network/network_authentication_system.h>
#include <ecs/system/network/network_lobby_system.h>
#include <ecs/system/network/network_tick_system.h>
#endif
#include <spdlog/spdlog.h>

#include "ecs/system/core/menu_system.h"
#if USE_VULKAN
#include "ecs/system/rendering/renderer_begin_frame_system.h"
#include "ecs/system/rendering/renderer_end_frame_system.h"
#include "ecs/system/rendering/sdl_to_renderer_events_system.h"
#ifdef USE_IMGUI
#include "ecs/system/imgui/imgui_end_frame_system.h"
#include "ecs/system/imgui/imgui_main_menu_system.h"
#include "ecs/system/imgui/imgui_systems_visualizer_system.h"
#include "ecs/system/imgui/imgui_profiling_data_system.h"
#include "ecs/system/imgui/imgui_begin_frame_system.h"
#endif
#endif
#include "ecs/system/persistance/save_system.h"
#include <ecs/system/persistance/load_system.h>
#include "ecs/system/sdl/sdl_event_system.h"
#include "ecs/world.h"

// #define DEBUG_MEMORY
#ifdef DEBUG_MEMORY
#include "windows.h"
#define _CRTDBG_MAP_ALLOC // to get more details
#include <crtdbg.h>		  //for malloc and free
#include <stdlib.h>
#endif

SystemCollection* AddCoreRenderingSystems(World& World)
{
	auto& SystemsBank = World.AccessSystemsBank();
	auto CoreRenderingCollection = std::make_unique<SystemCollection>();
#if USE_VULKAN
	CoreRenderingCollection->AddSystem<SDLToRendererEventsSystem>(SystemsBank, -150);
	CoreRenderingCollection->AddSystem<RendererBeginFrameSystem>(SystemsBank, -100);
	CoreRenderingCollection->AddSystem<RendererEndFrameSystem>(SystemsBank, 100);
#endif
	return SystemsBank.AddSystemCollectionTemplate("CoreRendering", std::move(CoreRenderingCollection));
}

SystemCollection* AddCoreImguiSystems(World& World)
{
	auto& SystemsBank = World.AccessSystemsBank();
	auto CoreImguiCollection = std::make_unique<SystemCollection>();

#if USE_VULKAN
#if USE_IMGUI
	CoreImguiCollection->AddSystem<IMGUIBeginFrameSystem>(SystemsBank, -99);
	CoreImguiCollection->AddSystem<ImguiMainMenuSystem>(SystemsBank);
#if USE_NAKAMA
	CoreImguiCollection->AddSystem<ImguiNetworkAccountMenuSystem>(SystemsBank);
	CoreImguiCollection->AddSystem<ImguiNetworkLobbySystem>(SystemsBank);
#endif
	CoreImguiCollection->AddSystem<ImguiSystemsVisualizerSystem>(SystemsBank, 90);
	CoreImguiCollection->AddSystem<ImguiProfilingDataSystem>(SystemsBank, 90);
	CoreImguiCollection->AddSystem<IMGUIEndFrameSystem>(SystemsBank, 99);
#endif
#endif
	return SystemsBank.AddSystemCollectionTemplate("CoreImgui", std::move(CoreImguiCollection));
}

SystemCollection* AddCoreSystems(World& World)
{
	auto& SystemsBank = World.AccessSystemsBank();
	auto CoreCollection = std::make_unique<SystemCollection>();

	CoreCollection->AddSystem<LoadSystem>(SystemsBank, -500);
#if USE_NAKAMA
	CoreCollection->AddSystem<NetworkTickSystem>(SystemsBank, -1001);
	CoreCollection->AddSystem<NetworkAuthenticationSystem>(SystemsBank);
	CoreCollection->AddSystem<NetworkLobbySystem>(SystemsBank, -400);
#endif
	CoreCollection->AddSystem<SDLEventSystem>(SystemsBank, -200);
	CoreCollection->AddSystem<MenuSystem>(SystemsBank);
	CoreCollection->AddSystem<SaveSystem>(SystemsBank, 500);
#if USE_NAKAMA
	CoreCollection->AddSystem<NetworkTickSystem>(SystemsBank, 2001);
#endif

	return SystemsBank.AddSystemCollectionTemplate("Core", std::move(CoreCollection));
}

int main(int /*argc*/, char* /*argv*/[])
{
#ifdef DEBUG_MEMORY
	_CrtSetDbgFlag(_CRTDBG_ALLOC_MEM_DF | _CRTDBG_LEAK_CHECK_DF);
#endif
	spdlog::set_level(spdlog::level::trace);
	{
		// Initialize SDL
		if (SDL_Init(SDL_INIT_VIDEO | SDL_INIT_EVENTS) != 0)
		{
			std::cerr << "Unable to initialize SDL" << std::endl;
			return -1;
		}

		{
			World world;
			SystemCollection SimulationCollection;
			SystemCollection VisualizationCollection;

			VisualizationCollection.AddOtherCollection(AddCoreSystems(world));
			VisualizationCollection.AddOtherCollection(AddCoreImguiSystems(world));
			VisualizationCollection.AddOtherCollection(AddCoreRenderingSystems(world));

			world.SetNextSystemCollection(std::move(SimulationCollection), std::move(VisualizationCollection));

			while (!world.ShouldShutdown())
			{
				world.Update();
			}
		}

		SDL_Quit();
	}

	return 0;
}
