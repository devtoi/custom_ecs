#include "string_database.h"

#include <unordered_map>
#include <string>

namespace
{
	std::unordered_map<StringID, std::string, StringIDHasher> StringLookup;
}

namespace StringDatabase
{
	StringID AddString(std::string_view String)
	{
		StringID ID{ String };
		StringLookup[ID] = String;
		return ID;
	}

	std::string_view LookupString(StringID ID)
	{
		auto it = StringLookup.find(ID);
		if (it != StringLookup.end())
		{
			return it->second;
		}
		return std::string_view{ "NON_EXISTANT_STRING" };
	}
}
