#pragma once
#include <string/string_id.h>

namespace StringDatabase
{
	StringID AddString(std::string_view String);
	std::string_view LookupString(StringID ID);
}
