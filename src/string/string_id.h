#pragma once
#include <limits>
#include <cstdint>

#include <type_traits>
#include <string>
#include <string_view>

class StringID
{
public:
	using HashType = size_t;
	static constexpr HashType InvalidValue = std::numeric_limits<HashType>::max();

	constexpr explicit StringID() : m_ID{ InvalidValue }
	{
	}

	constexpr explicit StringID(HashType UnderlayingValue) : m_ID(UnderlayingValue)
	{
	}

	constexpr explicit StringID(const std::string& str) : StringID(str.c_str())
	{
	}

	constexpr explicit StringID(const char* pChar) : StringID(std::string_view{ pChar })
	{
	}

	constexpr explicit StringID(std::string_view sv)
	{
		// fnv-1 64 bit
		size_t result = 0xcbf29ce484222325;

		for (char c : sv)
		{
			result *= 1099511628211;
			result ^= c;
		}

		m_ID = result;
	}

	constexpr bool operator==(const StringID& Other) const
	{
		return Other.GetID() == GetID();
	}

	[[nodiscard]] constexpr HashType GetID() const
	{
		return m_ID;
	}

	[[nodiscard]] constexpr bool IsInvalid() const
	{
		return m_ID == InvalidValue;
	}

	constexpr HashType& AccessUnderlayingValue()
	{
		return m_ID;
	}

private:
	HashType m_ID = InvalidValue;
};

constexpr StringID operator"" _sid(const char* pChar, size_t /*s*/)
{
	return StringID(pChar);
}

constexpr StringID operator"" _StringID(const char* pChar, size_t /*s*/)
{
	return StringID(pChar);
}

constexpr StringID::HashType operator"" _sidv(const char* pChar, size_t /*s*/)
{
	return StringID{ pChar }.GetID();
}

constexpr StringID::HashType operator"" _StringIDValue(const char* pChar, size_t /*s*/)
{
	return StringID{ pChar }.GetID();
}

struct StringIDHasher
{
	std::size_t operator()(const StringID& Key) const
	{
		return Key.GetID();
	}
};
