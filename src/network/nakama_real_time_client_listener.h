#pragma once

#include "nakama-cpp-c-wrapper/NakamaWrapper.h"

using namespace NAKAMA_NAMESPACE;

class World;

class NakamaRealTimeClientListener : public NRtClientListenerInterface
{
public:
	void SetWorld(World* pWorld);
	/**
	 * Called when the client socket has been connected.
	 */
	void onConnect() override
	{
	}

	/**
	 * Called when the client socket disconnects.
	 *
	 * @param info The <c>NRtClientDisconnectInfo</c>.
	 */
	void onDisconnect(const NRtClientDisconnectInfo& info) override
	{
		(void)info;
	}

	/**
	 * Called when the client receives an error.
	 *
	 * @param error The <c>NRtError</c> received.
	 */
	void onError(const NRtError& error) override
	{
		(void)error;
	}

	/**
	 * Called when a new channel message has been received.
	 *
	 * @param message The <c>NChannelMessage</c> received.
	 */
	void onChannelMessage(const NChannelMessage& message) override
	{
		(void)message;
	}

	/**
	 * Called when a new channel presence update has been received.
	 *
	 * @param presence The <c>NChannelPresenceEvent</c> received.
	 */
	void onChannelPresence(const NChannelPresenceEvent& presence) override
	{
		(void)presence;
	}

	/**
	 * Called when a matchmaking has found a match.
	 *
	 * @param matched The <c>NMatchmakerMatched</c> received.
	 */
	void onMatchmakerMatched(NMatchmakerMatchedPtr matched) override
	{
		(void)matched;
	}

	/**
	 * Called when a new match data is received.
	 *
	 * @param matchData The <c>NMatchData</c> received.
	 */
	void onMatchData(const NMatchData& matchData) override;

	/**
	 * Called when a new match presence update is received.
	 *
	 * @param matchPresence The <c>NMatchPresenceEvent</c> received.
	 */
	void onMatchPresence(const NMatchPresenceEvent& matchPresence) override;

	/**
	 * Called when the client receives new notifications.
	 *
	 * @param notifications The list of <c>NNotification</c> received.
	 */
	void onNotifications(const NNotificationList& notifications) override
	{
		(void)notifications;
	}

	/**
	 * Called when occur when the current user's invitation request is accepted
	 * by the party leader of a closed party.
	 *
	 * @param party the <c>NParty</c> joined by the user.
	 */
	void onParty(const NParty& party) override
	{
		(void)party;
	}

	/**
	 * Called when either the user's party closes or the user is removed from the party.
	 *
	 * @param partyClosedEvent The <c>NPartyClose</c> received.
	 */
	void onPartyClosed(const NPartyClose& partyCloseEvent) override
	{
		(void)partyCloseEvent;
	};

	/**
	 * Called when the user receives custom party data.
	 *
	 * @param partyData The <c>NPartyData</c> received.
	 */
	void onPartyData(const NPartyData& partyData) override
	{
		(void)partyData;
	};

	/**
	 * Called when the user receives a request to join the party.
	 *
	 * @param party The <c>NPartyJoinRequest</c> received.
	 */
	void onPartyJoinRequest(const NPartyJoinRequest& partyJoinRequest) override
	{
		(void)partyJoinRequest;
	};

	/**
	 * Called when the user's party leader has changed.
	 *
	 * @param partyLeader the new <c>NPartyLeader</c>.
	 */
	void onPartyLeader(const NPartyLeader& partyLeader) override
	{
		(void)partyLeader;
	};

	/**
	 * Called when the user receives a new party matchmaker ticket.
	 *
	 * @param ticket the <c>NPartyMatchmakerTicket</c> received upon entering the matchmaking system.
	 */
	void onPartyMatchmakerTicket(const NPartyMatchmakerTicket& ticket) override
	{
		(void)ticket;
	};

	/**
	 * Called when a presence event occurs within the party.
	 * Received a new presence event in the party.
	 *
	 * @param presenceEvent the <c>NPNPartyPresenceEvent</c> received.
	 */
	void onPartyPresence(const NPartyPresenceEvent& presenceEvent) override
	{
		(void)presenceEvent;
	};

	/**
	 * Called when the client receives status presence updates.
	 *
	 * @param presence Updated <c>NStatusPresenceEvent</c> presence.
	 */
	void onStatusPresence(const NStatusPresenceEvent& presence) override
	{
		(void)presence;
	}

	/**
	 * Called when the client receives stream presence updates.
	 *
	 * @param presence Updated <c>NStreamPresenceEvent</c> presence.
	 */
	void onStreamPresence(const NStreamPresenceEvent& presence) override
	{
		(void)presence;
	}

	/**
	 * Called when the client receives stream data.
	 *
	 * @param data Stream <c>NStreamData</c> data received.
	 */
	void onStreamData(const NStreamData& data) override
	{
		(void)data;
	}

private:
	World* _pWorld;
};
