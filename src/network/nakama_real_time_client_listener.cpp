#include "nakama_real_time_client_listener.h"

#include "ecs/component/core/simulation_input_component.h"
#include "ecs/component/network/network_match_component.h"
#include "ecs/component/network/network_player_component.h"
#include "ecs/component/network/network_to_simulation_input_component.h"
#include "ecs/simulation_input/network/tick_input.h"
#include "ecs/simulation_input/core/set_simulation_pause_state_input.h"
#include "ecs/utils/network_util.h"
#include "ecs/world.h"
#include "serializer/json/json_text_reader.h"

void NakamaRealTimeClientListener::SetWorld(World* pWorld)
{
	_pWorld = pWorld;
}

void NakamaRealTimeClientListener::onMatchData(const NMatchData& matchData)
{
	auto& EntityManager = _pWorld->AccessEntityManager();
	auto* pMatch = EntityManager.AccessFirstComponent<NetworkMatchComponent>();
	if (!pMatch)
	{
		return;
	}
	NetworkToSimulationInputComponent* pNetworkToSimulation = nullptr;
	StringID SessionID{ matchData.presence.sessionId };
	for (auto PlayerEntity : pMatch->PlayerEntities)
	{
		const auto& Player = EntityManager.AccessComponent<NetworkPlayerComponent>(PlayerEntity);
		// TODO store sessionid in match player as pair
		if (Player.SessionID == SessionID)
		{
			pNetworkToSimulation = &EntityManager.AccessComponent<NetworkToSimulationInputComponent>(PlayerEntity);
			break;
		}
	}
	if (pNetworkToSimulation)
	{
		if (auto pInput =
				SimulationInputFactory::CreateInput(StringID{ static_cast<StringID::HashType>(matchData.opCode) }))
		{
			JsonTextReader Reader(nlohmann::json::parse(matchData.data));
			pInput->Serialize(Reader);
			if (pInput->GetID() == TickInput::ID)
			{
				auto* pTickInput = static_cast<TickInput*>(pInput.get());
				pTickInput->FromSessionID = StringID{ matchData.presence.sessionId };
				pInput->Execute(*_pWorld);
			}
			else if (pInput->GetID() == UnpauseSimulationInput::ID)
			{
				if (pInput->IsValid(*_pWorld))
				{
					pInput->Execute(*_pWorld);
				}
			}
			else
			{
				pNetworkToSimulation->_NetworkToSimulationInputs
					.at((pInput->GetTargetFrame()) % NETWORK_FRAME_BUFFER_SIZE)
					.emplace_back(std::move(pInput));
			}
		}
	}
}

void NakamaRealTimeClientListener::onMatchPresence(const NMatchPresenceEvent& matchPresence)
{
	auto* pMatch = _pWorld->AccessEntityManager().AccessFirstComponent<NetworkMatchComponent>();
	if (!pMatch)
	{
		return;
	}

	for (const auto& Join : matchPresence.joins)
	{
		NetworkUtil::AddPlayer(*_pWorld, Join, *pMatch);
	}
	// TODO Handles leaves
}
