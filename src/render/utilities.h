#pragma once
#include <vector>
#include <string>

#include <vulkan/vulkan_raii.hpp>

#if defined(_MSC_VER) && !defined(_WIN64)
#define NON_DISPATCHABLE_HANDLE_TO_UINT64_CAST(type, x) static_cast<type>(x)
#else
#define NON_DISPATCHABLE_HANDLE_TO_UINT64_CAST(type, x) reinterpret_cast<uint64_t>(static_cast<type>(x))
#endif

namespace renderer
{
	namespace utilities
	{
		template <typename T> void SetDebugName(vk::raii::Device& device, T& object, const std::string& name)
		{
			vk::DebugUtilsObjectNameInfoEXT nameInfo;
			nameInfo.objectType = T::objectType;
			nameInfo.objectHandle = NON_DISPATCHABLE_HANDLE_TO_UINT64_CAST(typename T::CType, *object);
			nameInfo.pObjectName = name.c_str();

			device.setDebugUtilsObjectNameEXT(nameInfo);
		}

		std::vector<char> readFile(const std::string& filename);

		void createBuffer(vk::raii::PhysicalDevice& physicaldevice, vk::raii::Device& device, vk::DeviceSize size,
			vk::BufferUsageFlags usage, vk::MemoryPropertyFlags properties, std::unique_ptr<vk::raii::Buffer>& buffer,
			std::unique_ptr<vk::raii::DeviceMemory>& bufferMemory, const std::string& DebugName);

		uint32_t findMemoryType(
			vk::raii::PhysicalDevice& physicalDevice, uint32_t typeFilter, vk::MemoryPropertyFlags properties);

		std::unique_ptr<vk::raii::ShaderModule> createShaderModule(
			vk::raii::Device& device, const std::vector<char>& code);
	}
}
