#pragma once
#include <glm/vec2.hpp>

struct Camera
{
	glm::vec2 Offset{ 0, 0 };
	float Zoom = 1.0f;
	float padding;
};
