#pragma once

#include <glm/vec2.hpp>
#include <glm/vec3.hpp>

struct Sprite
{
	glm::vec2 pos;
	float size;
	glm::vec3 color;
};
