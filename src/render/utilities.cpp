#include "utilities.h"
#include <fstream>

std::vector<char> renderer::utilities::readFile(const std::string& filename)
{
	std::ifstream file(filename, std::ios::ate | std::ios::binary);

	if (!file.is_open())
	{
		throw std::runtime_error("failed to open file: " + filename + "!");
	}

	size_t fileSize = (size_t)file.tellg();
	std::vector<char> buffer(fileSize);

	file.seekg(0);
	file.read(buffer.data(), fileSize);

	file.close();

	return buffer;
}

void renderer::utilities::createBuffer(vk::raii::PhysicalDevice& physicaldevice, vk::raii::Device& device,
	vk::DeviceSize size, vk::BufferUsageFlags usage, vk::MemoryPropertyFlags properties,
	std::unique_ptr<vk::raii::Buffer>& buffer, std::unique_ptr<vk::raii::DeviceMemory>& bufferMemory, const std::string& DebugName)
{
	vk::BufferCreateInfo bufferInfo{};
	bufferInfo.size = size;
	bufferInfo.usage = usage;
	bufferInfo.sharingMode = vk::SharingMode::eExclusive;

	buffer = std::make_unique<vk::raii::Buffer>(device, bufferInfo);

	SetDebugName(device, *buffer, DebugName);

	vk::MemoryRequirements memRequirements = buffer->getMemoryRequirements();

	vk::MemoryAllocateInfo allocInfo{};
	allocInfo.allocationSize = memRequirements.size;
	allocInfo.memoryTypeIndex =
		renderer::utilities::findMemoryType(physicaldevice, memRequirements.memoryTypeBits, properties);

	bufferMemory = std::make_unique<vk::raii::DeviceMemory>(device, allocInfo);
	SetDebugName(device, *bufferMemory, DebugName + " Memory");

	buffer->bindMemory(**bufferMemory, 0);
}

uint32_t renderer::utilities::findMemoryType(
	vk::raii::PhysicalDevice& physicalDevice, uint32_t typeFilter, vk::MemoryPropertyFlags properties)
{
	vk::PhysicalDeviceMemoryProperties memProperties = physicalDevice.getMemoryProperties();

	for (uint32_t i = 0; i < memProperties.memoryTypeCount; i++)
	{
		if ((typeFilter & (1 << i)) && (memProperties.memoryTypes[i].propertyFlags & properties) == properties)
		{
			return i;
		}
	}

	throw std::runtime_error("failed to find suitable memory type!");
	return 0;
}

std::unique_ptr<vk::raii::ShaderModule> renderer::utilities::createShaderModule(
	vk::raii::Device& device, const std::vector<char>& code)
{
	vk::ShaderModuleCreateInfo createInfo{};
	createInfo.codeSize = code.size();
	createInfo.pCode = reinterpret_cast<const uint32_t*>(code.data());

	return std::make_unique<vk::raii::ShaderModule>(device, createInfo);
}
