#pragma once

#include <serializer/serializer_base.h>
#include "nakama-cpp-c-wrapper/NakamaWrapper.h"
#include "string/string_id.h"

using namespace NAKAMA_NAMESPACE;

struct NetworkPlayerComponent
{
	StringID SessionID{};
	NUserPresence Presence;
	void serialize(SerializerBase& Serializer)
	{
		// Serializer.serialize(MyMember);
	}
};
