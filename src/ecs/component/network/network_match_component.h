#pragma once

#include <serializer/serializer_base.h>
#include "ecs/simulation_input/simulation_input.h"
#include "nakama-cpp-c-wrapper/NakamaWrapper.h"

using namespace NAKAMA_NAMESPACE;

constexpr size_t NETWORK_BUFFERED_FRAMES = 5;
constexpr size_t NETWORK_FRAME_BUFFER_SIZE = NETWORK_BUFFERED_FRAMES + 10;

struct NetworkMatchComponent
{
	NMatch _Match;
	Entity _LocalPlayer;
	std::vector<Entity> PlayerEntities;

	std::vector<std::unique_ptr<SimulationInput>> _OutboundInputs;
	void serialize(SerializerBase& Serializer)
	{
		// Serializer.serialize(MyMember);
	}
};
