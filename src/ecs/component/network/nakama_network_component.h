#pragma once

#include <serializer/serializer_base.h>

#include "nakama-cpp-c-wrapper/NakamaWrapper.h"
#include "network/nakama_real_time_client_listener.h"

using namespace NAKAMA_NAMESPACE;

struct NakamaNetworkComponent
{
	NClientPtr _Client;
	NRtClientPtr _RealtimeClient;
	NSessionPtr _Session;
	NakamaRealTimeClientListener _Listener;

	void serialize(SerializerBase& Serializer)
	{
		// Serializer.serialize(MyMember);
	}
};
