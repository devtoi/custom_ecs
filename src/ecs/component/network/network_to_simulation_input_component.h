#pragma once

#include <serializer/serializer_base.h>
#include "ecs/component/network/network_match_component.h"
#include "ecs/simulation_input/simulation_input.h"
#include "nakama-cpp-c-wrapper/NakamaWrapper.h"

using namespace NAKAMA_NAMESPACE;

struct NetworkToSimulationInputComponent
{
	size_t _LastFinishedFrame = std::numeric_limits<size_t>::max();
	std::array<std::vector<std::unique_ptr<SimulationInput>>, NETWORK_FRAME_BUFFER_SIZE> _NetworkToSimulationInputs;
	std::array<int32_t, NETWORK_FRAME_BUFFER_SIZE> _FrameHashes{ 0 };
	void serialize(SerializerBase& Serializer)
	{
		// Serializer.serialize(MyMember);
	}
};
