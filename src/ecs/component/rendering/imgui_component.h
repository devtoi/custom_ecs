#pragma once
#if USE_IMGUI
#include <memory>
#include <vulkan/vulkan_raii.hpp>
#include <imgui/backends/imgui_impl_sdl.h>
#include <imgui/backends/imgui_impl_vulkan.h>
#include <imgui/imgui.h>
#endif
#include <serializer/serializer_base.h>

struct ImGuiComponent
{
#if USE_IMGUI
	std::unique_ptr<vk::raii::DescriptorPool> DescriptorPool;
	std::vector<vk::raii::CommandBuffer> CommandBuffers;

	std::unique_ptr<vk::raii::RenderPass> RenderPass;
	std::unique_ptr<vk::raii::PipelineLayout> PipelineLayout;
	std::unique_ptr<vk::raii::Pipeline> Pipeline;
#endif
	void serialize(SerializerBase& serializer)
	{
	}
};
