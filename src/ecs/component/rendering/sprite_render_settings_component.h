#pragma once

#include <serializer/serializer_base.h>

struct SpriteRenderSettingsComponent
{
	bool Render = true;
	bool RenderPheromones = true;

	void serialize(SerializerBase& Serializer)
	{
		Serializer.serialize("Render", Render);
		Serializer.serialize("RenderPheromones", RenderPheromones);
	}
};
