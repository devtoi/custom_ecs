#pragma once
#if USE_VULKAN
#include <vulkan/vulkan_raii.hpp>
#endif
#include <serializer/serializer_base.h>
#include <ecs/entity.h>

struct TilemapOffscreenRenderPipelineComponent
{
#if USE_VULKAN
	static constexpr vk::Format Format = vk::Format::eB8G8R8A8Srgb;

	std::unique_ptr<vk::raii::RenderPass> RenderPass;
	std::unique_ptr<vk::raii::PipelineLayout> PipelineLayout;
	std::unique_ptr<vk::raii::Pipeline> Pipeline;
	std::unique_ptr<vk::raii::DescriptorSet> DescriptorSet;
	std::unique_ptr<vk::raii::CommandBuffer> CommandBuffer;
	vk::raii::Fence* InFlightFence = nullptr;

	std::unique_ptr<vk::raii::Buffer> MapDataBuffer;
	std::unique_ptr<vk::raii::DeviceMemory> MapDataBufferMemory;
	std::unique_ptr<vk::raii::DescriptorSetLayout> MapDataDescriptorSetLayout;
#endif
	Entity CurrentlyRenderTo;

	void serialize(SerializerBase& serializer)
	{
	}
};
