#pragma once

#include <vector>

#if USE_VULKAN
#include <vulkan/vulkan_raii.hpp>
#endif
#include <serializer/serializer_base.h>
#include <glm/vec2.hpp>

struct TileMapTextureVertex
{
	glm::vec2 pos;
	glm::vec2 uv;

	static vk::VertexInputBindingDescription getBindingDescription()
	{
		vk::VertexInputBindingDescription bindingDescription{};
		bindingDescription.binding = 0;
		bindingDescription.stride = sizeof(TileMapTextureVertex);
		bindingDescription.inputRate = vk::VertexInputRate::eVertex;

		return bindingDescription;
	}

	static std::array<vk::VertexInputAttributeDescription, 2> getAttributeDescriptions()
	{
		std::array<vk::VertexInputAttributeDescription, 2> attributeDescriptions{};

		attributeDescriptions[0].binding = 0;
		attributeDescriptions[0].location = 0;
		attributeDescriptions[0].format = vk::Format::eR32G32Sfloat;
		attributeDescriptions[0].offset = offsetof(TileMapTextureVertex, pos);

		attributeDescriptions[1].binding = 0;
		attributeDescriptions[1].location = 1;
		attributeDescriptions[1].format = vk::Format::eR32G32Sfloat;
		attributeDescriptions[1].offset = offsetof(TileMapTextureVertex, uv);

		return attributeDescriptions;
	}
};


struct TileMapTextureComponent
{
#if USE_VULKAN
	std::unique_ptr<vk::raii::DeviceMemory> _rtImageMemory;
	std::unique_ptr<vk::raii::Image> _rtImage;
	std::unique_ptr<vk::raii::ImageView> _rtImageView;
	std::unique_ptr<vk::raii::Framebuffer> _rtFrameBuffer;
	std::unique_ptr<vk::raii::Sampler> _rtSampler;

	std::unique_ptr<vk::raii::Buffer> _vertexBuffer;
	std::unique_ptr<vk::raii::DeviceMemory> _vertexBufferMemory;
	std::vector<vk::raii::DescriptorSet> _DescriptorSets;

	std::unique_ptr<vk::raii::Fence> Fence;

	void* _vertexBufferCPUMap = nullptr;
	std::unique_ptr<vk::raii::Buffer> OutputVertexBuffer;
	std::unique_ptr<vk::raii::DeviceMemory> OutputVertexBufferMemory;
	bool RenderedTo = false;
#endif

	void serialize(SerializerBase& serializer)
	{
	}
};
