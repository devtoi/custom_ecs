#pragma once
#if USE_VULKAN
#include <vulkan/vulkan.hpp>
#include <vulkan/vulkan_raii.hpp>
#include <SDL2/SDL.h>
#include <SDL2/SDL_vulkan.h>
#endif
#include <serializer/serializer_base.h>

class World;

struct RendererComponent
{
#if USE_VULKAN
	std::vector<std::function<void(World&, RendererComponent&)>> OnRecreateSwapChain;

	SDL_Window* _window = nullptr;
	int _windowWidth = 1600;
	int _windowHeight = 900;
	int _maxFramesInFlight = 2;

	unsigned int _graphicsQueueIndex = 0;
	bool _draw = true;
	bool _recreateSwapChain = false;
	bool _recreatedSwapChain = false;

	vk::SurfaceTransformFlagBitsKHR _transform = vk::SurfaceTransformFlagBitsKHR::eIdentity;
	vk::ImageUsageFlags _imageUsage = vk::ImageUsageFlagBits::eColorAttachment;
	vk::Format _swapChainImageFormat;
	vk::Extent2D _swapChainExtent{};

	unsigned int _currentFrame = 0;
	uint32_t _currentImageIndex = 0;

	std::unique_ptr<vk::raii::Context> _context;
	std::unique_ptr<vk::raii::Instance> _instance;
	std::unique_ptr<vk::raii::DebugUtilsMessengerEXT> _debugCallback;
	std::unique_ptr<vk::raii::PhysicalDevice> _physicalDevice;
	std::unique_ptr<vk::raii::Device> _device;
	std::unique_ptr<vk::raii::SurfaceKHR> _presentationSurface;
	std::unique_ptr<vk::raii::SwapchainKHR> _swapChain;
	std::unique_ptr<vk::raii::Queue> _graphicsQueue;
	std::unique_ptr<vk::raii::DescriptorPool> _descriptorPool;
	std::unique_ptr<vk::raii::CommandPool> _commandPool;
	std::unique_ptr<vk::raii::RenderPass> _renderPass;

	std::vector<vk::raii::Semaphore> _imageAvailableSemaphores;
	std::vector<vk::raii::Semaphore> _renderFinishedSemaphores;
	std::vector<vk::raii::Fence> _inFlightFences;
	std::vector<vk::raii::Fence*> _imagesInFlight;
	std::vector<vk::raii::ImageView> _swapChainImageViews;
	std::vector<vk::raii::Framebuffer> _swapChainFrameBuffers;
	std::vector<vk::raii::CommandBuffer> _commandBuffers;
	std::vector<vk::raii::Buffer> _cameraBuffers;
	std::vector<vk::raii::DeviceMemory> _cameraBufferMemories;
#endif
	void serialize(SerializerBase& serializer)
	{
	}
};
