#pragma once

#include <vulkan/vulkan_raii.hpp>
#include <serializer/serializer_base.h>

struct SpriteRenderPipelineComponent
{
	static constexpr int MAX_SPRITES = 100'000;

	unsigned int _vertexCount = 0;

	std::unique_ptr<vk::raii::DescriptorSetLayout> _cameraDescriptorSetLayout;
	std::unique_ptr<vk::raii::PipelineLayout> _pipelineLayout;
	std::unique_ptr<vk::raii::Pipeline> _graphicsPipeline;
	std::vector<vk::raii::DescriptorSet> _descriptorSets;

	std::unique_ptr<vk::raii::Buffer> _vertexBuffer;
	std::unique_ptr<vk::raii::DeviceMemory> _vertexBufferMemory;
	void* _vertexBufferCPUMap = nullptr;

	void serialize(SerializerBase& serializer)
	{
	}
};
