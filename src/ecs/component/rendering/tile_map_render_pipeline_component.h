#pragma once
#if USE_VULKAN
#include <vulkan/vulkan_raii.hpp>
#endif
#include <serializer/serializer_base.h>

struct TilemapRenderPipelineComponent
{
#if USE_VULKAN
	static constexpr vk::Format Format = vk::Format::eB8G8R8A8Srgb;

	std::unique_ptr<vk::raii::PipelineLayout> PipelineLayout;
	std::unique_ptr<vk::raii::Pipeline> Pipeline;
	std::unique_ptr<vk::raii::DescriptorSetLayout> DescriptorSetLayout;
	bool Used = false;
#endif

	void serialize(SerializerBase& serializer)
	{
	}
};
