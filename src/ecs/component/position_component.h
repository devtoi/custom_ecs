#pragma once

#include <glm/vec2.hpp>

#include "../../serializer/serializer_base.h"

struct PositionComponent
{
	glm::ivec2 position{ 0, 0 };
	void serialize(SerializerBase& serializer)
	{
		serializer.serialize("position", position);
	}
};
