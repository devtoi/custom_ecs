#pragma once

#include <glm/vec2.hpp>

#include <serializer/serializer_base.h>
#include <ecs/entity.h>

struct HomeTrackerComponent
{
	Entity HomeEntity;
	glm::ivec2 PositionOfHome;

	void serialize(SerializerBase& serializer)
	{
		serializer.serialize("HomeEntity", HomeEntity);
		serializer.serialize("PositionOfHome", PositionOfHome);
	}
};
