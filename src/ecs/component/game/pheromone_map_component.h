#pragma once

#include <ecs/component/tile_map_component.h>
#include <serializer/serializer_base.h>
#include <fpm/fixed.hpp>
#include <fpm/math.hpp>

using PheromoneStrength = fpm::fixed_16_16;

static constexpr PheromoneStrength FOOD_PHEROMONE_STRENGTH{ 0.1 };
static constexpr PheromoneStrength HOME_PHEROMONE_STRENGTH{ 0.2 };
static constexpr PheromoneStrength MAX_HOME_PHEROMONE_STRENGTH{ 1 };
static constexpr PheromoneStrength MAX_FOOD_PHEROMONE_STRENGTH{ 1 };

struct PheromonMapComponent
{
	std::array<std::array<PheromoneStrength, MAP_SIZE_HEIGHT_TILES>, MAP_SIZE_WIDTH_TILES> FoodPheromoneTiles{
		decltype(FoodPheromoneTiles)::value_type{ static_cast<PheromoneStrength>(0) }
	};

	std::array<std::array<PheromoneStrength, MAP_SIZE_HEIGHT_TILES>, MAP_SIZE_WIDTH_TILES> HomePheromoneTiles{
		decltype(FoodPheromoneTiles)::value_type{ static_cast<PheromoneStrength>(0) }
	};

	void serialize(SerializerBase& serializer)
	{
		{
			size_t width = FoodPheromoneTiles.size();
			size_t height = FoodPheromoneTiles[0].size();
			serializer.serialize("width", width);
			serializer.serialize("height", height);
			size_t size = width * height;
			serializer.push_array("FoodPheromoneTiles", size, sizeof(int));
			for (int y = 0; y < height; ++y)
			{
				for (int x = 0; x < width; ++x)
				{
					assert(width <= FoodPheromoneTiles.size());
					assert(height <= FoodPheromoneTiles[0].size());

					auto& Number = FoodPheromoneTiles[x][y];
					serializer.array_element_serialize(Number);
				}
			}
			serializer.pop_array();
		}
		{
			size_t width = HomePheromoneTiles.size();
			size_t height = HomePheromoneTiles[0].size();
			serializer.serialize("width", width);
			serializer.serialize("height", height);
			size_t size = width * height;
			serializer.push_array("HomePheromoneTiles", size, sizeof(int));
			for (int y = 0; y < height; ++y)
			{
				for (int x = 0; x < width; ++x)
				{
					assert(width <= HomePheromoneTiles.size());
					assert(height <= HomePheromoneTiles[0].size());

					auto& Number = HomePheromoneTiles[x][y];
					serializer.array_element_serialize(Number);
				}
			}
			serializer.pop_array();
		}
	}
};
