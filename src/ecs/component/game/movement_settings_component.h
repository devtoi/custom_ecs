#pragma once

#include <serializer/serializer_base.h>

struct MovementSettingsComponent
{
	int64_t BaseProbability = 1;
	int64_t FoodTileBaseProbability = 1000000;
	int64_t DirectionWeight = 10000;
	int64_t DistanceFromHomeWeightWhenHasFood = 10000;
	int64_t HomePheromoneMult = 10;
	int64_t HomePheromoneMultWhenScouting = 10;
	int64_t HomeBaseMult = 10;
	int64_t FoodPheromoneMult = 10;
	int64_t FoodBaseMult = 10;

	void serialize(SerializerBase& Serializer)
	{
		Serializer.serialize("BaseProbability", BaseProbability);
		Serializer.serialize("FoodTileBaseProbability", FoodTileBaseProbability);
		Serializer.serialize("DirectionWeight)", DirectionWeight);
		Serializer.serialize("DistanceFromHomeWeightWhenHasFood", DistanceFromHomeWeightWhenHasFood);
		Serializer.serialize("HomePheromoneMult", HomePheromoneMult);
		Serializer.serialize("HomePheromoneMultWhenScouting", HomePheromoneMultWhenScouting);
		Serializer.serialize("HomeBaseMult", HomeBaseMult);
		Serializer.serialize("FoodPheromoneMult", FoodPheromoneMult);
		Serializer.serialize("FoodBaseMult", FoodBaseMult);
	}
};
