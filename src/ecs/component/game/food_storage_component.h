#pragma once

#include <serializer/serializer_base.h>

struct FoodStorageComponent
{
	int FoodAmount = 0;

	void serialize(SerializerBase& serializer)
	{
		serializer.serialize("FoodAmount", FoodAmount);
	}
};
