#pragma once

#include <serializer/serializer_base.h>

struct FoodMoverComponent
{
	int FoodAmount = 0;

	void serialize(SerializerBase& serializer)
	{
		serializer.serialize("FoodAmount", FoodAmount);
	}
};
