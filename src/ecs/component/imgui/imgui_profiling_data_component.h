#pragma once

#include <serializer/serializer_base.h>

struct ImguiProfilingDataComponent
{
	static constexpr size_t MAX_FRAME_TIMES = 1'000;
	std::vector<float> _FrameTimes;
	void serialize(SerializerBase& Serializer)
	{
	}
};
