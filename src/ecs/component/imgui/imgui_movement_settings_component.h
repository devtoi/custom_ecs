#pragma once

#include <serializer/serializer_base.h>

struct ImguiMovementSettingsComponent
{
	int PheromoneTrackerIndex = 0;
	void serialize(SerializerBase& Serializer)
	{
		Serializer.serialize("PheromoneTrackerIndex", PheromoneTrackerIndex);
	}
};
