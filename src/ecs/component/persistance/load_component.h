#pragma once

#include <filesystem>
#include <serializer/serializer_base.h>

struct LoadComponent
{
	std::filesystem::path FileToLoad;
	bool RunLoad = false;

	void serialize(SerializerBase& Serializer)
	{
		// Serializer.serialize(MyMember);
	}
};
