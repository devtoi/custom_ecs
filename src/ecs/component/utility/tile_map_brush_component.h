#pragma once

#include <vector>

#include <serializer/serializer_base.h>

#include <ecs/component/tile_map_component.h>

struct TileMapBrushComponent
{
	int BrushSize = 3;
	TileType TileTypeToDraw = TileType::Food;

	void serialize(SerializerBase& serializer)
	{
		serializer.serialize("BrushSize", BrushSize);

		{
			auto& Tile = TileTypeToDraw;
			TileTypeType underlayingValue = static_cast<TileTypeType>(Tile);
			serializer.serialize("tile_type", underlayingValue);
			Tile = static_cast<TileType>(underlayingValue);
		}
	}
};
