#pragma once

#include <glm/vec2.hpp>

#include "../../serializer/serializer_base.h"

struct CameraComponent
{
	glm::vec2 Offset{ 0.0f, 0.0f };
	float Zoom = 1.0f;
	// Must be kept aligned so that shaders can use this structure directly
	float Padding;

	void serialize(SerializerBase& serializer)
	{
		serializer.serialize("Offset", Offset);
		serializer.serialize("Zoom", Zoom);
	}
};
