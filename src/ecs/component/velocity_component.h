#pragma once

#include <glm/vec2.hpp>

#include "../../serializer/serializer_base.h"

const static fpm::fixed_16_16 TILE_SUB_SIZE{ 1 };

struct VelocityComponent
{
	fpm::fixed_16_16 left{ 0 };
	fpm::fixed_16_16 speed{ 10 };
	glm::ivec2 direction{ 0, 0 };
	bool JustSwitchedTile = false;

	void serialize(SerializerBase& serializer)
	{
		serializer.serialize("left", left);
		serializer.serialize("speed", speed);
		serializer.serialize("direction", direction);
		serializer.serialize("JustSwitchedTile", JustSwitchedTile);
	}
};
