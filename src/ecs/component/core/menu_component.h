#pragma once

#include <filesystem>
#include <serializer/serializer_base.h>

#include <ecs/core/system_collection.h>

struct MenuComponent
{
	SystemCollection SimulationSystemsForNextMode;
	SystemCollection VisualizationSystemsForNextMode;
	bool SwitchToNextMode = true;

	void serialize(SerializerBase& Serializer)
	{
		// Serializer.serialize(MyMember);
	}
};
