#pragma once

#include <memory>
#include "ecs/simulation_input/simulation_input.h"
#include <serializer/serializer_base.h>

struct SimulationInputComponent
{
	std::vector<std::unique_ptr<SimulationInput>> _SimulationInputs;

	void serialize(SerializerBase& Serializer)
	{
		// Serializer.serialize(MyMember);
	}
};
