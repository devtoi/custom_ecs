#pragma once

#include <serializer/serializer_base.h>

#include <SDL2/SDL_events.h>

struct SDLWindowComponent
{
	std::vector<SDL_WindowEventID> m_SDLWindowEvents;

	void serialize(SerializerBase& Serializer)
	{
	}
};
