#pragma once

#include "serializer/serializer_base.h"
#include <cstdint>
#include <vector>
#include <array>

static constexpr size_t MAP_SIZE_WIDTH_TILES = 380;
static constexpr size_t MAP_SIZE_HEIGHT_TILES = 200;
static constexpr size_t TILE_SIZE = 4;
static constexpr size_t MAP_SIZE_WIDTH_PIXELS = MAP_SIZE_WIDTH_TILES * TILE_SIZE;
static constexpr size_t MAP_SIZE_HEIGHT_PIXELS = MAP_SIZE_HEIGHT_TILES * TILE_SIZE;

using TileTypeType = uint16_t;

enum class TileType : TileTypeType
{
	Passable,
	Impassable,
	Food,
	Count,
};

struct TileMapComponent
{
	std::array<std::array<TileType, MAP_SIZE_HEIGHT_TILES>, MAP_SIZE_WIDTH_TILES> map{ decltype(map)::value_type{
		TileType::Impassable } };

	std::array<std::array<int, MAP_SIZE_HEIGHT_TILES>, MAP_SIZE_WIDTH_TILES> _TileNumber{
		decltype(_TileNumber)::value_type{ 0 }
	};

	bool _Dirty = true;

	[[nodiscard]] static bool IsPosInside(glm::ivec2 Pos)
	{
		return Pos.x >= 0 && Pos.x < MAP_SIZE_WIDTH_TILES && Pos.y >= 0 && Pos.y < MAP_SIZE_HEIGHT_TILES;
	}

	void serialize(SerializerBase& serializer)
	{
		size_t width = map.size();
		size_t height = map[0].size();
		serializer.serialize("width", width);
		serializer.serialize("height", height);
		size_t size = width * height;
		serializer.push_array("tilemap", size, sizeof(TileTypeType));
		for (int y = 0; y < height; ++y)
		{
			for (int x = 0; x < width; ++x)
			{
				assert(width <= map.size());
				assert(height <= map[0].size());

				auto& Tile = map[x][y];
				TileTypeType underlayingValue = static_cast<TileTypeType>(Tile);
				serializer.array_element_serialize(underlayingValue);
				Tile = static_cast<TileType>(underlayingValue);
			}
		}
		serializer.pop_array();
		serializer.push_array("tilenumber", size, sizeof(int));
		for (int y = 0; y < height; ++y)
		{
			for (int x = 0; x < width; ++x)
			{
				assert(width <= map.size());
				assert(height <= map[0].size());

				auto& Number = _TileNumber[x][y];
				serializer.array_element_serialize(Number);
			}
		}
		serializer.pop_array();
	}
};
