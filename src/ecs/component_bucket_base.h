#pragma once

#include "../serializer/serializer_base.h"

class ComponentBucketBase
{
public:
	virtual size_t CreateComponent() = 0;
	virtual void DestroyComponent(size_t index) = 0;
	virtual void serialize(SerializerBase& serializer) = 0;
};
