#include "entity_manager.h"

#include <spdlog/spdlog.h>

ComponentUpdateParameters::ComponentUpdateParameters(
	class World& world, ArchetypeBucket& bucket, size_t start, size_t end)
	: World(world), Bucket(bucket), Start(start), End(end)
{
}

Entity EntityManager::CreateEntity(const ArchetypeMask& componentsInEntity)
{
	Entity entity = m_NextEntity;

	ArchetypeBucket& bucket = EnsureBucket(componentsInEntity);
	bucket.CreateEntity(entity);
	m_EntityToBucket.emplace(entity, bucket.GetHandle());

	m_NextEntity = Entity{ static_cast<Entity::Implementation>(m_NextEntity) + 1 };
	return entity;
}

ArchetypeBucket* EntityManager::FindArchetypeBucket(const ArchetypeMask& componentsInEntity)
{
	for (auto& bucket : m_ArchetypeBuckets)
	{
		if (bucket.GetComponentTypesInThisBucket() == componentsInEntity)
		{
			return &bucket;
		}
	}
	return nullptr;
}

void EntityManager::MarkEntityToBeKilled(Entity entity)
{
	spdlog::info("Marking entity {0} for removal", entity.get_underlaying_value());
	m_EntitiesToBeKilled.emplace_back(entity);
}

ComponentStorageRegistry& EntityManager::AccessComponentStorageRegistry()
{
	return m_ComponentStorageRegistry;
}

void EntityManager::serialize(SerializerBase& serializer)
{
	serializer.serialize("m_NextEntity", m_NextEntity);
	serializer.serialize("m_EntitiesToBeKilled", m_EntitiesToBeKilled);
	serializer.serialize("m_ComponentStorageRegistry", m_ComponentStorageRegistry);
	serializer.serialize("m_ArchetypeBuckets", m_ArchetypeBuckets, &m_ComponentStorageRegistry);
	serializer.serialize("m_EntityToBucket", m_EntityToBucket);
}

ArchetypeBucket& EntityManager::EnsureBucket(const ArchetypeMask& componentsInEntity)
{
	ArchetypeBucket* bucket = FindArchetypeBucket(componentsInEntity);

	if (!bucket)
	{
		ArchetypeHandle handle{ static_cast<ArchetypeHandle::Implementation>(m_ArchetypeBuckets.size()) };
		m_ArchetypeBuckets.emplace_back(componentsInEntity, handle, &m_ComponentStorageRegistry);
		bucket = &m_ArchetypeBuckets.back();
	}
	return *bucket;
}

ArchetypeBucket& EntityManager::AccessBucket(ArchetypeHandle archetypeHandle)
{
	return const_cast<ArchetypeBucket&>(GetBucket(archetypeHandle));
}

const ArchetypeBucket& EntityManager::GetBucket(ArchetypeHandle archetypeHandle) const
{
	assert(archetypeHandle.get_underlaying_value() < m_ArchetypeBuckets.size());
	return m_ArchetypeBuckets[archetypeHandle.get_underlaying_value()];
}

void EntityManager::KillMarkedEntities()
{
	while (!m_EntitiesToBeKilled.empty())
	{
		Entity entity = m_EntitiesToBeKilled.back();

		auto it = m_EntityToBucket.find(entity);
		assert(it != m_EntityToBucket.end());

		ArchetypeBucket& bucket = AccessBucket(it->second);

		bucket.RemoveEntity(entity);
		m_EntityToBucket.erase(it);

		m_EntitiesToBeKilled.pop_back();
	}
}
