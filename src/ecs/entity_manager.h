#pragma once

#include <vector>
#include <map>
#include <memory>
#include <cassert>
#include <functional>
#include <type_traits>
#include "../serializer/serializer_base.h"
#include "archetype_bucket.h"
#include "component_storage_registry.h"
#include "../job/job_engine.h"
#include "../profiler/profiler_scope.h"

class EntityManager
{
public:
	Entity CreateEntity(const ArchetypeMask& componentsInEntity);
	ArchetypeBucket* FindArchetypeBucket(const ArchetypeMask& componentsInEntity);

	template <typename Component> Entity GetFirstEntityWithComponent() const
	{
		ArchetypeMask mask = ArchetypeMaskFromComponents<Component>();

		for (const auto& bucket : m_ArchetypeBuckets)
		{
			if ((bucket.GetComponentTypesInThisBucket() & mask) == mask)
			{
				return bucket.LookupEntityForIndex(0);
			}
		}
		assert(false && "Failure to find existing component");
		return Entity::invalid();
	}

	template <typename Component> const Component* GetFirstComponent() const
	{
		ArchetypeMask mask = ArchetypeMaskFromComponents<Component>();

		for (const auto& bucket : m_ArchetypeBuckets)
		{
			if ((bucket.GetComponentTypesInThisBucket() & mask) == mask)
			{
				return &bucket.GetComponent<Component>(0);
			}
		}
		// assert(false && "Failure to find existing component");
		return nullptr;
	}

	template <typename Component> Component* AccessFirstComponent()
	{
		return const_cast<Component*>(GetFirstComponent<Component>());
	}

	template <typename... ComponentBuckets>
	void ForeachSerial(World& world, std::function<void(ComponentUpdateParameters&, ComponentBuckets&...)> func)
	{
		ArchetypeMask mask = ArchetypeMaskFromComponentBucketsReadWrite<ComponentBuckets...>();

		for (auto& bucket : m_ArchetypeBuckets)
		{
			if ((bucket.GetComponentTypesInThisBucket() & mask) == mask)
			{
				const size_t numberOfEntities = bucket.GetSize();

				ComponentUpdateParameters parameters{ world, bucket, 0, numberOfEntities };
				func(parameters, bucket.GetOrAccessComponentBucket<typename ComponentBuckets::ComponentType>()...);
			}
		}
	}

	template <typename... ComponentBuckets>
	void ForeachParallell(World& world, std::function<void(ComponentUpdateParameters&, ComponentBuckets&...)> func)
	{
		std::vector<std::unique_ptr<Job>> jobs;
		const std::unique_ptr<Job> parent = std::make_unique<Job>();
		ArchetypeMask mask = ArchetypeMaskFromComponentBucketsReadWrite<ComponentBuckets...>();

		const auto numberOfWorkers = JobEngine::GetInstance().GetNumberOfWorkers();

		for (auto& bucket : m_ArchetypeBuckets)
		{
			if ((bucket.GetComponentTypesInThisBucket() & mask) == mask)
			{
				const size_t numberOfEntities = bucket.GetSize();

				size_t numberOfJobs = std::max<size_t>(1, numberOfWorkers);

				if (numberOfEntities < numberOfJobs)
				{
					numberOfJobs = numberOfEntities;
				}

				const size_t entitiesPerJob = numberOfEntities / numberOfJobs;

				for (size_t i = 0; i < numberOfJobs; ++i)
				{
					const size_t start = i * entitiesPerJob;
					const int remainingEntities = std::max<int>(numberOfEntities - i * entitiesPerJob, 0);
					const size_t size = (remainingEntities > entitiesPerJob && i != numberOfJobs - 1)
											? entitiesPerJob
											: remainingEntities;
					const size_t end = start + size;

					auto fun = [&, start, end](Job& /*job*/) {
						PROFILE_SCOPE("bucket slice");
						ComponentUpdateParameters parameters{ world, bucket, start, end };
						func(parameters,
							bucket.GetOrAccessComponentBucket<typename ComponentBuckets::ComponentType>()...);
					};
					jobs.emplace_back(std::make_unique<Job>(fun, parent.get()));
					JobEngine::GetInstance().GetThreadWorker()->AddJob(jobs.back().get());
				}
			}
		}
		JobEngine::GetInstance().GetThreadWorker()->WaitForJob(parent.get());
	}

	template <typename Component> const Component& GetComponent(Entity entity) const
	{
		auto it = m_EntityToBucket.find(entity);
		assert(it != m_EntityToBucket.end());
		const auto& bucket = GetBucket(it->second);
		return bucket.GetComponentForEntity<Component>(entity);
	}

	template <typename Component> Component& AccessComponent(Entity entity)
	{
		return const_cast<Component&>(GetComponent<Component>(entity));
	}

	void MarkEntityToBeKilled(Entity entity);

	ComponentStorageRegistry& AccessComponentStorageRegistry();

	void serialize(SerializerBase& serializer);

	ArchetypeBucket& EnsureBucket(const ArchetypeMask& componentsInEntity);
	ArchetypeBucket& AccessBucket(ArchetypeHandle archetypeHandle);
	const ArchetypeBucket& GetBucket(ArchetypeHandle archetypeHandle) const;

private:
	void KillMarkedEntities();

	friend World;

	Entity m_NextEntity{ 0 };

	std::vector<Entity> m_EntitiesToBeKilled;
	std::vector<ArchetypeBucket> m_ArchetypeBuckets;
	std::unordered_map<Entity, ArchetypeHandle> m_EntityToBucket;

	ComponentStorageRegistry m_ComponentStorageRegistry;
};
