#include "network_to_simulation_system.h"
#include "ecs/component/core/simulation_input_component.h"
#include "ecs/component/network/network_match_component.h"
#include "ecs/component/network/network_to_simulation_input_component.h"
#include "ecs/simulation_input/network/tick_input.h"
#include "ecs/simulation_input/core/set_simulation_pause_state_input.h"

#include <ecs/world.h>

NetworkToSimulationSystem::NetworkToSimulationSystem() : System(NetworkToSimulationSystem::NAME)
{
}

void UpdateComponents(ComponentUpdateParameters& Parameters,
	ComponentBucket<NetworkToSimulationInputComponent>& NetworkToSimulationInputs)
{
	auto* pSimulationInput = Parameters.World.AccessEntityManager().AccessFirstComponent<SimulationInputComponent>();
	if (!pSimulationInput)
	{
		return;
	}
	for (size_t i = Parameters.Start; i < Parameters.End; i++)
	{
		auto& NetworkInputs = NetworkToSimulationInputs.AccessComponent(i)._NetworkToSimulationInputs.at(
			Parameters.World.GetSimulationFrameNumber() % NETWORK_FRAME_BUFFER_SIZE);
		auto& SimulationInputs = pSimulationInput->_SimulationInputs;
		while (!NetworkInputs.empty())
		{
			// Pop from network and move to simulation inputs
			auto Input = std::move(NetworkInputs.front());
			size_t SimulationFrame = Parameters.World.GetSimulationFrameNumber();
			if (Input->_TargetFrame != SimulationFrame)
			{
				int i = 0;
				assert(false);
			}
			NetworkInputs.erase(NetworkInputs.begin());
			SimulationInputs.emplace_back(std::move(Input));
		}
	}
}

ArchetypeReadWriteMaskPair NetworkToSimulationSystem::RegisterComponents(ComponentStorageRegistry& Registry)
{
	ArchetypeReadWriteMaskPair Pair = ArchetypeReadWriteMaskPairFromFunction(Registry, std::function(UpdateComponents));
	return System::RegisterComponents(Registry);
}

void NetworkToSimulationSystem::Initialize(World& World)
{
}

void NetworkToSimulationSystem::Update(World& World)
{
	// World.AccessEntityManager().ForeachParallell(World, std::function(UpdateComponents));
	World.AccessEntityManager().ForeachSerial(World, std::function(UpdateComponents));
}

void NetworkToSimulationSystem::Uninitialize(World& World)
{
}
