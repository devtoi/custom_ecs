#pragma once

#include "ecs/system/system.h"

class NetworkTickSystem final : public System
{
public:
	static constexpr std::string_view NAME = "NetworkTickSystem";
	NetworkTickSystem();

	ArchetypeReadWriteMaskPair RegisterComponents(ComponentStorageRegistry& registry) override;
	void Initialize(World& world) override;
	void Update(World& world) override;
	void Uninitialize(World& world) override;
};
