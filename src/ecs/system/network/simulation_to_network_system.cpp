#include "simulation_to_network_system.h"

#include "ecs/component/core/simulation_input_component.h"
#include "ecs/component/network/nakama_network_component.h"
#include "ecs/component/network/network_match_component.h"
#include "serializer/json/json_text_writer.h"
#include "ecs/world.h"

SimulationToNetworkSystem::SimulationToNetworkSystem() : System(SimulationToNetworkSystem::NAME)
{
}

namespace
{
	void UpdateComponents(ComponentUpdateParameters& Parameters, ComponentBucket<NetworkMatchComponent>& NetworkMatches)
	{
		auto* pNakamaNetwork = Parameters.World.AccessEntityManager().AccessFirstComponent<NakamaNetworkComponent>();
		if (!pNakamaNetwork)
		{
			return;
		}
		for (size_t i = Parameters.Start; i < Parameters.End; ++i)
		{
			auto& Match = NetworkMatches.AccessComponent(i);
			if (!Match._Match.matchId.empty())
			{
				auto& SimulationInputs = Match._OutboundInputs;
				while (!SimulationInputs.empty())
				{
					auto Input = std::move(SimulationInputs.front());
					SimulationInputs.erase(SimulationInputs.begin());
					JsonTextWriter Writer;
					Input->Serialize(Writer);
					NBytes data = Writer.GetJson().dump(4);
					pNakamaNetwork->_RealtimeClient->sendMatchData(
						Match._Match.matchId, static_cast<int64_t>(Input->GetID().GetID()), data);
				}
			}
		}
	}
}

ArchetypeReadWriteMaskPair SimulationToNetworkSystem::RegisterComponents(ComponentStorageRegistry& Registry)
{
	/* ArchetypeReadWriteMaskPair Pair = */
	ArchetypeReadWriteMaskPairFromFunction(Registry, std::function(UpdateComponents));
	// Pair.ComponentReadMask |= ArchetypeMaskFromComponents<MySecondComponent>();
	// Pair.ComponentWriteMask |= ArchetypeMaskFromComponents<MyThirdComponent>();
	// return Pair;
	return System::RegisterComponents(Registry);
}

void SimulationToNetworkSystem::Initialize(World& World)
{
}

void SimulationToNetworkSystem::Update(World& World)
{
	// World.AccessEntityManager().ForeachParallell(World, std::function(UpdateComponents));
	World.AccessEntityManager().ForeachSerial(World, std::function(UpdateComponents));
}

void SimulationToNetworkSystem::Uninitialize(World& World)
{
}
