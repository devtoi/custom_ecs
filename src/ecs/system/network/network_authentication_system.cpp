#include "network_authentication_system.h"
#include "spdlog/spdlog.h"

#include <ecs/world.h>

#include <ecs/component/network/nakama_network_component.h>
#include <ecs/component/network/network_login_info_component.h>

#include <utility>

NetworkAuthenticationSystem::NetworkAuthenticationSystem() : System(NetworkAuthenticationSystem::NAME)
{
}

void UpdateComponents(ComponentUpdateParameters& Parameters, ComponentBucket<NakamaNetworkComponent>& NakamaNetworks,
	ComponentBucket<NetworkLoginInfoComponent>& NetworkLoginInfos)
{
	auto loginFailedCallback = [](const NError& error) {
		spdlog::error("Nakama error: ({}) {}", error.code, error.message);
	};

	auto loginSucceededCallback = [&World = Parameters.World](NSessionPtr Session) {
		auto* pNakamaNetwork = World.AccessEntityManager().AccessFirstComponent<NakamaNetworkComponent>();
		spdlog::info("Connected\n\ttoken {}\n\tuserid {}\n\tusername {}\n\texpired {}\n\texpireTime {}",
			Session->getAuthToken(), Session->getUserId(), Session->getUsername(), Session->isExpired(),
			Session->getExpireTime());
		pNakamaNetwork->_Session = std::move(Session);
		pNakamaNetwork->_Client->tick();
		bool createStatus = true; // if the server should show the user as online to others.
		// define realtime client in your class as NRtClientPtr rtClient;
		pNakamaNetwork->_RealtimeClient = pNakamaNetwork->_Client->createRtClient(DEFAULT_PORT);
		// define listener in your class as NRtDefaultClientListener listener;
		// pNakamaNetwork->_Listener.setConnectCallback([]() { std::cout << "Socket connected" << std::endl; });
		pNakamaNetwork->_RealtimeClient->setListener(&pNakamaNetwork->_Listener);
		pNakamaNetwork->_RealtimeClient->connect(pNakamaNetwork->_Session, createStatus);
		pNakamaNetwork->_Client->tick();
	};

	for (size_t i = Parameters.Start; i < Parameters.End; ++i)
	{
		auto& NakamaNetwork = NakamaNetworks.AccessComponent(i);
		auto& NetworkLoginInfo = NetworkLoginInfos.AccessComponent(i);

		if (NetworkLoginInfo.Login)
		{
			NakamaNetwork._Client->authenticateEmail(NetworkLoginInfo.Email, NetworkLoginInfo.Password,
				NetworkLoginInfo.Username, NetworkLoginInfo.CreateAccount, {}, loginSucceededCallback,
				loginFailedCallback);
			NetworkLoginInfo.Password.clear();
			NakamaNetwork._Client->tick();
			NetworkLoginInfo.Login = false;
		}
	}
}

ArchetypeReadWriteMaskPair NetworkAuthenticationSystem::RegisterComponents(ComponentStorageRegistry& Registry)
{
	ArchetypeReadWriteMaskPair Pair = ArchetypeReadWriteMaskPairFromFunction(Registry, std::function(UpdateComponents));
	// Pair.ComponentWriteMask |= ArchetypeMaskFromComponents<NakamaNetworkComponent>();
	// return Pair;
	return System::RegisterComponents(Registry);
}

void NetworkAuthenticationSystem::Initialize(World& World)
{
	NLogger::initWithConsoleSink(NLogLevel::Debug);
	auto NakamaEntity = World.AccessEntityManager().CreateEntity(
		ArchetypeMaskFromComponents<NakamaNetworkComponent, NetworkLoginInfoComponent>());

	auto& NakamaNetwork = World.AccessEntityManager().AccessComponent<NakamaNetworkComponent>(NakamaEntity);
	NClientParameters Parameters;
	Parameters.serverKey = "kladdkakamedstortk";
	Parameters.host = "127.0.0.1";
	Parameters.port = 7350;
	NakamaNetwork._Client = createDefaultClient(Parameters);
	NakamaNetwork._Listener.SetWorld(&World);
}

void NetworkAuthenticationSystem::Update(World& World)
{
	// World.AccessEntityManager().ForeachParallell(World, std::function(UpdateComponents));
	World.AccessEntityManager().ForeachSerial(World, std::function(UpdateComponents));
}

void NetworkAuthenticationSystem::Uninitialize(World& World)
{
}
