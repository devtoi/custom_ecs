#include "network_lobby_system.h"
#include "ecs/component/network/network_match_component.h"
#include "ecs/component/network/nakama_network_component.h"
#include "ecs/component/network/network_player_component.h"
#include "ecs/component/network/network_to_simulation_input_component.h"

#include <ecs/world.h>

NetworkLobbySystem::NetworkLobbySystem() : System(NetworkLobbySystem::NAME)
{
}

ArchetypeReadWriteMaskPair NetworkLobbySystem::RegisterComponents(ComponentStorageRegistry& Registry)
{
	/* ArchetypeReadWriteMaskPair Pair =  */
	// ArchetypeReadWriteMaskPairFromFunction(Registry, std::function(UpdateComponents));
	// Pair.ComponentReadMask |= ArchetypeMaskFromComponents<MySecondComponent>();
	// Pair.ComponentWriteMask |= ArchetypeMaskFromComponents<MyThirdComponent>();
	// return Pair;
	Registry.RegisterComponent<NetworkPlayerComponent>();
	Registry.RegisterComponent<NetworkToSimulationInputComponent>();
	Registry.RegisterComponent<NetworkMatchComponent>();
	return System::RegisterComponents(Registry);
}

void NetworkLobbySystem::Initialize(World& World)
{
}

void NetworkLobbySystem::Update(World& World)
{
	// World.AccessEntityManager().ForeachParallell(World, std::function(UpdateComponents));
	// World.AccessEntityManager().ForeachSerial(World, std::function(UpdateComponents));
}

void NetworkLobbySystem::Uninitialize(World& World)
{
}
