#include "network_tick_system.h"

#include "ecs/component/network/nakama_network_component.h"
#include "ecs/world.h"
#include "spdlog/spdlog.h"

NetworkTickSystem::NetworkTickSystem() : System(NetworkTickSystem::NAME)
{
}

void UpdateComponents(ComponentUpdateParameters& Parameters, ComponentBucket<NakamaNetworkComponent>& NakamaNetworks)
{
	for (size_t i = Parameters.Start; i < Parameters.End; ++i)
	{
		auto& NakamaNetwork = NakamaNetworks.AccessComponent(i);
		NakamaNetwork._Client->tick();
		if (NakamaNetwork._RealtimeClient)
		{
			NakamaNetwork._RealtimeClient->tick();
		}
	}
}

ArchetypeReadWriteMaskPair NetworkTickSystem::RegisterComponents(ComponentStorageRegistry& Registry)
{
	ArchetypeReadWriteMaskPairFromFunction(Registry, std::function(UpdateComponents));
	return System::RegisterComponents(Registry);
}

void NetworkTickSystem::Initialize(World& World)
{
}

void NetworkTickSystem::Update(World& World)
{
	World.AccessEntityManager().ForeachSerial(World, std::function(UpdateComponents));
}

void NetworkTickSystem::Uninitialize(World& World)
{
}
