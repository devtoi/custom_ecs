#define NOMINMAX
#include "movement_system.h"
#include <algorithm>

#include "ecs/component/game/movement_settings_component.h"

#include <glm/geometric.hpp>

#include <ecs/archetype_bucket.h>
#include <ecs/component/position_component.h>
#include <ecs/component/tile_map_component.h>
#include <ecs/component/velocity_component.h>
#include <ecs/component/game/food_mover_component.h>
#include <ecs/component/game/home_tracker_component.h>
#include <ecs/component/game/pheromone_map_component.h>
#include <ecs/world.h>

MovementSystem::MovementSystem() : System(MovementSystem::NAME)
{
}

static const glm::ivec2 killbox_min{ 0, 0 };
static const glm::ivec2 killbox_max{ MAP_SIZE_WIDTH_TILES, MAP_SIZE_HEIGHT_TILES };

std::array<int64_t, MovementSystem::NeighbourCount> MovementSystem::DetermineProbabilities(
	const FoodMoverComponent& FoodMover, const HomeTrackerComponent& HomeTracker, const PositionComponent& Position,
	const TileMapComponent& TileMap, const PheromonMapComponent& PheromonMap, const VelocityComponent& Velocity,
	World& World)
{
	std::array<int64_t, MovementSystem::NeighbourCount> Probabilities{ 0 };
	bool HasFood = FoodMover.FoodAmount > 0;
	int DistanceToHomeFromCurrent =
		static_cast<int>(glm::distance(glm::vec2(HomeTracker.PositionOfHome), glm::vec2(Position.position)));
	auto& MovementSettings = *World.AccessEntityManager().AccessFirstComponent<MovementSettingsComponent>();
	for (int c = 0; c < MovementSystem::NeighbourCount; ++c)
	{
		int x = (c % 3) - 1;
		int y = (c / 3) - 1;
		if (y == 0 && x == 0)
		{
			continue;
		}
		glm::ivec2 Target = glm::ivec2(Position.position.x + x, Position.position.y + y);
		if (Target.x >= 0 && Target.x < TileMap.map.size() && Target.y >= 0 && Target.y < TileMap.map.at(0).size())
		{
			auto TTT = TileMap.map[Target.x][Target.y];
			int DistanceToHomeFromTarget =
				static_cast<int>(glm::distance(glm::vec2(HomeTracker.PositionOfHome), glm::vec2(Target)));
			if (TTT != TileType::Impassable)
			{
				auto& Probability = Probabilities.at(c);
				Probability = MovementSettings.BaseProbability;

				float DiffFromCurrentDirection = glm::distance(glm::vec2(x, y), glm::vec2(Velocity.direction)) * 10;
				//				Probability += std::pow((30 - DiffFromCurrentDirection), 4);
				Probability += MovementSettings.DirectionWeight / std::max(DiffFromCurrentDirection, 1.0f);

				int DistanceDiff = DistanceToHomeFromCurrent - DistanceToHomeFromTarget;
				if (HasFood)
				{
					Probability += DistanceDiff * MovementSettings.DistanceFromHomeWeightWhenHasFood;
					Probability *= MovementSettings.HomeBaseMult +
								   static_cast<int64_t>(PheromonMap.HomePheromoneTiles[Target.x][Target.y] *
														MovementSettings.HomePheromoneMult * 1000);
				}
				else
				{
					if (TTT == TileType::Food)
					{
						Probability += MovementSettings.FoodTileBaseProbability;
					}
					Probability *= MovementSettings.FoodBaseMult +
								   static_cast<int64_t>(PheromonMap.FoodPheromoneTiles[Target.x][Target.y] *
														MovementSettings.FoodPheromoneMult * 1000);
					Probability -= static_cast<int64_t>(PheromonMap.HomePheromoneTiles[Target.x][Target.y] *
														MovementSettings.HomePheromoneMultWhenScouting * 1000);
					Probability -= DistanceDiff;
				}

				int64_t min = 0;
				Probability = std::max(min, Probability);
			}
		}
	}
	return Probabilities;
}

void UpdateComponents(ComponentUpdateParameters& Parameters, ComponentBucket<PositionComponent>& Positions,
	ComponentBucket<VelocityComponent>& Velocities, const ComponentBucket<FoodMoverComponent>& FoodMovers,
	const ComponentBucket<HomeTrackerComponent>& HomeTrackers)
{
	World& World = Parameters.World;

	std::mt19937 generator(World.GetSimulationFrameNumber() + Parameters.Start);

	const auto& TileMap = *World.AccessEntityManager().AccessFirstComponent<TileMapComponent>();
	const auto& PheromonMap = *World.AccessEntityManager().AccessFirstComponent<PheromonMapComponent>();

	for (size_t i = Parameters.Start; i < Parameters.End; ++i)
	{
		PositionComponent& Position = Positions.AccessComponent(i);
		VelocityComponent& Velocity = Velocities.AccessComponent(i);

		Velocity.left -= Velocity.speed * World.GetSimulationTimeStep();

		if (Velocity.left <= decltype(Velocity.left){ 0 })
		{
			while (Velocity.left <= decltype(Velocity.left){ 0 })
			{
				Position.position += Velocity.direction;

				const FoodMoverComponent& FoodMover = FoodMovers.GetComponent(i);
				const HomeTrackerComponent& HomeTracker = HomeTrackers.GetComponent(i);

				auto Probabilities = MovementSystem::DetermineProbabilities(
					FoodMover, HomeTracker, Position, TileMap, PheromonMap, Velocity, World);

				bool AllZero = true;
				for (auto& Probability : Probabilities)
				{
					if (Probability > 0)
					{
						AllZero = false;
						break;
					}
				}
				if (!AllZero)
				{
					std::discrete_distribution<> RandomTile(Probabilities.begin(), Probabilities.end());
					int RandomNeighbourIndex = RandomTile(generator);
					int RandomNeighbourX = (RandomNeighbourIndex % 3) - 1;
					int RandomNeighbourY = (RandomNeighbourIndex / 3) - 1;
					Velocity.direction.x = RandomNeighbourX;
					Velocity.direction.y = RandomNeighbourY;
				}

				Velocity.left += TILE_SUB_SIZE; // + (std::abs(vel.direction.x) + std::abs(vel.direction.y));
				Velocity.JustSwitchedTile = true;
			}
		}
		else
		{
			Velocity.JustSwitchedTile = false;
		}
	}
}

ArchetypeReadWriteMaskPair MovementSystem::RegisterComponents(ComponentStorageRegistry& registry)
{
	ArchetypeReadWriteMaskPair Pair = ArchetypeReadWriteMaskPairFromFunction(registry, std::function(UpdateComponents));
	Pair.ComponentReadMask |=
		ArchetypeMaskFromComponents<TileMapComponent, PheromonMapComponent, MovementSettingsComponent>();
	registry.RegisterComponent<MovementSettingsComponent>();
	return Pair;
}

void MovementSystem::Initialize(World& World)
{
	World.AccessEntityManager().CreateEntity(ArchetypeMaskFromComponents<MovementSettingsComponent>());
}

void MovementSystem::Update(World& world)
{
	world.AccessEntityManager().ForeachParallell(world, std::function(UpdateComponents));
}
