#pragma once

#include <ecs/system/system.h>

class ImguiTileMapEditorSystem : public System
{
public:
	static constexpr std::string_view NAME = "ImguiTileMapEditorSystem";
	ImguiTileMapEditorSystem();

	ArchetypeReadWriteMaskPair RegisterComponents(ComponentStorageRegistry& registry) override;
	void Update(World& world) override;
};
