#include "imgui_tilemap_editor_system.h"

#include <SDL2/SDL_mouse.h>
#include <imgui.h>
#include <glm/geometric.hpp>

#include <ecs/component/utility/tile_map_brush_component.h>
#include <ecs/component/camera_component.h>
#include <ecs/world.h>

ImguiTileMapEditorSystem::ImguiTileMapEditorSystem() : System(ImguiTileMapEditorSystem::NAME)
{
}

void UpdateSystem(ComponentUpdateParameters& Params, ComponentBucket<TileMapBrushComponent>& TileMapBrushes,
	const ComponentBucket<CameraComponent>& Cameras)
{
	ImGui::Begin("Tile map editor");
	for (size_t i = Params.Start; i < Params.End; ++i)
	{
		auto& TileMapBrush = TileMapBrushes.AccessComponent(i);

		ImGui::SliderInt("Brush Size: ", &TileMapBrush.BrushSize, 1, 20);

		int TTT = static_cast<int>(TileMapBrush.TileTypeToDraw);
		constexpr int TileTypeCount = static_cast<int>(TileType::Count);
		const char* const TileTypes[] = { "Passable", "Impassable", "Food" };
		ImGui::Combo("Brush", &TTT, TileTypes, TileTypeCount, 10);
		TileMapBrush.TileTypeToDraw = static_cast<TileType>(TTT);

		auto* pTileMap = Params.World.AccessEntityManager().AccessFirstComponent<TileMapComponent>();
		if (!pTileMap)
		{
			ImGui::End();
			return;
		}

		glm::ivec2 MousePos;
		auto MouseState = SDL_GetMouseState(&MousePos.x, &MousePos.y);
		ImGui::Text("Mouse: %d, %d", MousePos.x, MousePos.y);

		const auto& Camera = Cameras.GetComponent(i);
		glm::ivec2 WorldPos = MousePos + glm::ivec2(Camera.Offset);
		WorldPos.x *= 1.0f / Camera.Zoom;
		WorldPos.y *= 1.0f / Camera.Zoom;
		ImGui::Text("WorldPos: %d, %d", WorldPos.x, WorldPos.y);

		glm::ivec2 TilePos = WorldPos / glm::ivec2(TILE_SIZE);
		ImGui::Text("TilePos: %d, %d", TilePos.x, TilePos.y);

		if (MouseState & SDL_BUTTON_LEFT && !ImGui::GetIO().WantCaptureMouse)
		{
			ImGui::Text("Mouse Down Pos: %d, %d", MousePos.x, MousePos.y);
			for (int x = TilePos.x - TileMapBrush.BrushSize; x < TilePos.x + TileMapBrush.BrushSize; ++x)
			{
				for (int y = TilePos.y - TileMapBrush.BrushSize; y < TilePos.y + TileMapBrush.BrushSize; ++y)
				{
					if (!pTileMap->IsPosInside(glm::ivec2(x, y)) ||
						glm::distance(glm::vec2(TilePos), glm::vec2(x, y)) >= TileMapBrush.BrushSize)
					{
						continue;
					}
					pTileMap->map[x][y] = TileMapBrush.TileTypeToDraw;
					pTileMap->_TileNumber[x][y] += 1;
				}
			}
			pTileMap->_Dirty = true;
		}
	}
	ImGui::End();
}

ArchetypeReadWriteMaskPair ImguiTileMapEditorSystem::RegisterComponents(ComponentStorageRegistry& registry)
{
	ArchetypeReadWriteMaskPair Pair = ArchetypeReadWriteMaskPairFromFunction(registry, std::function(UpdateSystem));
	return Pair;
}

void ImguiTileMapEditorSystem::Update(World& world)
{
	world.AccessEntityManager().ForeachSerial(world, std::function(UpdateSystem));
}
