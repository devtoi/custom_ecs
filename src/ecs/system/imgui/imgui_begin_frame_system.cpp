#include "imgui_begin_frame_system.h"

#include <imgui/misc/single_file/imgui_single_file.h>
#include <implot/implot.h>

#include <spdlog/spdlog.h>

#include "../../world.h"
#include "backends/imgui_impl_vulkan.h"
#include <ecs/component/rendering/renderer_component.h>
#include <ecs/component/rendering/imgui_component.h>
#include <ecs/component/camera_component.h>

IMGUIBeginFrameSystem::IMGUIBeginFrameSystem() : System(IMGUIBeginFrameSystem::NAME)
{
}

static void check_vk_result(VkResult err)
{
	if (err == 0)
	{
		return;
	}

	spdlog::error("[vulkan] Error: vk::Result = {0}", static_cast<int>(err));
	if (err < 0)
	{
		throw std::runtime_error("[vulkan] Error");
	}
}

std::unique_ptr<vk::raii::DescriptorPool> createIMGUIDescriptorPool(vk::raii::Device& device)
{
	vk::DescriptorPoolSize pool_sizes[] = { { vk::DescriptorType::eSampler, 1000 },
		{ vk::DescriptorType::eCombinedImageSampler, 1000 }, { vk::DescriptorType::eSampledImage, 1000 },
		{ vk::DescriptorType::eStorageImage, 1000 }, { vk::DescriptorType::eUniformTexelBuffer, 1000 },
		{ vk::DescriptorType::eStorageTexelBuffer, 1000 }, { vk::DescriptorType::eUniformBuffer, 1000 },
		{ vk::DescriptorType::eStorageBuffer, 1000 }, { vk::DescriptorType::eUniformBufferDynamic, 1000 },
		{ vk::DescriptorType::eStorageBufferDynamic, 1000 }, { vk::DescriptorType::eInputAttachment, 1000 } };

	vk::DescriptorPoolCreateInfo pool_info = {};
	pool_info.flags = vk::DescriptorPoolCreateFlagBits::eFreeDescriptorSet;
	pool_info.maxSets = 1000 * IM_ARRAYSIZE(pool_sizes);
	pool_info.poolSizeCount = (uint32_t)IM_ARRAYSIZE(pool_sizes);
	pool_info.pPoolSizes = pool_sizes;

	return std::make_unique<vk::raii::DescriptorPool>(device, pool_info);
}

std::vector<vk::raii::CommandBuffer> CreateCommandBuffers(RendererComponent& render)
{
	vk::CommandBufferAllocateInfo allocInfo{};
	allocInfo.commandPool = **render._commandPool;
	allocInfo.level = vk::CommandBufferLevel::ePrimary;
	allocInfo.commandBufferCount = render._swapChainImageViews.size();
	return std::move(vk::raii::CommandBuffers(*render._device, allocInfo));
}

void OnSwapchainRecreated(World& world, RendererComponent& render)
{
	auto* pImGui = world.AccessEntityManager().AccessFirstComponent<ImGuiComponent>();
	if (!pImGui)
	{
		return;
	}

	pImGui->CommandBuffers = CreateCommandBuffers(render);
}

ArchetypeReadWriteMaskPair IMGUIBeginFrameSystem::RegisterComponents(ComponentStorageRegistry& registry)
{
	ArchetypeReadWriteMaskPair Pair = System::RegisterComponents(registry);
	Pair.ComponentWriteMask |= ArchetypeMaskFromComponents<RendererComponent>();
	registry.RegisterComponent<ImGuiComponent>();
	return Pair;
}

void IMGUIBeginFrameSystem::Initialize(World& world)
{
	auto* pRenderer = world.AccessEntityManager().AccessFirstComponent<RendererComponent>();
	if (!pRenderer)
	{
		return;
	}
	RendererComponent& render = *pRenderer;

	render.OnRecreateSwapChain.emplace_back(OnSwapchainRecreated);

	ArchetypeMask mask = ArchetypeMaskFromComponents<ImGuiComponent>();
	Entity entity = world.AccessEntityManager().CreateEntity(mask);
	auto& imguiComponent = world.AccessEntityManager().AccessComponent<ImGuiComponent>(entity);
	imguiComponent.DescriptorPool = createIMGUIDescriptorPool(*pRenderer->_device);
	imguiComponent.CommandBuffers = CreateCommandBuffers(render);

	// Setup Dear ImGui context
	IMGUI_CHECKVERSION();
	ImGui::CreateContext();
	ImPlot::CreateContext();
	ImGuiIO& io = ImGui::GetIO();
	(void)io;

	// Setup Dear ImGui style
	ImGui::StyleColorsDark();

	// Setup Platform/Renderer backends
	ImGui_ImplSDL2_InitForVulkan(render._window);
	ImGui_ImplVulkan_InitInfo init_info = {};
	init_info.Instance = **render._instance;
	init_info.PhysicalDevice = **render._physicalDevice;
	init_info.Device = **render._device;
	init_info.QueueFamily = render._graphicsQueueIndex;
	init_info.Queue = **render._graphicsQueue;
	init_info.PipelineCache = nullptr;
	init_info.DescriptorPool = **imguiComponent.DescriptorPool;
	init_info.Allocator = nullptr;
	init_info.MinImageCount = 2;
	init_info.ImageCount = render._swapChainImageViews.size();
	init_info.CheckVkResultFn = check_vk_result;
	ImGui_ImplVulkan_Init(&init_info, **render._renderPass);

	// Upload Fonts
	{
		pRenderer->_commandPool->reset();

		// Use any command queue
		auto& command_buffer = imguiComponent.CommandBuffers[0];

		vk::CommandBufferBeginInfo begin_info = {};
		begin_info.flags |= vk::CommandBufferUsageFlagBits::eOneTimeSubmit;
		command_buffer.begin(begin_info);

		ImGui_ImplVulkan_CreateFontsTexture(*command_buffer);

		vk::SubmitInfo end_info = {};
		end_info.commandBufferCount = 1;
		end_info.pCommandBuffers = &*command_buffer;
		command_buffer.end();

		pRenderer->_graphicsQueue->submit(end_info);

		pRenderer->_device->waitIdle();
		ImGui_ImplVulkan_DestroyFontUploadObjects();
	}
}

void IMGUIBeginFrameSystem::Uninitialize(World& world)
{
	const auto* pRender = world.AccessEntityManager().GetFirstComponent<RendererComponent>();
	pRender->_graphicsQueue->waitIdle();
	ImGui_ImplVulkan_Shutdown();
	ImGui_ImplSDL2_Shutdown();
	ImGui::DestroyContext();
	ImPlot::DestroyContext();
}

void IMGUIBeginFrameSystem::Update(World& world)
{
	auto* pRenderer = world.AccessEntityManager().AccessFirstComponent<RendererComponent>();
	if (!pRenderer)
	{
		return;
	}

	// Start the Dear ImGui frame
	ImGui_ImplVulkan_NewFrame();
	ImGui_ImplSDL2_NewFrame(pRenderer->_window);
	ImGui::NewFrame();
}
