#pragma once

#include <ecs/system/system.h>

class ImguiCameraMoverSystem : public System
{
public:
	static constexpr std::string_view NAME = "ImguiCameraMoverSystem";
	ImguiCameraMoverSystem();

	ArchetypeReadWriteMaskPair RegisterComponents(ComponentStorageRegistry& registry) override;
	void Update(World& world) override;
};
