#include "imgui_time_control_system.h"
#include "ecs/component/core/simulation_input_component.h"
#include "ecs/simulation_input/core/set_simulation_pause_state_input.h"
#include "ecs/utils/simulation_io.h"

#include <imgui.h>

#include <ecs/world.h>

ImguiTimeControlSystem::ImguiTimeControlSystem() : System(ImguiTimeControlSystem::NAME)
{
}

void ImguiTimeControlSystem::Update(World& World)
{
	ImGui::Begin("Time control");

	const float RealDelta = World.GetRealTimeDelta();
	ImGui::Text("Current fps %.0f", 1 / RealDelta);

	ImGui::Text("Visualization Frame %zu", World.GetVisualizationFrameNumber());
	ImGui::Text("Simulation Frame %zu/%zu", World.GetSimulationFrameNumber(), World.GetTargetSimulationFrameNumber());

	if (ImGui::Button("TogglePause"))
	{
		auto* pInput = World.AccessEntityManager().AccessFirstComponent<SimulationInputComponent>();
		if (pInput)
		{
			if (World.IsSimulationPaused())
			{
				SimulationIO::RecordInput(World, UnpauseSimulationInput{});
				World.SetSimulationPauseState(false);
			}
			else
			{
				SimulationIO::RecordInput(World, PauseSimulationInput{});
			}
		}
	}
	{
		float TimeStep = static_cast<float>(World.GetSimulationTimeStep());
		ImGui::SliderFloat("Delta", &TimeStep, 0.0f, 10.0f, "%.3f", ImGuiSliderFlags_::ImGuiSliderFlags_Logarithmic);
		World.SetSimulationTimeStep(World::SimulationTimeUnit{ TimeStep });
	}

	{
		int StepsPerFrame = static_cast<int>(World.GetSimulationStepsPerFrame());
		ImGui::SliderInt(
			"StepsPerFrame", &StepsPerFrame, 0, 1000, "%d", ImGuiSliderFlags_::ImGuiSliderFlags_Logarithmic);
		World.SetSimulationStepsPerFrame(StepsPerFrame);
	}

	{
		int Speed = static_cast<int>(World.GetSimulationSpeed());
		ImGui::SliderInt("Simulation Speed", &Speed, 1, 1000, "%d", ImGuiSliderFlags_::ImGuiSliderFlags_Logarithmic);
		World.SetSimulationSpeed(Speed);
	}

	ImGui::End();
}
