#pragma once

#include <ecs/system/system.h>

class ImguiSystemsVisualizerSystem final : public System
{
public:
	static constexpr std::string_view NAME = "ImguiSystemsVisualizerSystem";
	ImguiSystemsVisualizerSystem();

	void Update(World& world) override;
};
