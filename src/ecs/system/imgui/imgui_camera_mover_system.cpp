#include "imgui_camera_mover_system.h"

#include <imgui.h>

#include <ecs/world.h>
#include <ecs/component/camera_component.h>

ImguiCameraMoverSystem::ImguiCameraMoverSystem() : System(ImguiCameraMoverSystem::NAME)
{
}

void UpdateComponents(ComponentUpdateParameters& Params, ComponentBucket<CameraComponent>& Cameras)
{
	ImGui::Begin("Camera");
	for (size_t i = Params.Start; i < Params.End; ++i)
	{
		auto& Camera = Cameras.AccessComponent(i);
		ImGui::SliderFloat("OffsetX", &Camera.Offset.x, -500, 500);
		ImGui::SliderFloat("OffsetY", &Camera.Offset.y, -500, 500);
		ImGui::SliderFloat("Zoom", &Camera.Zoom, 3.0f, 0.1f);
	}
	ImGui::End();
}

ArchetypeReadWriteMaskPair ImguiCameraMoverSystem::RegisterComponents(ComponentStorageRegistry& registry)
{
	ArchetypeReadWriteMaskPair Pair = ArchetypeReadWriteMaskPairFromFunction(registry, std::function(UpdateComponents));
	return Pair;
}

void ImguiCameraMoverSystem::Update(World& world)
{
	world.AccessEntityManager().ForeachSerial(world, std::function(UpdateComponents));
}
