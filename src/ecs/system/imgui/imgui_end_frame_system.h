#pragma once

#include "../system.h"

class IMGUIEndFrameSystem final : public System
{
public:
	static constexpr std::string_view NAME = "IMGUIEndFrameSystem";
	IMGUIEndFrameSystem();

	void Initialize(World& world) override;

	void Update(World& world) override;
};
