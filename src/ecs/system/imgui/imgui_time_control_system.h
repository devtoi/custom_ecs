#pragma once

#include <ecs/system/system.h>

class ImguiTimeControlSystem final : public System
{
public:
	static constexpr std::string_view NAME = "ImguiTimeControlSystem";
	ImguiTimeControlSystem();

	void Update(World& world) override;
};
