#pragma once

#include <vector>
#include <chrono>
#include <random>

#include <ecs/system/system.h>

struct ProfilerEntry;

class ImguiProfilingDataSystem : public System
{
public:
	static constexpr std::string_view NAME = "ImguiProfilingDataSystem";
	ImguiProfilingDataSystem();

	ArchetypeReadWriteMaskPair RegisterComponents(ComponentStorageRegistry& registry) override;
	void Initialize(World& world) override;
	void Update(World& world) override;
};
