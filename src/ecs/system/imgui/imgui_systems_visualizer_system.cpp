#include "imgui_systems_visualizer_system.h"

#include <imgui.h>

#include <ecs/world.h>

ImguiSystemsVisualizerSystem::ImguiSystemsVisualizerSystem() : System(ImguiSystemsVisualizerSystem::NAME)
{
}

void ImguiSystemsVisualizerSystem::Update(World&  /*World*/)
{
	ImGui::Begin("Systems");
	ImGui::TreePush();
	// for (const auto& System : World.GetSystems())
	// {
	// 	if (ImGui::TreeNode(System->GetName().c_str()))
	// 	{
	// 		ImGui::Text("Read : %s", System->GetComponentReadMask().to_string().c_str());
	// 		ImGui::Text("Write: %s", System->GetComponentWriteMask().to_string().c_str());
	// 		ImGui::TreePop();
	// 	}
	// }
	ImGui::TreePop();
	ImGui::End();
}
