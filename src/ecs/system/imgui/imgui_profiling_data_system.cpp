#include "imgui_profiling_data_system.h"
#include <chrono>
#include <imgui.h>
#include <implot.h>
#include <implot_internal.h>

#include <ecs/world.h>
#include <ecs/component/imgui/imgui_profiling_data_component.h>
#include <profiler/profiler.h>
#include <string/string_database.h>

ImguiProfilingDataSystem::ImguiProfilingDataSystem() : System(ImguiProfilingDataSystem::NAME)
{
}

ArchetypeReadWriteMaskPair ImguiProfilingDataSystem::RegisterComponents(ComponentStorageRegistry& registry)
{
	registry.RegisterComponent<ImguiProfilingDataComponent>();
	ArchetypeReadWriteMaskPair Pair;
	Pair.ComponentWriteMask |= ArchetypeMaskFromComponents<ImguiProfilingDataComponent>();
	return Pair;
}

void ImguiProfilingDataSystem::Initialize(World& world)
{
	ImPlot::GetStyle().PlotDefaultSize.y = 170;
	world.AccessEntityManager().CreateEntity(ArchetypeMaskFromComponents<ImguiProfilingDataComponent>());
	auto* pImguiProfilingData = world.AccessEntityManager().AccessFirstComponent<ImguiProfilingDataComponent>();
	pImguiProfilingData->_FrameTimes.resize(ImguiProfilingDataComponent::MAX_FRAME_TIMES, 0);
}

void ImguiProfilingDataSystem::Update(World& world)
{
	auto* pImguiProfilingData = world.AccessEntityManager().AccessFirstComponent<ImguiProfilingDataComponent>();
	if (!pImguiProfilingData)
	{
		return;
	}
	auto& FrameTimes = pImguiProfilingData->_FrameTimes;
	FrameTimes.at(world.GetVisualizationFrameNumber() % FrameTimes.size()) = world.GetRealTimeDelta();

	ImGui::Begin("Profiling");

	static bool RenderProfilingData = false;
	static bool ProfilerRecord = false;

	ImGui::Checkbox("Record", &ProfilerRecord);
	Profiler::SetRecording(ProfilerRecord);

	ImGui::Checkbox("Render Profiling Data", &RenderProfilingData);
	if (RenderProfilingData)
	{
		std::chrono::high_resolution_clock::time_point MinTime = std::chrono::high_resolution_clock::time_point::max();

		for (auto FrameStart : Profiler::GetFrameStarts())
		{
			MinTime = std::min(MinTime, FrameStart);
		}
		if (MinTime != std::chrono::high_resolution_clock::time_point::max() && ImPlot::BeginPlot("Profiling data"))
		{
			// get ImGui window DrawList
			ImDrawList* draw_list = ImPlot::GetPlotDrawList();
			// begin plot item
			if (ImPlot::BeginItem("test"))
			{
				int c = 0;

				int64_t LastTime = 0;

				auto* pFont = ImGui::GetCurrentContext()->Font;

				size_t MaxNumberOfThreads = 0;
				for (const auto& Frame : Profiler::GetPreviousFrameCollections())
				{
					size_t Row = 0;
					for (const auto& Thread : Frame.PerThreadData)
					{
						Row++;
						for (const auto& Entry : Thread)
						{
							auto color = IM_COL32(Entry.Tag.GetID() % 256, (Entry.Tag.GetID() + 128) % 256,
								(Entry.Tag.GetID() + 64) % 256, 255);
							auto Start = std::chrono::duration<double>(Entry.Start - MinTime).count();
							auto End = std::chrono::duration<double>(Entry.End - MinTime).count();
							if (LastTime < End)
							{
								LastTime = End;
							}
							auto LowerLeft = ImPlot::PlotToPixels(ImVec2(Start, Row - 1));
							auto UpperRight = ImPlot::PlotToPixels(ImVec2(End, Row));
							draw_list->AddRectFilled(LowerLeft, UpperRight, color);
							auto Size = ImVec4(LowerLeft.x, LowerLeft.y, UpperRight.x, UpperRight.y);
							auto Width = UpperRight.x - LowerLeft.x;
							if (Width >= 5.0)
							{
								draw_list->AddText(pFont, ImGui::GetFontSize(), ImVec2(LowerLeft.x, UpperRight.y),
									IM_COL32_WHITE, std::string(StringDatabase::LookupString(Entry.Tag)).c_str(),
									nullptr, UpperRight.x - LowerLeft.x, nullptr);
							}
						}
					}
					MaxNumberOfThreads = std::max(MaxNumberOfThreads, Row);
				}
				ImPlot::FitPoint(ImPlotPoint(0, 0));
				ImPlot::FitPoint(ImPlotPoint(LastTime, MaxNumberOfThreads));

				// end plot item
				ImPlot::EndItem();
			}
			ImPlot::EndPlot();
		}
	}

	if (ImPlot::BeginPlot("Frame times"))
	{
		ImPlot::SetupAxis(ImAxis_::ImAxis_X1, "Frame", ImPlotAxisFlags_::ImPlotAxisFlags_AutoFit);
		ImPlot::SetupAxis(ImAxis_::ImAxis_Y1, "Seconds", ImPlotAxisFlags_::ImPlotAxisFlags_AutoFit);
		ImPlot::PlotLine("Frame times", FrameTimes.data(), FrameTimes.size());

		ImPlot::EndPlot();
	}
	ImGui::End();
}
