#include "imgui_home_info_system.h"

#include "ecs/component/core/simulation_input_component.h"
#include "ecs/simulation_input/game/spawn_ant_input.h"
#include "ecs/utils/simulation_io.h"
#include "imgui.h"

#include <ecs/world.h>
#include "ecs/component/game/food_storage_component.h"

ImguiHomeInfoSystem::ImguiHomeInfoSystem() : System(ImguiHomeInfoSystem::NAME)
{
}

void UpdateComponents(ComponentUpdateParameters& Params, const ComponentBucket<FoodStorageComponent>& FoodStorages)
{
	ImGui::Begin("Home");
	for (size_t i = Params.Start; i < Params.End; i++)
	{
		const auto& FoodStorage = FoodStorages.GetComponent(i);
		ImGui::Text("Food: %d", FoodStorage.FoodAmount);
		if (ImGui::Button("Spawn ant"))
		{
			SimulationIO::RecordInput(Params.World, SpawnAntInput{});
		}
	}
	ImGui::End();
}

ArchetypeReadWriteMaskPair ImguiHomeInfoSystem::RegisterComponents(ComponentStorageRegistry& Registry)
{
	ArchetypeReadWriteMaskPair Pair = ArchetypeReadWriteMaskPairFromFunction(Registry, std::function(UpdateComponents));
	return Pair;
}

void ImguiHomeInfoSystem::Update(World& World)
{
	World.AccessEntityManager().ForeachSerial(World, std::function(UpdateComponents));
}
