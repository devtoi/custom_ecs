#include "imgui_save_system.h"

#include <imgui.h>

#include <ecs/world.h>
#include <ecs/component/persistance/save_component.h>
#include <ecs/component/persistance/load_component.h>

ImguiSaveSystem::ImguiSaveSystem() : System(ImguiSaveSystem::NAME)
{
}

void ImguiSaveSystem::Update(World& World)
{
	ImGui::Begin("Persistance");
	if (ImGui::Button("Save"))
	{
		World.AccessEntityManager().AccessFirstComponent<SaveComponent>()->Save = true;
	}
	if (ImGui::Button("Load"))
	{
		World.AccessEntityManager().AccessFirstComponent<LoadComponent>()->RunLoad = true;
	}
	ImGui::End();
}
