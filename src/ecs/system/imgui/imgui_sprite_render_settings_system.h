#pragma once

#include <ecs/system/system.h>

class ImguiSpriteRenderSettingsSystem final : public System
{
public:
	static constexpr std::string_view NAME = "ImguiSpriteRenderSettingsSystem";
	ImguiSpriteRenderSettingsSystem();

	ArchetypeReadWriteMaskPair RegisterComponents(ComponentStorageRegistry& registry) override;
	void Update(World& world) override;
};
