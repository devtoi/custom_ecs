#include "imgui_network_lobby_system.h"

#include <ecs/world.h>
#include "ecs/component/network/nakama_network_component.h"
#include "ecs/component/network/network_match_component.h"
#include "ecs/component/network/network_to_simulation_input_component.h"
#include "ecs/utils/network_util.h"
#include "imgui.h"

ImguiNetworkLobbySystem::ImguiNetworkLobbySystem() : System(ImguiNetworkLobbySystem::NAME)
{
}

NMatchListPtr gMatchList = nullptr;

void UpdateComponents(
	ComponentUpdateParameters& Parameters, const ComponentBucket<NakamaNetworkComponent>& NakamaNetworkComponents)
{
	for (size_t i = Parameters.Start; i < Parameters.End; ++i)
	{
		const auto& NakamaNetwork = NakamaNetworkComponents.GetComponent(i);
		if (NakamaNetwork._Session)
		{
			ImGui::Begin("Network lobby");
			if (ImGui::Button("Start match"))
			{
				NetworkUtil::CreateMatch(Parameters.World);
			}
			if (ImGui::Button("Fetch matches"))
			{
				NakamaNetwork._Client->listMatches(NakamaNetwork._Session, opt::nullopt, opt::nullopt, opt::nullopt,
					opt::nullopt, opt::nullopt,
					[&World = Parameters.World](NMatchListPtr MatchList) { gMatchList = MatchList; });
			}
			if (gMatchList)
			{
				ImGui::TreePush();
				for (auto& Match : gMatchList->matches)
				{
					// if (ImGui::TreeNode(Match.label.c_str()))
					// {
					// 	ImGui::Text("%s", Match.matchId.c_str());
					if (ImGui::Button("Join"))
					{
						NetworkUtil::JoinMatch(Parameters.World, Match.matchId);
					}
					// 	ImGui::TreePop();
					// }
				}
				ImGui::TreePop();
			}
			ImGui::End();
		}
	}
}

ArchetypeReadWriteMaskPair ImguiNetworkLobbySystem::RegisterComponents(ComponentStorageRegistry& Registry)
{
	// ArchetypeReadWriteMaskPair Pair = ArchetypeReadWriteMaskPairFromFunction(Registry,
	// std::function(UpdateComponents)); Pair.ComponentReadMask |= ArchetypeMaskFromComponents<MySecondComponent>();
	// Pair.ComponentWriteMask |= ArchetypeMaskFromComponents<MyThirdComponent>();
	// return Pair;
	return System::RegisterComponents(Registry);
}

void ImguiNetworkLobbySystem::Initialize(World& World)
{
}

void ImguiNetworkLobbySystem::Update(World& World)
{
	// World.AccessEntityManager().ForeachParallell(World, std::function(UpdateComponents));
	World.AccessEntityManager().ForeachSerial(World, std::function(UpdateComponents));
}

void ImguiNetworkLobbySystem::Uninitialize(World& World)
{
}
