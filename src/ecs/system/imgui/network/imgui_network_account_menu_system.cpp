#include "imgui_network_account_menu_system.h"

#include "imgui.h"
#include "imgui_stdlib.h"

#include <ecs/world.h>
#include "ecs/component/network/network_login_info_component.h"
#include "ecs/component/network/nakama_network_component.h"

ImguiNetworkAccountMenuSystem::ImguiNetworkAccountMenuSystem() : System(ImguiNetworkAccountMenuSystem::NAME)
{
}

void UpdateComponents(ComponentUpdateParameters& Parameters,
	ComponentBucket<NetworkLoginInfoComponent>& NetworkLoginInfos,
	ComponentBucket<NakamaNetworkComponent>& NakamaNetworks)
{
	for (size_t i = Parameters.Start; i < Parameters.End; ++i)
	{
		auto& NetworkLoginInfo = NetworkLoginInfos.AccessComponent(i);
		auto& NakamaNetwork = NakamaNetworks.AccessComponent(i);
		ImGui::Begin("Network");
		if (!NakamaNetwork._Session)
		{
			ImGui::InputText("Email", &NetworkLoginInfo.Email);
			ImGui::InputText("Password", &NetworkLoginInfo.Password, ImGuiInputTextFlags_Password);
			ImGui::InputText("Username", &NetworkLoginInfo.Username);
			if (ImGui::Button("Login"))
			{
				NetworkLoginInfo.Login = true;
			}
		}
		else
		{
			ImGui::Text("Token: %s", NakamaNetwork._Session->getAuthToken().c_str());
			ImGui::Text("UserID: %s", NakamaNetwork._Session->getUserId().c_str());
			ImGui::Text("Username: %s", NakamaNetwork._Session->getUsername().c_str());
		}
		ImGui::End();
	}
}

ArchetypeReadWriteMaskPair ImguiNetworkAccountMenuSystem::RegisterComponents(ComponentStorageRegistry& Registry)
{
	// ArchetypeReadWriteMaskPair Pair = ArchetypeReadWriteMaskPairFromFunction(Registry,
	// std::function(UpdateComponents)); Pair.ComponentReadMask |= ArchetypeMaskFromComponents<MySecondComponent>();
	// Pair.ComponentWriteMask |= ArchetypeMaskFromComponents<MyThirdComponent>();
	// return Pair;
	return System::RegisterComponents(Registry);
}

void ImguiNetworkAccountMenuSystem::Initialize(World& World)
{
}

void ImguiNetworkAccountMenuSystem::Update(World& World)
{
	// World.AccessEntityManager().ForeachParallell(World, std::function(UpdateComponents));
	World.AccessEntityManager().ForeachSerial(World, std::function(UpdateComponents));
}

void ImguiNetworkAccountMenuSystem::Uninitialize(World& World)
{
}
