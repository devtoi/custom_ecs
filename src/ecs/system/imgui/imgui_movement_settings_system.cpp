#include "imgui_movement_settings_system.h"

#include "imgui.h"

#include <ecs/world.h>
#include <ecs/component/game/food_mover_component.h>
#include <ecs/component/game/home_tracker_component.h>
#include <ecs/component/position_component.h>
#include <ecs/component/velocity_component.h>
#include <ecs/component/game/pheromone_map_component.h>
#include <ecs/component/tile_map_component.h>
#include <ecs/component/game/movement_settings_component.h>
#include <ecs/component/imgui/imgui_movement_settings_component.h>
#include <ecs/system/movement_system.h>

ImguiMovementSettingsSystem::ImguiMovementSettingsSystem() : System(ImguiMovementSettingsSystem::NAME)
{
}

void UpdateComponents(ComponentUpdateParameters& Params, const ComponentBucket<PositionComponent>& Positions,
	const ComponentBucket<VelocityComponent>& Velocities, const ComponentBucket<FoodMoverComponent>& FoodMovers,
	const ComponentBucket<HomeTrackerComponent>& HomeTrackers)
{
	const TileMapComponent& TileMap = *Params.World.AccessEntityManager().AccessFirstComponent<TileMapComponent>();
	const PheromonMapComponent& PheromonMap =
		*Params.World.AccessEntityManager().AccessFirstComponent<PheromonMapComponent>();

	auto& ImguiMovementSettings =
		*Params.World.AccessEntityManager().AccessFirstComponent<ImguiMovementSettingsComponent>();

	if (ImguiMovementSettings.PheromoneTrackerIndex <= Params.End)
	{
		auto Probabilities =
			MovementSystem::DetermineProbabilities(FoodMovers.GetComponent(ImguiMovementSettings.PheromoneTrackerIndex),
				HomeTrackers.GetComponent(ImguiMovementSettings.PheromoneTrackerIndex),
				Positions.GetComponent(ImguiMovementSettings.PheromoneTrackerIndex), TileMap, PheromonMap,
				Velocities.GetComponent(ImguiMovementSettings.PheromoneTrackerIndex), Params.World);
		ImGui::BeginTable("Pheromones", 3);
		ImGui::TableNextColumn();
		ImGui::Text("%lu", Probabilities[0]);
		ImGui::TableNextColumn();
		ImGui::Text("%lu", Probabilities[1]);
		ImGui::TableNextColumn();
		ImGui::Text("%lu", Probabilities[2]);
		ImGui::TableNextRow();

		ImGui::TableNextColumn();
		ImGui::Text("%lu", Probabilities[3]);
		ImGui::TableNextColumn();
		ImGui::Text("%lu", Probabilities[4]);
		ImGui::TableNextColumn();
		ImGui::Text("%lu", Probabilities[5]);
		ImGui::TableNextRow();

		ImGui::TableNextColumn();
		ImGui::Text("%lu", Probabilities[6]);
		ImGui::TableNextColumn();
		ImGui::Text("%lu", Probabilities[7]);
		ImGui::TableNextColumn();
		ImGui::Text("%lu", Probabilities[8]);
		ImGui::EndTable();
	}
}

ArchetypeReadWriteMaskPair ImguiMovementSettingsSystem::RegisterComponents(ComponentStorageRegistry& Registry)
{
	ArchetypeReadWriteMaskPair Pair = ArchetypeReadWriteMaskPairFromFunction(Registry, std::function(UpdateComponents));
	Pair.ComponentReadMask |= ArchetypeMaskFromComponents<TileMapComponent, PheromonMapComponent>();
	Pair.ComponentWriteMask |= ArchetypeMaskFromComponents<MovementSettingsComponent, ImguiMovementSettingsComponent>();
	Registry.RegisterComponent<ImguiMovementSettingsComponent>();
	return Pair;
}

void ImguiMovementSettingsSystem::Initialize(World& World)
{
	World.AccessEntityManager().CreateEntity(ArchetypeMaskFromComponents<ImguiMovementSettingsComponent>());
}

void ImguiMovementSettingsSystem::Update(World& World)
{
	ImGui::Begin("Ants");
	static int Index = 0;

	auto& MovementSettings = *World.AccessEntityManager().AccessFirstComponent<MovementSettingsComponent>();

	ImGui::InputInt("Index", &Index);
	World.AccessEntityManager().ForeachSerial(World, std::function(UpdateComponents));
	auto InputInt64 = [](const char* pName, int64_t& val) {
		int i = static_cast<int>(val);
		ImGui::InputInt(pName, &i);
		val = i;
	};

	InputInt64("BaseProbability", MovementSettings.BaseProbability);
	InputInt64("FoodTileBaseProbability", MovementSettings.FoodTileBaseProbability);
	InputInt64("DirectionWeight", MovementSettings.DirectionWeight);
	InputInt64("DistanceFromHomeWeightWhenHasFood", MovementSettings.DistanceFromHomeWeightWhenHasFood);
	InputInt64("HomePheromoneMult", MovementSettings.HomePheromoneMult);
	InputInt64("HomePheromoneMultWhenScouting", MovementSettings.HomePheromoneMultWhenScouting);
	InputInt64("HomeBaseMult", MovementSettings.HomeBaseMult);
	InputInt64("FoodPheromoneMult", MovementSettings.FoodPheromoneMult);
	InputInt64("FoodBaseMult", MovementSettings.FoodBaseMult);
	ImGui::End();
}
