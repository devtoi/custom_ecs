#include "imgui_end_frame_system.h"

#include <imgui/misc/single_file/imgui_single_file.h>
#include <vulkan/vulkan_raii.hpp>
#include <backends/imgui_impl_vulkan.h>

#include <ecs/world.h>
#include <ecs/component/rendering/imgui_component.h>
#include <ecs/component/rendering/renderer_component.h>

IMGUIEndFrameSystem::IMGUIEndFrameSystem() : System(IMGUIEndFrameSystem::NAME)
{
}

void IMGUIEndFrameSystem::Initialize(World& world)
{
}

void IMGUIEndFrameSystem::Update(World& world)
{
#if USE_IMGUI
	ImGui::Render();

	ImDrawData* draw_data = ImGui::GetDrawData();
	const bool is_minimized = (draw_data->DisplaySize.x <= 0.0f || draw_data->DisplaySize.y <= 0.0f);
	if (!is_minimized)
	{
		// Record dear imgui primitives into command buffer
		auto* pRenderer = world.AccessEntityManager().AccessFirstComponent<RendererComponent>();
		if (!pRenderer)
			return;
		RendererComponent& render = *pRenderer;
		ImGui_ImplVulkan_RenderDrawData(draw_data, *render._commandBuffers[render._currentImageIndex]);
	}
#endif
}
