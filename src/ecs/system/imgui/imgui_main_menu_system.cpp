#include "imgui_main_menu_system.h"

#include <imgui.h>

#include <ecs/world.h>
#include <ecs/component/core/menu_component.h>

ImguiMainMenuSystem::ImguiMainMenuSystem() : System(ImguiMainMenuSystem::NAME)
{
}

// void UpdateComponents(ComponentUpdateParameters& Parameters, ComponentBucket<MyComponent>& MyComponents)
//{
// }

ArchetypeReadWriteMaskPair ImguiMainMenuSystem::RegisterComponents(ComponentStorageRegistry& Registry)
{
	// ArchetypeReadWriteMaskPair Pair = ArchetypeReadWriteMaskPairFromFunction(Registry,
	// std::function(UpdateComponents)); Pair.ComponentReadMask |= ArchetypeMaskFromComponents<MySecondComponent>();
	// Pair.ComponentWriteMask |= ArchetypeMaskFromComponents<MyThirdComponent>();
	// return Pair;
	return System::RegisterComponents(Registry);
}

void ImguiMainMenuSystem::Initialize(World& World)
{
}

void ImguiMainMenuSystem::Update(World& World)
{
	auto* pMenu = World.AccessEntityManager().AccessFirstComponent<MenuComponent>();
	if (!pMenu)
	{
		return;
	}

	ImGui::Begin("Main menu");
	if (ImGui::Button("Ants"))
	{
		pMenu->SwitchToNextMode = true;
	}
	ImGui::End();
}

void ImguiMainMenuSystem::Uninitialize(World& World)
{
}
