#pragma once

#include "../system.h"

class IMGUIBeginFrameSystem final : public System
{
public:
	static constexpr std::string_view NAME = "IMGUIBeginFrameSystem";
	IMGUIBeginFrameSystem();

	ArchetypeReadWriteMaskPair RegisterComponents(ComponentStorageRegistry& registry) override;
	void Initialize(World& world) override;
	void Uninitialize(World& world) override;
	void Update(World& world) override;
};
