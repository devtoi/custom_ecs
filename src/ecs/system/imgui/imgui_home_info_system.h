#pragma once

#include <ecs/system/system.h>

class ImguiHomeInfoSystem final : public System
{
public:
	static constexpr std::string_view NAME = "ImguiHomeInfoSystem";
	ImguiHomeInfoSystem();

	ArchetypeReadWriteMaskPair RegisterComponents(ComponentStorageRegistry& registry) override;
	void Update(World& world) override;
};
