#include "imgui_sprite_render_settings_system.h"

#include <imgui.h>

#include <ecs/world.h>
#include <ecs/component/rendering/sprite_render_settings_component.h>

ImguiSpriteRenderSettingsSystem::ImguiSpriteRenderSettingsSystem() : System(ImguiSpriteRenderSettingsSystem::NAME)
{
}
ArchetypeReadWriteMaskPair ImguiSpriteRenderSettingsSystem::RegisterComponents(ComponentStorageRegistry&  /*Registry*/)
{
	ArchetypeReadWriteMaskPair Pair;
	Pair.ComponentWriteMask |= ArchetypeMaskFromComponents<SpriteRenderSettingsComponent>();
	return Pair;
}

void ImguiSpriteRenderSettingsSystem::Update(World& world)
{
#if USE_IMGUI
	auto* pSpriteRenderSettings = world.AccessEntityManager().AccessFirstComponent<SpriteRenderSettingsComponent>();
	if (!pSpriteRenderSettings)
	{
		return;
	}
	ImGui::Begin("Ants");
	ImGui::Checkbox("Render", &pSpriteRenderSettings->Render);
	ImGui::Checkbox("Pheromones", &pSpriteRenderSettings->RenderPheromones);
	ImGui::End();
#endif
}
