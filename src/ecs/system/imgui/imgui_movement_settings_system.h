#pragma once

#include <ecs/system/system.h>

class ImguiMovementSettingsSystem final : public System
{
public:
	static constexpr std::string_view NAME = "ImguiMovementSettingsSystem";
	ImguiMovementSettingsSystem();

	ArchetypeReadWriteMaskPair RegisterComponents(ComponentStorageRegistry& registry) override;
	void Initialize(World& world) override;
	void Update(World& world) override;
};
