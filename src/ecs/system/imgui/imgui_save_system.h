#pragma once

#include <ecs/system/system.h>

class ImguiSaveSystem final : public System
{
public:
	static constexpr std::string_view NAME = "ImguiSaveSystem";
	ImguiSaveSystem();

	void Update(World& world) override;
};
