#pragma once

#include <memory>
#include "../system.h"

class SDLToRendererEventsSystem final : public System
{
public:
	static constexpr std::string_view NAME = "SDLToRendererEventsSystem";
	SDLToRendererEventsSystem();
	ArchetypeReadWriteMaskPair RegisterComponents(ComponentStorageRegistry& registry) override;
	void Initialize(World& world) override;
	void Update(World& world) override;
	void Uninitialize(World& world) override;
};
