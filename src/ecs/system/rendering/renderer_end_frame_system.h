#pragma once

#include "../system.h"

class RendererEndFrameSystem final : public System
{
public:
	static constexpr std::string_view NAME = "RendererEndFrameSystem";
	RendererEndFrameSystem();
	void Update(World& world) override;
};
