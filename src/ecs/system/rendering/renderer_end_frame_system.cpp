#include "renderer_end_frame_system.h"

#include <ecs/world.h>
#include <ecs/component/rendering/renderer_component.h>
#include <ecs/component/rendering/tile_map_render_pipeline_component.h>
#include <ecs/component/camera_component.h>

RendererEndFrameSystem::RendererEndFrameSystem() : System(RendererEndFrameSystem::NAME)
{
}

void buildCommandBuffers(World&  /*world*/, RendererComponent& rendererComponent)
{
	vk::CommandBufferBeginInfo beginInfo{};

	auto& buffer = rendererComponent._commandBuffers[rendererComponent._currentImageIndex];

	buffer.endRenderPass();
	buffer.end();
}


void RendererEndFrameSystem::Update(World& world)
{
	auto* pRenderer = world.AccessEntityManager().AccessFirstComponent<RendererComponent>();
	if (!pRenderer)
		return;
	RendererComponent& render = *pRenderer;

	buildCommandBuffers(world, render);

	std::vector<vk::CommandBuffer> CommandBuffers;
	CommandBuffers.emplace_back(*render._commandBuffers[render._currentImageIndex]);

	vk::PipelineStageFlags waitStages[] = { vk::PipelineStageFlagBits::eColorAttachmentOutput };
	vk::SubmitInfo submitInfo{};
	submitInfo.waitSemaphoreCount = 1;
	submitInfo.pWaitSemaphores = &*render._imageAvailableSemaphores[render._currentFrame];
	submitInfo.pWaitDstStageMask = waitStages;

	submitInfo.commandBufferCount = CommandBuffers.size();
	submitInfo.pCommandBuffers = CommandBuffers.data();

	submitInfo.signalSemaphoreCount = 1;
	submitInfo.pSignalSemaphores = &*render._renderFinishedSemaphores[render._currentFrame];

	render._device->resetFences(*render._inFlightFences[render._currentFrame]);

	render._graphicsQueue->submit(submitInfo, *render._inFlightFences[render._currentFrame]);

	vk::PresentInfoKHR presentInfo{};

	presentInfo.waitSemaphoreCount = 1;
	presentInfo.pWaitSemaphores = &*render._renderFinishedSemaphores[render._currentFrame];

	presentInfo.swapchainCount = 1;
	presentInfo.pSwapchains = &**render._swapChain;
	presentInfo.pImageIndices = &render._currentImageIndex;

	presentInfo.pResults = nullptr; // Optional

	auto result = render._graphicsQueue->presentKHR(presentInfo);

	if (result == vk::Result::eErrorOutOfDateKHR || result == vk::Result::eSuboptimalKHR)
	{
		render._recreateSwapChain = true;
	}
	else if (result != vk::Result::eSuccess)
	{
		throw std::runtime_error("failed to present swap chain image!");
	}

	render._currentFrame = (render._currentFrame + 1) % render._maxFramesInFlight;
}
