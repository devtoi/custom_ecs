#pragma once

#include "../system.h"

class RendererBeginFrameSystem final : public System
{
public:
	static constexpr std::string_view NAME = "RendererBeginFrameSystem";
	RendererBeginFrameSystem();
	ArchetypeReadWriteMaskPair RegisterComponents(ComponentStorageRegistry& registry) override;
	void Initialize(World& world) override;
	void Uninitialize(World& world) override;
	void Update(World& world) override;
};
