#pragma once

#include "../system.h"

#include <render/sprite.h>

struct ComponentUpdateParameters;
struct PositionComponent;
struct VelocityComponent;
struct FoodMoverComponent;

class SpriteRenderSystem final : public System
{
public:
	static constexpr std::string_view NAME = "SpriteRenderSystem";
	SpriteRenderSystem();

	ArchetypeReadWriteMaskPair RegisterComponents(ComponentStorageRegistry& registry) override;
	void Initialize(World& world) override;
	void Update(World& world) override;

private:
	void BuildSpriteList(ComponentUpdateParameters& params,
		const ComponentBucket<PositionComponent>& PositionComponents,
		const ComponentBucket<VelocityComponent>& VelocityComponents,
		const ComponentBucket<FoodMoverComponent>& FoodMovers);

	std::vector<Sprite> _Sprites;
};
