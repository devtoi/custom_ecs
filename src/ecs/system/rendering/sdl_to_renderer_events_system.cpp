#include "sdl_to_renderer_events_system.h"

#include <imgui/misc/single_file/imgui_single_file.h>
#include <spdlog/spdlog.h>

#include <ecs/world.h>
#include <ecs/component/rendering/renderer_component.h>
#include <ecs/component/position_component.h>
#include <ecs/component/camera_component.h>
#include <ecs/component/core/sdl_window_component.h>

SDLToRendererEventsSystem::SDLToRendererEventsSystem() : System(SDLToRendererEventsSystem::NAME)
{
}

ArchetypeReadWriteMaskPair SDLToRendererEventsSystem::RegisterComponents(ComponentStorageRegistry& registry)
{
	registry.RegisterComponent<SDLWindowComponent>();
	ArchetypeReadWriteMaskPair Pair;
	Pair.ComponentWriteMask |= ArchetypeMaskFromComponents<SDLWindowComponent, RendererComponent>();
	return Pair;
}

int OnSDLEvent(void* pUserData, SDL_Event* pEvent)
{
	if (pEvent->type != SDL_WINDOWEVENT)
	{
		return 0;
	}
	auto* pWorld = static_cast<World*>(pUserData);
	auto* pSDLWindowComponent = pWorld->AccessEntityManager().AccessFirstComponent<SDLWindowComponent>();
	if (!pSDLWindowComponent)
	{
		SPDLOG_WARN("SDL window event {}, but no sdl window component present", pEvent->window.event);
		return 0;
	}
	SPDLOG_TRACE("SDL window event {}", event->window.event);
	pSDLWindowComponent->m_SDLWindowEvents.emplace_back(static_cast<SDL_WindowEventID>(pEvent->window.event));
	return 0;
}

void SDLToRendererEventsSystem::Initialize(World& world)
{
	world.AccessEntityManager().CreateEntity(ArchetypeMaskFromComponents<SDLWindowComponent>());
	SDL_AddEventWatch(OnSDLEvent, &world);
}

void SDLToRendererEventsSystem::Update(World& World)
{
	auto* pSDLWindowComponent = World.AccessEntityManager().AccessFirstComponent<SDLWindowComponent>();
	if (!pSDLWindowComponent)
	{
		return;
	}

	auto& WindowEvents = pSDLWindowComponent->m_SDLWindowEvents;

	if (WindowEvents.empty())
	{
		return;
	}

	auto* pRenderComponent = World.AccessEntityManager().AccessFirstComponent<RendererComponent>();
	if (!pRenderComponent)
	{
		return;
	}

	while (!WindowEvents.empty())
	{
		RendererComponent& render = *pRenderComponent;
		switch (WindowEvents.back())
		{
		case SDL_WINDOWEVENT_SIZE_CHANGED: {
			render._recreateSwapChain = true;
			break;
		}
		case SDL_WINDOWEVENT_MINIMIZED: {
			render._draw = true;
			break;
		}
		case SDL_WINDOWEVENT_MAXIMIZED: {
			render._recreateSwapChain = true;
			break;
		}
		case SDL_WINDOWEVENT_RESIZED: {
			render._recreateSwapChain = true;
			break;
		}
		default:
			break;
		}
		WindowEvents.pop_back();
	}
}

void SDLToRendererEventsSystem::Uninitialize(World& /*world*/)
{
	SDL_DelEventWatch(OnSDLEvent, this);
}
