#include "renderer_begin_frame_system.h"
#include "render/camera.h"
#include "render/sprite.h"
#include "render/utilities.h"

#include <imgui/misc/single_file/imgui_single_file.h>
#include <set>
#include <spdlog/spdlog.h>
#include <vulkan/vulkan.hpp>
#include <vulkan/vulkan_raii.hpp>
#include <glm/glm.hpp>

#include <ecs/world.h>
#include <ecs/component/rendering/renderer_component.h>
#include <ecs/component/camera_component.h>
#include <ecs/component/utility/tile_map_brush_component.h>
#include <render/vertex.h>

namespace
{
	const char* APP_NAME = "custom_ecs";
	const char* ENGINE_NAME = "custom_ecs_engine";

	const vk::PresentModeKHR PREFFERED_PRESENTATION_MODE = vk::PresentModeKHR::eFifo;
	const vk::SurfaceTransformFlagBitsKHR TRANSFORM = vk::SurfaceTransformFlagBitsKHR::eIdentity;
	const vk::Format FORMAT = vk::Format::eB8G8R8A8Srgb;
	const vk::ColorSpaceKHR COLOR_SPACE = vk::ColorSpaceKHR::eSrgbNonlinear;
}


RendererBeginFrameSystem::RendererBeginFrameSystem() : System(RendererBeginFrameSystem::NAME)
{
}

const std::set<std::string>& getRequestedLayerNames()
{
	static std::set<std::string> layers;
	if (layers.empty())
	{
		// layers.emplace("VK_LAYER_NV_optimus");
		layers.emplace("VK_LAYER_KHRONOS_validation");
		// layers.emplace("VK_LAYER_MESA_device_select");
		// layers.emplace("VK_LAYER_MESA_overlay");
	}
	return layers;
}

const std::set<std::string>& getRequestedDeviceExtensionNames()
{
	static std::set<std::string> layers;
	if (layers.empty())
	{
		layers.emplace(VK_KHR_SWAPCHAIN_EXTENSION_NAME);
	}
	return layers;
}

const std::vector<vk::ImageUsageFlags> getRequestedImageUsages()
{
	static std::vector<vk::ImageUsageFlags> usages;
	if (usages.empty())
	{
		usages.emplace_back(vk::ImageUsageFlagBits::eColorAttachment);
	}
	return usages;
}

static VKAPI_ATTR VkBool32 VKAPI_CALL debugCallback(VkDebugUtilsMessageSeverityFlagBitsEXT messageSeverity,
	VkDebugUtilsMessageTypeFlagsEXT  /*messageType*/, const VkDebugUtilsMessengerCallbackDataEXT* pCallbackData,
	void*  /*pUserData*/)
{
	std::vector<std::string> objs;
	if (pCallbackData->objectCount > 0)
	{
		for (int i = 0; i < pCallbackData->objectCount; ++i)
		{
			const auto& obj = pCallbackData->pObjects[i];
			const auto* name = obj.pObjectName;
			if (name)
			{
				objs.emplace_back(name);
			}
		}
	}
	if (messageSeverity & VkDebugUtilsMessageSeverityFlagBitsEXT::VK_DEBUG_UTILS_MESSAGE_SEVERITY_ERROR_BIT_EXT)
	{
		SPDLOG_ERROR("{0}", pCallbackData->pMessage);
	}
	if (messageSeverity & VkDebugUtilsMessageSeverityFlagBitsEXT::VK_DEBUG_UTILS_MESSAGE_SEVERITY_WARNING_BIT_EXT)
	{
		SPDLOG_WARN("{0}", pCallbackData->pMessage);
	}
	if (messageSeverity & VkDebugUtilsMessageSeverityFlagBitsEXT::VK_DEBUG_UTILS_MESSAGE_SEVERITY_INFO_BIT_EXT)
	{
		SPDLOG_INFO("{0}", pCallbackData->pMessage);
	}
	if (messageSeverity & VkDebugUtilsMessageSeverityFlagBitsEXT::VK_DEBUG_UTILS_MESSAGE_SEVERITY_VERBOSE_BIT_EXT)
	{
		SPDLOG_TRACE("{0}", pCallbackData->pMessage);
	}

	return VK_FALSE;
}

std::unique_ptr<vk::raii::DebugUtilsMessengerEXT> setupDebugCallback(vk::raii::Instance& instance)
{
	vk::DebugUtilsMessengerCreateInfoEXT createInfo;
	createInfo.messageSeverity = vk::DebugUtilsMessageSeverityFlagBitsEXT::eError |
								 vk::DebugUtilsMessageSeverityFlagBitsEXT::eWarning |
								 vk::DebugUtilsMessageSeverityFlagBitsEXT::eInfo;
	createInfo.pfnUserCallback = debugCallback;
	createInfo.messageType = vk::DebugUtilsMessageTypeFlagBitsEXT::eGeneral |
							 vk::DebugUtilsMessageTypeFlagBitsEXT::eValidation |
							 vk::DebugUtilsMessageTypeFlagBitsEXT::ePerformance;

	return std::make_unique<vk::raii::DebugUtilsMessengerEXT>(instance, createInfo);
}

std::vector<std::string> getAvailableVulkanLayers()
{
	std::vector<std::string> outLayers;
	std::vector<vk::LayerProperties> instance_layer_names = vk::enumerateInstanceLayerProperties();

	// Display layer names and find the ones we specified above
	SPDLOG_DEBUG("found {0} instance layers:", instance_layer_names.size());

	const std::set<std::string>& lookup_layers = getRequestedLayerNames();
	int count(0);
	for (const auto& name : instance_layer_names)
	{
		SPDLOG_DEBUG("{0}: {1}: {2}", count, name.layerName, name.description);
		auto it = lookup_layers.find(std::string(name.layerName.data()));
		if (it != lookup_layers.end())
		{
			outLayers.emplace_back(name.layerName.data());
		}
		count++;
	}

	// Print the ones we're enabling
	for (const auto& layer : outLayers)
	{
		spdlog::info("applying layer: {0}", layer.c_str());
	}
	return outLayers;
}

std::vector<std::string> getAvailableVulkanExtensions(SDL_Window* window)
{
	std::vector<std::string> outExtensions;
	// Figure out the amount of extensions vulkan needs to interface with the os
	// windowing system This is necessary because vulkan is a platform agnostic
	// API and needs to know how to interface with the windowing system
	unsigned int ext_count = 0;
	if (!SDL_Vulkan_GetInstanceExtensions(window, &ext_count, nullptr))
	{
		throw std::runtime_error("Unable to query the number of Vulkan instance extensions");
	}

	// Use the amount of extensions queried before to retrieve the names of the
	// extensions
	std::vector<const char*> ext_names(ext_count);
	if (!SDL_Vulkan_GetInstanceExtensions(window, &ext_count, ext_names.data()))
	{
		throw std::runtime_error("Unable to query the number of Vulkan instance extension names");
	}

	// Display names
	SPDLOG_DEBUG("found {0} Vulkan instance extensions: ", ext_count);
	for (unsigned int i = 0; i < ext_count; i++)
	{
		SPDLOG_DEBUG("{0}: {1}", i, ext_names[i]);
		outExtensions.emplace_back(ext_names[i]);
	}

	// Add debug display extension, we need this to relay debug messages
	outExtensions.emplace_back(VK_EXT_DEBUG_UTILS_EXTENSION_NAME);
	return outExtensions;
}

std::unique_ptr<vk::raii::Instance> createVulkanInstance(vk::raii::Context& context,
	const std::vector<std::string>& layerNames, const std::vector<std::string>& extensionNames)
{
	vk::ApplicationInfo app_info = {};
	app_info.pApplicationName = APP_NAME;
	app_info.pEngineName = ENGINE_NAME;
	app_info.applicationVersion = VK_MAKE_API_VERSION(0, 0, 1, 0);
	app_info.engineVersion = VK_MAKE_API_VERSION(0, 0, 1, 0);
	app_info.apiVersion = VK_API_VERSION_1_2;

	// Copy layers
	std::vector<const char*> layer_names;
	for (const auto& layer : layerNames)
		layer_names.emplace_back(layer.c_str());

	// Copy extensions
	std::vector<const char*> ext_names;
	for (const auto& ext : extensionNames)
		ext_names.emplace_back(ext.c_str());

	// initialize the vk::InstanceCreateInfo structure
	vk::InstanceCreateInfo inst_info = {};
	inst_info.pApplicationInfo = &app_info;
	inst_info.enabledExtensionCount = static_cast<uint32_t>(ext_names.size());
	inst_info.ppEnabledExtensionNames = ext_names.data();
	inst_info.enabledLayerCount = static_cast<uint32_t>(layer_names.size());
	inst_info.ppEnabledLayerNames = layer_names.data();

	return std::make_unique<vk::raii::Instance>(context, inst_info);
}

unsigned int selectGPU(vk::raii::PhysicalDevice& physicalDevice)
{
	std::vector<vk::QueueFamilyProperties> queue_properties = physicalDevice.getQueueFamilyProperties();
	if (queue_properties.size() == 0)
	{
		throw std::runtime_error("device has no family of queues associated with it");
	}

	// Make sure the family of commands contains an option to issue graphical
	// commands.
	unsigned int queue_node_index = 0;
	for (auto entry : queue_properties)
	{
		if (entry.queueCount > 0 && entry.queueFlags & vk::QueueFlagBits::eGraphics)
		{
			return queue_node_index;
		}
		queue_node_index++;
	}
	throw std::runtime_error("Unable to find a queue command family that accepts graphics commands");
}

/**
 *  Creates a logical device
 */
std::unique_ptr<vk::raii::Device> createLogicalDevice(
	vk::raii::PhysicalDevice& physicalDevice, uint32_t graphicsQueueIndex, const std::vector<std::string>& layerNames)
{
	// Copy layer names
	std::vector<const char*> layer_names;
	for (const auto& layer : layerNames)
		layer_names.emplace_back(layer.c_str());

	auto extensionProperties = physicalDevice.enumerateDeviceExtensionProperties();
	SPDLOG_DEBUG("found {0} device extensions", extensionProperties.size());

	// Match names against requested extension
	std::vector<const char*> device_property_names;
	const std::set<std::string>& required_extension_names = getRequestedDeviceExtensionNames();
	int count = 0;
	for (const auto& ext_property : extensionProperties)
	{
		SPDLOG_DEBUG("{0}: {1}", count, ext_property.extensionName);
		auto it = required_extension_names.find(std::string(ext_property.extensionName.data()));
		if (it != required_extension_names.end())
		{
			device_property_names.emplace_back(ext_property.extensionName);
		}
		count++;
	}

	// Warn if not all required extensions were found
	if (required_extension_names.size() != device_property_names.size())
	{
		throw std::runtime_error("not all required device extensions are supported!");
	}

	for (const auto& name : device_property_names)
		SPDLOG_DEBUG("applying device extension: {0}", name);

	// Create queue information structure used by device based on the previously
	// fetched queue information from the physical device We create one command
	// processing queue for graphics
	vk::DeviceQueueCreateInfo queue_create_info{};
	queue_create_info.queueFamilyIndex = graphicsQueueIndex;
	queue_create_info.queueCount = 1;
	float queue_prio = 1.0f;
	queue_create_info.pQueuePriorities = &queue_prio;

	// Device creation information
	vk::DeviceCreateInfo create_info{};
	create_info.queueCreateInfoCount = 1;
	create_info.pQueueCreateInfos = &queue_create_info;
	create_info.ppEnabledLayerNames = layer_names.data();
	create_info.enabledLayerCount = static_cast<uint32_t>(layer_names.size());
	create_info.ppEnabledExtensionNames = device_property_names.data();
	create_info.enabledExtensionCount = static_cast<uint32_t>(device_property_names.size());

	return std::make_unique<vk::raii::Device>(physicalDevice, create_info);
}

std::unique_ptr<vk::raii::DescriptorPool> createDescriptorPool(vk::raii::Device& device)
{
	constexpr int maxSize = 100;
	std::vector<vk::DescriptorPoolSize> poolSizes = { vk::DescriptorPoolSize{
														  vk::DescriptorType::eUniformBuffer, maxSize },
		vk::DescriptorPoolSize{ vk::DescriptorType::eCombinedImageSampler, maxSize } };

	vk::DescriptorPoolCreateInfo poolInfo;
	poolInfo.poolSizeCount = poolSizes.size();
	poolInfo.pPoolSizes = poolSizes.data();
	poolInfo.maxSets = maxSize * poolSizes.size();
	poolInfo.flags = vk::DescriptorPoolCreateFlagBits::eFreeDescriptorSet;

	return std::make_unique<vk::raii::DescriptorPool>(device, poolInfo);
}

/**
 *  Creates the vulkan surface that is rendered to by the device using SDL
 */
std::unique_ptr<vk::raii::SurfaceKHR> createSurface(vk::raii::Instance& instance,
	vk::raii::PhysicalDevice& physicalDevice, SDL_Window* window, uint32_t graphicsQueueIndex)
{
	VkSurfaceKHR surface;
	if (!SDL_Vulkan_CreateSurface(window, static_cast<vk::Instance>(*instance), &surface))
	{
		throw std::runtime_error("Unable to create Vulkan compatible surface using SDL");
	}

	auto presentationSurface = std::make_unique<vk::raii::SurfaceKHR>(instance, surface);

	// Make sure the surface is compatible with the queue family and gpu
	vk::Bool32 supported = physicalDevice.getSurfaceSupportKHR(graphicsQueueIndex, **presentationSurface);
	if (!supported)
	{
		throw std::runtime_error("Surface is not supported by physical device!");
	}
	return presentationSurface;
}

/**
 * @return if the present modes could be queried and ioMode is set
 * @param outMode the mode that is requested, will contain FIFO when requested
 * mode is not available
 */
vk::PresentModeKHR getPresentationMode(
	vk::raii::PhysicalDevice& physicalDevice, const vk::raii::SurfaceKHR& presentationSurface)
{
	auto presentModes = physicalDevice.getSurfacePresentModesKHR(*presentationSurface);

	for (auto& mode : presentModes)
	{
		if (mode == PREFFERED_PRESENTATION_MODE)
			return mode;
	}
	spdlog::error("unable to obtain preferred display mode, fallback to FIFO");
	return vk::PresentModeKHR::eFifo;
}

/**
 * Figure out the number of images that are used by the swapchain and
 * available to us in the application, based on the minimum amount of necessary
 * images provided by the capabilities struct.
 */
unsigned int getNumberOfSwapImages(const vk::SurfaceCapabilitiesKHR& capabilities)
{
	unsigned int number = capabilities.minImageCount + 1;
	return number > capabilities.maxImageCount ? capabilities.minImageCount : number;
}

/**
 *  Returns the size of a swapchain image based on the current surface
 */
vk::Extent2D getSwapImageSize(
	const vk::SurfaceCapabilitiesKHR& capabilities, uint32_t windowWidth, uint32_t windowHeight)
{
	// Default size = window size
	vk::Extent2D size = { windowWidth, windowHeight };

	// This happens when the window scales based on the size of an image
	if (capabilities.currentExtent.width == 0xFFFFFFFF)
	{
		size.width =
			glm::clamp<unsigned int>(size.width, capabilities.minImageExtent.width, capabilities.maxImageExtent.width);
		size.height = glm::clamp<unsigned int>(
			size.height, capabilities.minImageExtent.height, capabilities.maxImageExtent.height);
	}
	else
	{
		size = capabilities.currentExtent;
	}
	return size;
}

/**
 * Checks if the surface supports color and other required surface bits
 * If so constructs a ImageUsageFlags bitmask that is returned in outUsage
 * @return if the surface supports all the previously defined bits
 */
vk::ImageUsageFlags getImageUsage(const vk::SurfaceCapabilitiesKHR& capabilities)
{
	vk::ImageUsageFlags usageFlags = {};

	const std::vector<vk::ImageUsageFlags>& desir_usages = getRequestedImageUsages();
	assert(desir_usages.size() > 0);

	for (const auto& desired_usage : desir_usages)
	{
		vk::ImageUsageFlags image_usage = desired_usage & capabilities.supportedUsageFlags;
		if (image_usage != desired_usage)
		{
			/* spdlog::info("unsupported image usage flag: {0}", desired_usage); */
			throw std::runtime_error("unsupported image usage flag");
		}

		// Add bit if found as supported color
		usageFlags = (usageFlags | desired_usage);
	}
	return usageFlags;
}

/**
 * @return transform based on global declared above, current transform if that
 * transform isn't available
 */
vk::SurfaceTransformFlagBitsKHR getTransform(const vk::SurfaceCapabilitiesKHR& capabilities)
{
	if (capabilities.supportedTransforms & TRANSFORM)
		return TRANSFORM;
	// spdlog::warn("unsupported surface transform: {0}", TRANSFORM);
	return capabilities.currentTransform;
}

/**
 * @return the most appropriate color space based on the globals provided above
 */
vk::SurfaceFormatKHR getFormat(
	vk::raii::PhysicalDevice& physicalDevice, const vk::raii::SurfaceKHR& presentationSurface)
{
	vk::SurfaceFormatKHR surfaceFormat;

	auto surfaceFormats = physicalDevice.getSurfaceFormatsKHR(*presentationSurface);

	// This means there are no restrictions on the supported format.
	// Preference would work
	if (surfaceFormats.size() == 1 && surfaceFormats[0].format == vk::Format::eUndefined)
	{
		surfaceFormat.format = FORMAT;
		surfaceFormat.colorSpace = COLOR_SPACE;
		return surfaceFormat;
	}

	// Otherwise check if both are supported
	for (const auto& found_format_outer : surfaceFormats)
	{
		// Format found
		if (found_format_outer.format == FORMAT)
		{
			surfaceFormat.format = found_format_outer.format;
			for (const auto& found_format_inner : surfaceFormats)
			{
				// Color space found
				if (found_format_inner.colorSpace == COLOR_SPACE)
				{
					surfaceFormat.colorSpace = found_format_inner.colorSpace;
					return surfaceFormat;
				}
			}

			// No matching color space, pick first one
			spdlog::warn("warning: no matching color space found, picking first "
						 "available one!");
			surfaceFormat.colorSpace = surfaceFormats[0].colorSpace;
			return surfaceFormat;
		}
	}

	// No matching formats found
	spdlog::warn("warning: no matching color format found, picking first available one");
	return surfaceFormats[0];
}

/**
 * creates the swap chain using utility functions above to retrieve swap chain
 * properties Swap chain is associated with a single window (surface) and allows
 * us to display images to screen
 */
void createSwapChain(RendererComponent& rendererComponent)
{
	// Get properties of surface, necessary for creation of swap-chain
	auto surface_properties =
		rendererComponent._physicalDevice->getSurfaceCapabilitiesKHR(**rendererComponent._presentationSurface);

	// Get the image presentation mode (synced, immediate etc.)
	vk::PresentModeKHR presentation_mode =
		getPresentationMode(*rendererComponent._physicalDevice, *rendererComponent._presentationSurface);

	// Get other swap chain related features
	unsigned int swap_image_count = getNumberOfSwapImages(surface_properties);

	// Size of the images
	rendererComponent._swapChainExtent =
		getSwapImageSize(surface_properties, rendererComponent._windowWidth, rendererComponent._windowHeight);
	SPDLOG_DEBUG(
		"Width: {}, Height: {}", rendererComponent._swapChainExtent.width, rendererComponent._swapChainExtent.height);

	// Get image usage (color etc.)
	auto usage_flags = getImageUsage(surface_properties);

	// Get the transform, falls back on current transform when transform is not
	// supported
	vk::SurfaceTransformFlagBitsKHR transform = getTransform(surface_properties);

	// Get swapchain image format
	vk::SurfaceFormatKHR surfaceFormat =
		getFormat(*rendererComponent._physicalDevice, *rendererComponent._presentationSurface);

	// Populate swapchain creation info
	vk::SwapchainCreateInfoKHR swap_info;
	swap_info.surface = **rendererComponent._presentationSurface;
	swap_info.minImageCount = swap_image_count;
	swap_info.imageFormat = surfaceFormat.format;
	swap_info.imageColorSpace = surfaceFormat.colorSpace;
	swap_info.imageExtent = rendererComponent._swapChainExtent;
	swap_info.imageArrayLayers = 1;
	swap_info.imageUsage = usage_flags;
	swap_info.imageSharingMode = vk::SharingMode::eExclusive;
	swap_info.preTransform = transform;
	swap_info.compositeAlpha = vk::CompositeAlphaFlagBitsKHR::eOpaque;
	swap_info.presentMode = presentation_mode;
	swap_info.clipped = true;

	rendererComponent._swapChainImageFormat = surfaceFormat.format;

	rendererComponent._swapChain = std::make_unique<vk::raii::SwapchainKHR>(*rendererComponent._device, swap_info);
}

std::vector<vk::raii::ImageView> getSwapChainImageViewHandles(
	vk::raii::Device& device, vk::raii::SwapchainKHR& swapChain, vk::Format swapChainImageFormat)
{
	auto swapChainImages = swapChain.getImages();

	std::vector<vk::raii::ImageView> swapChainImageViews;
	swapChainImageViews.reserve(swapChainImages.size());
	for (auto& image : swapChainImages)
	{
		vk::ImageViewCreateInfo createInfo{};
		createInfo.image = image;
		createInfo.viewType = vk::ImageViewType::e2D;
		createInfo.format = swapChainImageFormat;
		createInfo.components.r = vk::ComponentSwizzle::eIdentity;
		createInfo.components.g = vk::ComponentSwizzle::eIdentity;
		createInfo.components.b = vk::ComponentSwizzle::eIdentity;
		createInfo.components.a = vk::ComponentSwizzle::eIdentity;
		createInfo.subresourceRange.aspectMask = vk::ImageAspectFlagBits::eColor;
		createInfo.subresourceRange.baseMipLevel = 0;
		createInfo.subresourceRange.levelCount = 1;
		createInfo.subresourceRange.baseArrayLayer = 0;
		createInfo.subresourceRange.layerCount = 1;

		swapChainImageViews.emplace_back(std::move(vk::raii::ImageView(device, createInfo)));
	}
	return swapChainImageViews;
}

std::unique_ptr<vk::raii::RenderPass> createRenderPass(vk::raii::Device& device, vk::Format swapChainImageFormat)
{
	vk::AttachmentDescription colorAttachment{};
	colorAttachment.format = swapChainImageFormat;
	colorAttachment.samples = vk::SampleCountFlagBits::e1;
	colorAttachment.loadOp = vk::AttachmentLoadOp::eClear;
	colorAttachment.storeOp = vk::AttachmentStoreOp::eStore;
	colorAttachment.stencilLoadOp = vk::AttachmentLoadOp::eDontCare;
	colorAttachment.stencilStoreOp = vk::AttachmentStoreOp::eDontCare;
	colorAttachment.initialLayout = vk::ImageLayout::eUndefined;
	colorAttachment.finalLayout = vk::ImageLayout::ePresentSrcKHR;

	vk::AttachmentReference colorAttachmentRef{};
	colorAttachmentRef.attachment = 0;
	colorAttachmentRef.layout = vk::ImageLayout::eColorAttachmentOptimal;

	vk::SubpassDescription subpass{};
	subpass.pipelineBindPoint = vk::PipelineBindPoint::eGraphics;
	subpass.colorAttachmentCount = 1;
	subpass.pColorAttachments = &colorAttachmentRef;

	vk::SubpassDependency dependency{};
	dependency.srcSubpass = VK_SUBPASS_EXTERNAL;
	dependency.dstSubpass = 0;
	dependency.srcStageMask = vk::PipelineStageFlagBits::eColorAttachmentOutput;
	dependency.dstStageMask = vk::PipelineStageFlagBits::eColorAttachmentOutput;
	dependency.dstAccessMask = vk::AccessFlagBits::eColorAttachmentWrite;

	vk::RenderPassCreateInfo renderPassInfo{};
	renderPassInfo.attachmentCount = 1;
	renderPassInfo.pAttachments = &colorAttachment;
	renderPassInfo.subpassCount = 1;
	renderPassInfo.pSubpasses = &subpass;
	renderPassInfo.dependencyCount = 1;
	renderPassInfo.pDependencies = &dependency;

	return std::make_unique<vk::raii::RenderPass>(device, renderPassInfo);
}


std::vector<vk::raii::Framebuffer> createFrameBuffers(vk::raii::Device& device, vk::raii::RenderPass& renderPass,
	const std::vector<vk::raii::ImageView>& swapChainImageViews, vk::Extent2D swapChainExtent)
{
	std::vector<vk::raii::Framebuffer> swapChainFrameBuffers;
	for (const auto& imageView : swapChainImageViews)
	{
		vk::FramebufferCreateInfo framebufferInfo{};
		framebufferInfo.renderPass = *renderPass;
		framebufferInfo.attachmentCount = 1;
		framebufferInfo.pAttachments = &*imageView;
		framebufferInfo.width = swapChainExtent.width;
		framebufferInfo.height = swapChainExtent.height;
		framebufferInfo.layers = 1;

		swapChainFrameBuffers.emplace_back(std::move(vk::raii::Framebuffer(device, framebufferInfo)));
	}
	return swapChainFrameBuffers;
}

std::unique_ptr<vk::raii::CommandPool> createCommandPool(vk::raii::Device& device, uint32_t graphicsQueueIndex)
{
	vk::CommandPoolCreateInfo poolInfo{};
	poolInfo.queueFamilyIndex = graphicsQueueIndex;
	poolInfo.flags = vk::CommandPoolCreateFlagBits::eResetCommandBuffer; // Optional

	return std::make_unique<vk::raii::CommandPool>(device, poolInfo);
}


void createCameraBuffers(RendererComponent& rendererComponent)
{
	rendererComponent._cameraBuffers.clear();
	rendererComponent._cameraBufferMemories.clear();
	for (int i = 0; i < rendererComponent._swapChainImageViews.size(); ++i)
	{
		vk::BufferCreateInfo bufferInfo{};
		bufferInfo.size = sizeof(Camera);
		bufferInfo.usage = vk::BufferUsageFlagBits::eUniformBuffer;
		bufferInfo.sharingMode = vk::SharingMode::eExclusive;

		rendererComponent._cameraBuffers.emplace_back(
			std::move(vk::raii::Buffer(*rendererComponent._device, bufferInfo)));
		{
			vk::DebugUtilsObjectNameInfoEXT nameInfo;
			nameInfo.objectType = vk::ObjectType::eBuffer;
			nameInfo.objectHandle =
				reinterpret_cast<uint64_t>(static_cast<VkBuffer>(*rendererComponent._cameraBuffers.back()));
			nameInfo.pObjectName = "Camera";

			rendererComponent._device->setDebugUtilsObjectNameEXT(nameInfo);
		}


		vk::MemoryRequirements memRequirements = rendererComponent._cameraBuffers.back().getMemoryRequirements();

		vk::MemoryAllocateInfo allocInfo{};
		allocInfo.allocationSize = memRequirements.size;
		allocInfo.memoryTypeIndex =
			renderer::utilities::findMemoryType(*rendererComponent._physicalDevice, memRequirements.memoryTypeBits,
				vk::MemoryPropertyFlagBits::eHostVisible | vk::MemoryPropertyFlagBits::eHostCoherent);

		rendererComponent._cameraBufferMemories.emplace_back(
			std::move(vk::raii::DeviceMemory(*rendererComponent._device, allocInfo)));

		rendererComponent._cameraBuffers.back().bindMemory(*rendererComponent._cameraBufferMemories.back(), 0);
	}
}

void updateCameraBuffers(
	std::vector<vk::raii::DeviceMemory>& cameraBufferMemories, const Camera& camera, unsigned int currentIndex)
{
	void* data = cameraBufferMemories[currentIndex].mapMemory(0, sizeof(Camera));
	memcpy(data, &camera, sizeof(Camera));
	cameraBufferMemories[currentIndex].unmapMemory();
}

// void copyBuffer(vk::Buffer srcBuffer, vk::Buffer dstBuffer, vk::DeviceSize size)
//{
//	vk::CommandBufferAllocateInfo allocInfo{};
//	allocInfo.level = vk::CommandBufferLevel::ePrimary;
//	allocInfo.commandPool = **_commandPool;
//	allocInfo.commandBufferCount = 1;
//
//	vk::raii::CommandBuffer commandBuffer = std::move(vk::raii::CommandBuffers(*_device, allocInfo).front());
//
//	vk::CommandBufferBeginInfo beginInfo{};
//	beginInfo.flags = vk::CommandBufferUsageFlagBits::eOneTimeSubmit;
//
//	commandBuffer.begin(beginInfo);
//
//	vk::BufferCopy copyRegion{};
//	copyRegion.srcOffset = 0; // Optional
//	copyRegion.dstOffset = 0; // Optional
//	copyRegion.size = size;
//	commandBuffer.copyBuffer(srcBuffer, dstBuffer, copyRegion);
//
//	commandBuffer.end();
//
//	vk::SubmitInfo submitInfo{};
//	submitInfo.commandBufferCount = 1;
//	submitInfo.pCommandBuffers = &*commandBuffer;
//
//	_graphicsQueue->submit(submitInfo);
//	_graphicsQueue->waitIdle();
//}

std::vector<vk::raii::CommandBuffer> createCommandBuffers(
	vk::raii::Device& device, vk::raii::CommandPool& commandPool, uint32_t frameBufferCount)
{
	vk::CommandBufferAllocateInfo allocInfo{};
	allocInfo.commandPool = *commandPool;
	allocInfo.level = vk::CommandBufferLevel::ePrimary;
	allocInfo.commandBufferCount = frameBufferCount;

	return vk::raii::CommandBuffers(device, allocInfo);
}

void createSyncObjects(RendererComponent& rendererComponent)
{
	rendererComponent._imagesInFlight.resize(rendererComponent._swapChainImageViews.size());
	rendererComponent._imageAvailableSemaphores.clear();
	rendererComponent._renderFinishedSemaphores.clear();
	rendererComponent._inFlightFences.clear();

	vk::SemaphoreCreateInfo semaphoreInfo{};

	vk::FenceCreateInfo fenceInfo{};
	fenceInfo.flags = vk::FenceCreateFlagBits::eSignaled;

	for (size_t i = 0; i < rendererComponent._maxFramesInFlight; i++)
	{
		rendererComponent._imageAvailableSemaphores.emplace_back(
			std::move(vk::raii::Semaphore(*rendererComponent._device, semaphoreInfo)));
		rendererComponent._renderFinishedSemaphores.emplace_back(
			std::move(vk::raii::Semaphore(*rendererComponent._device, semaphoreInfo)));
		rendererComponent._inFlightFences.emplace_back(
			std::move(vk::raii::Fence(*rendererComponent._device, fenceInfo)));
	}
}

void RecreateSwapChain(World&, RendererComponent& rendererComponent)
{
	rendererComponent._recreatedSwapChain = true;
	rendererComponent._device->waitIdle();
	spdlog::info("Recreating swapchain");

	vk::raii::Device& device = *rendererComponent._device;

	std::fill(rendererComponent._imagesInFlight.begin(), rendererComponent._imagesInFlight.end(), nullptr);
	createSwapChain(rendererComponent);
	rendererComponent._swapChainImageViews = getSwapChainImageViewHandles(
		*rendererComponent._device, *rendererComponent._swapChain, rendererComponent._swapChainImageFormat);
	rendererComponent._renderPass = createRenderPass(device, rendererComponent._swapChainImageFormat);
	rendererComponent._swapChainFrameBuffers = createFrameBuffers(device, *rendererComponent._renderPass,
		rendererComponent._swapChainImageViews, rendererComponent._swapChainExtent);
	createCameraBuffers(rendererComponent);
	rendererComponent._commandBuffers =
		createCommandBuffers(device, *rendererComponent._commandPool, rendererComponent._swapChainImageViews.size());
	createSyncObjects(rendererComponent);
}

void BeginFrame(const Camera& camera)
{
}

void RendererBeginFrameSystem::Initialize(World& world)
{
	ArchetypeMask rendererMask =
		ArchetypeMaskFromComponents<RendererComponent, CameraComponent, TileMapBrushComponent>();

	Entity entity = world.AccessEntityManager().CreateEntity(rendererMask);
	auto& render = world.AccessEntityManager().AccessComponent<RendererComponent>(entity);

	render.OnRecreateSwapChain.emplace_back(RecreateSwapChain);

	render._window = SDL_CreateWindow(APP_NAME, SDL_WINDOWPOS_CENTERED, SDL_WINDOWPOS_CENTERED, render._windowWidth,
		render._windowHeight, SDL_WINDOW_VULKAN | SDL_WINDOW_SHOWN /* | SDL_WINDOW_RESIZABLE */);
	if (!render._window)
	{
		SDL_Quit();
		throw std::runtime_error("Failed to initialize SDL window");
	}

	// Get available vulkan extensions, necessary for interfacing with native
	// window SDL takes care of this call and returns, next to the default
	// VK_KHR_surface a platform specific extension When initializing the vulkan
	// instance these extensions have to be enabled in order to create a valid
	// surface later on.
	std::vector<std::string> found_extensions = getAvailableVulkanExtensions(render._window);

	// Get available vulkan layer extensions, notify when not all could be found
	std::vector<std::string> found_layers = getAvailableVulkanLayers();

	// Warn when not all requested layers could be found
	if (found_layers.size() != getRequestedLayerNames().size())
		spdlog::warn("warning! not all requested layers could be found!");

	render._context = std::make_unique<vk::raii::Context>();

	// Create Vulkan Instance
	render._instance = createVulkanInstance(*render._context, found_layers, found_extensions);

	// Vulkan messaging callback
	render._debugCallback = setupDebugCallback(*render._instance);

	render._physicalDevice =
		std::make_unique<vk::raii::PhysicalDevice>(std::move(vk::raii::PhysicalDevices(*render._instance).front()));

	render._graphicsQueueIndex = selectGPU(*render._physicalDevice);

	// Create a logical device that interfaces with the physical device
	render._device = createLogicalDevice(*render._physicalDevice, render._graphicsQueueIndex, found_layers);

	// Create the surface we want to render to, associated with the window we
	// created before This call also checks if the created surface is compatible
	// with the previously selected physical device and associated render queue
	render._presentationSurface =
		createSurface(*render._instance, *render._physicalDevice, render._window, render._graphicsQueueIndex);

	// Create swap chain
	createSwapChain(render);

	render._graphicsQueue = std::make_unique<vk::raii::Queue>(*render._device, render._graphicsQueueIndex, 0);

	// Get image handles from swap chain
	render._swapChainImageViews =
		getSwapChainImageViewHandles(*render._device, *render._swapChain, render._swapChainImageFormat);

	render._renderPass = createRenderPass(*render._device, render._swapChainImageFormat);


	render._swapChainFrameBuffers =
		createFrameBuffers(*render._device, *render._renderPass, render._swapChainImageViews, render._swapChainExtent);
	render._commandPool = createCommandPool(*render._device, render._graphicsQueueIndex);
	createCameraBuffers(render);
	render._descriptorPool = createDescriptorPool(*render._device);

	render._commandBuffers =
		createCommandBuffers(*render._device, *render._commandPool, render._swapChainFrameBuffers.size());
	createSyncObjects(render);
}

void RendererBeginFrameSystem::Uninitialize(World& world)
{
	auto* pRendererComponent = world.AccessEntityManager().AccessFirstComponent<RendererComponent>();
	if (!pRendererComponent)
	{
		return;
	}
	pRendererComponent->_graphicsQueue->waitIdle();
}

void UpdateComponents(ComponentUpdateParameters& Params, ComponentBucket<RendererComponent>& Renderers,
	const ComponentBucket<CameraComponent>& Cameras)
{
	World& World = Params.World;
	for (size_t i = Params.Start; i < Params.End; ++i)
	{
		RendererComponent& render = Renderers.AccessComponent(i);
		const CameraComponent& cameraComponent = Cameras.GetComponent(i);
		Camera cam;
		cam.Offset = cameraComponent.Offset;
		cam.Zoom = cameraComponent.Zoom;

		if (!render._draw)
			return;

		vk::Result result = render._device->waitForFences(
			*render._inFlightFences[render._currentFrame], true, std::numeric_limits<uint64_t>::max());

		auto pairResultImageIndex = render._swapChain->acquireNextImage(
			std::numeric_limits<uint64_t>::max(), *render._imageAvailableSemaphores[render._currentFrame]);
		result = pairResultImageIndex.first;

		if (result == vk::Result::eErrorOutOfDateKHR || render._recreateSwapChain)
		{
			render._currentFrame = 0;
			for (auto& Callback : render.OnRecreateSwapChain)
			{
				Callback(World, render);
			}
			render._recreateSwapChain = false;
			result = render._device->waitForFences(
				*render._inFlightFences[render._currentFrame], true, std::numeric_limits<uint64_t>::max());

			pairResultImageIndex = render._swapChain->acquireNextImage(
				std::numeric_limits<uint64_t>::max(), *render._imageAvailableSemaphores[render._currentFrame]);

			result = pairResultImageIndex.first;
		}
		if (result != vk::Result::eSuccess && result != vk::Result::eSuboptimalKHR)
		{
			throw std::runtime_error("failed to acquire swap chain image!");
		}
		render._currentImageIndex = pairResultImageIndex.second;

		if (render._imagesInFlight[render._currentImageIndex] != nullptr)
		{
			result = render._device->waitForFences(
				**render._imagesInFlight[render._currentImageIndex], true, std::numeric_limits<uint64_t>::max());
		}
		render._imagesInFlight[render._currentImageIndex] = &render._inFlightFences[render._currentFrame];

		updateCameraBuffers(render._cameraBufferMemories, cam, render._currentImageIndex);

		vk::CommandBufferBeginInfo beginInfo{};

		auto& buffer = render._commandBuffers[render._currentImageIndex];

		buffer.begin(beginInfo);

		vk::RenderPassBeginInfo renderPassInfo{};
		renderPassInfo.renderPass = **render._renderPass;
		renderPassInfo.framebuffer = *render._swapChainFrameBuffers[render._currentImageIndex];

		renderPassInfo.renderArea.offset = vk::Offset2D{ 0, 0 };
		renderPassInfo.renderArea.extent = render._swapChainExtent;

		vk::ClearValue clearColor = vk::ClearColorValue{ std::array<float, 4>{ 0.0f, 0.0f, 0.0f, 1.0f } };
		renderPassInfo.clearValueCount = 1;
		renderPassInfo.pClearValues = &clearColor;

		buffer.beginRenderPass(renderPassInfo, vk::SubpassContents::eInline);
	}
}

void RendererBeginFrameSystem::Update(World& world)
{
	world.AccessEntityManager().ForeachSerial(world, std::function(UpdateComponents));
}

ArchetypeReadWriteMaskPair RendererBeginFrameSystem::RegisterComponents(ComponentStorageRegistry& registry)
{
	registry.RegisterComponent<RendererComponent>();
	registry.RegisterComponent<CameraComponent>();
	registry.RegisterComponent<TileMapBrushComponent>();

	ArchetypeReadWriteMaskPair Pair = ArchetypeReadWriteMaskPairFromFunction(registry, std::function(UpdateComponents));
	return Pair;
}
