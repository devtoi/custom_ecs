#include "sprite_render_system.h"

#include <glm/vec3.hpp>
#include <random>

#include <ecs/archetype_bucket.h>
#include <ecs/component/position_component.h>
#include <ecs/component/rendering/renderer_component.h>
#include <ecs/component/rendering/sprite_render_pipeline_component.h>
#include <ecs/component/rendering/sprite_render_settings_component.h>
#include <ecs/component/game/pheromone_map_component.h>
#include <ecs/component/game/food_mover_component.h>
#include <ecs/component/game/food_storage_component.h>
#include <ecs/component/tile_map_component.h>
#include <ecs/component/velocity_component.h>
#include <ecs/component/camera_component.h>
#include <ecs/world.h>
#include <render/sprite.h>
#include <render/vertex.h>
#include <render/utilities.h>
#include <render/camera.h>

namespace
{
	static constexpr glm::vec3 ANT_COLOR{ 0, 0, 1 };
	static constexpr glm::vec3 CARRYING_ANT_COLOR{ 1, 0, 1 };
}

SpriteRenderSystem::SpriteRenderSystem() : System(SpriteRenderSystem::NAME)
{
}

std::vector<vk::raii::DescriptorSet> createDescriptorSets(vk::raii::Device& device,
	vk::raii::DescriptorPool& descriptorPool, size_t swapChainImageCount,
	const vk::raii::DescriptorSetLayout& cameraDescriptorSetLayout, const std::vector<vk::raii::Buffer>& cameraBuffers)
{
	std::vector<vk::DescriptorSetLayout> layouts(swapChainImageCount, *cameraDescriptorSetLayout);

	vk::DescriptorSetAllocateInfo allocInfo{};
	allocInfo.descriptorPool = *descriptorPool;
	allocInfo.descriptorSetCount = static_cast<uint32_t>(swapChainImageCount);
	allocInfo.pSetLayouts = layouts.data();

	std::vector<vk::raii::DescriptorSet> descriptorSets = std::move(vk::raii::DescriptorSets(device, allocInfo));

	for (size_t i = 0; i < swapChainImageCount; ++i)
	{
		std::vector<vk::WriteDescriptorSet> writeDescriptorSet;

		{
			vk::DescriptorBufferInfo bufferInfo;
			bufferInfo.buffer = *cameraBuffers[i];
			bufferInfo.offset = 0;
			bufferInfo.range = sizeof(Camera);

			vk::WriteDescriptorSet descriptorWrite;
			descriptorWrite.dstSet = *descriptorSets[i];
			descriptorWrite.dstBinding = 0;
			descriptorWrite.dstArrayElement = 0;
			descriptorWrite.descriptorType = vk::DescriptorType::eUniformBuffer;
			descriptorWrite.descriptorCount = 1;
			descriptorWrite.pBufferInfo = &bufferInfo;
			descriptorWrite.pImageInfo = nullptr;
			descriptorWrite.pTexelBufferView = nullptr;

			writeDescriptorSet.emplace_back(descriptorWrite);
		}

		device.updateDescriptorSets(writeDescriptorSet, nullptr);
	}
	return descriptorSets;
}

void createPipeline(RendererComponent& rendererComponent, SpriteRenderPipelineComponent& spriteRenderPipeline)
{
	auto vertShaderCode = renderer::utilities::readFile("shader/vert.vert.spv");
	auto fragShaderCode = renderer::utilities::readFile("shader/frag.frag.spv");

	std::unique_ptr<vk::raii::ShaderModule> vertShaderModule =
		renderer::utilities::createShaderModule(*rendererComponent._device, vertShaderCode);
	std::unique_ptr<vk::raii::ShaderModule> fragShaderModule =
		renderer::utilities::createShaderModule(*rendererComponent._device, fragShaderCode);

	vk::PipelineShaderStageCreateInfo vertShaderStageInfo{};
	vertShaderStageInfo.stage = vk::ShaderStageFlagBits::eVertex;
	vertShaderStageInfo.module = **vertShaderModule;
	vertShaderStageInfo.pName = "main";

	vk::PipelineShaderStageCreateInfo fragShaderStageInfo{};
	fragShaderStageInfo.stage = vk::ShaderStageFlagBits::eFragment;
	fragShaderStageInfo.module = **fragShaderModule;
	fragShaderStageInfo.pName = "main";

	vk::PipelineShaderStageCreateInfo shaderStages[] = { vertShaderStageInfo, fragShaderStageInfo };

	auto bindingDescription = Vertex::getBindingDescription();
	auto attributeDescriptions = Vertex::getAttributeDescriptions();

	vk::PipelineVertexInputStateCreateInfo vertexInputInfo{};
	vertexInputInfo.vertexBindingDescriptionCount = 1;
	vertexInputInfo.vertexAttributeDescriptionCount = static_cast<uint32_t>(attributeDescriptions.size());
	vertexInputInfo.pVertexBindingDescriptions = &bindingDescription;
	vertexInputInfo.pVertexAttributeDescriptions = attributeDescriptions.data();

	vk::PipelineInputAssemblyStateCreateInfo inputAssembly{};
	inputAssembly.topology = vk::PrimitiveTopology::eTriangleList;
	inputAssembly.primitiveRestartEnable = false;

	vk::Viewport viewport{};
	viewport.x = 0.0f;
	viewport.y = 0.0f;
	viewport.width = (float)rendererComponent._swapChainExtent.width;
	viewport.height = (float)rendererComponent._swapChainExtent.height;
	viewport.minDepth = 0.0f;
	viewport.maxDepth = 1.0f;

	vk::Rect2D scissor{};
	scissor.offset = vk::Offset2D{ 0, 0 };
	scissor.extent = rendererComponent._swapChainExtent;

	vk::PipelineViewportStateCreateInfo viewportState{};
	viewportState.viewportCount = 1;
	viewportState.pViewports = &viewport;
	viewportState.scissorCount = 1;
	viewportState.pScissors = &scissor;

	vk::PipelineRasterizationStateCreateInfo rasterizer{};
	rasterizer.depthClampEnable = false;
	rasterizer.rasterizerDiscardEnable = false;
	rasterizer.polygonMode = vk::PolygonMode::eFill;
	rasterizer.lineWidth = 1.0f;
	rasterizer.cullMode = vk::CullModeFlagBits::eBack;
	rasterizer.frontFace = vk::FrontFace::eClockwise;
	rasterizer.depthBiasEnable = false;
	rasterizer.depthBiasConstantFactor = 0.0f; // Optional
	rasterizer.depthBiasClamp = 0.0f;		   // Optional
	rasterizer.depthBiasSlopeFactor = 0.0f;	   // Optional

	vk::PipelineMultisampleStateCreateInfo multisampling{};
	multisampling.sampleShadingEnable = false;
	multisampling.rasterizationSamples = vk::SampleCountFlagBits::e1;
	multisampling.minSampleShading = 1.0f;		 // Optional
	multisampling.alphaToCoverageEnable = false; // Optional
	multisampling.alphaToOneEnable = false;		 // Optional

	vk::PipelineColorBlendAttachmentState colorBlendAttachment{};
	colorBlendAttachment.colorWriteMask = vk::ColorComponentFlagBits::eR | vk::ColorComponentFlagBits::eG |
										  vk::ColorComponentFlagBits::eB | vk::ColorComponentFlagBits::eA;
	colorBlendAttachment.blendEnable = true;
	colorBlendAttachment.srcColorBlendFactor = vk::BlendFactor::eSrcAlpha;
	colorBlendAttachment.dstColorBlendFactor = vk::BlendFactor::eOneMinusSrcAlpha;
	colorBlendAttachment.colorBlendOp = vk::BlendOp::eAdd;
	colorBlendAttachment.srcAlphaBlendFactor = vk::BlendFactor::eOne;
	colorBlendAttachment.dstAlphaBlendFactor = vk::BlendFactor::eZero;
	colorBlendAttachment.alphaBlendOp = vk::BlendOp::eAdd;

	vk::PipelineColorBlendStateCreateInfo colorBlending{};
	colorBlending.logicOpEnable = false;
	colorBlending.logicOp = vk::LogicOp::eCopy; // Optional
	colorBlending.attachmentCount = 1;
	colorBlending.pAttachments = &colorBlendAttachment;
	colorBlending.blendConstants[0] = 0.0f; // Optional
	colorBlending.blendConstants[1] = 0.0f; // Optional
	colorBlending.blendConstants[2] = 0.0f; // Optional
	colorBlending.blendConstants[3] = 0.0f; // Optional

	vk::DynamicState dynamicStates[] = { vk::DynamicState::eViewport, vk::DynamicState::eLineWidth };

	vk::PipelineDynamicStateCreateInfo dynamicState{};
	dynamicState.dynamicStateCount = 2;
	dynamicState.pDynamicStates = dynamicStates;

	vk::PipelineLayoutCreateInfo pipelineLayoutInfo{};
	pipelineLayoutInfo.setLayoutCount = 1;
	pipelineLayoutInfo.pSetLayouts = &**spriteRenderPipeline._cameraDescriptorSetLayout; // Optional
	pipelineLayoutInfo.pushConstantRangeCount = 0;										 // Optional

	spriteRenderPipeline._pipelineLayout =
		std::make_unique<vk::raii::PipelineLayout>(*rendererComponent._device, pipelineLayoutInfo);

	vk::GraphicsPipelineCreateInfo pipelineInfo{};
	pipelineInfo.stageCount = 2;
	pipelineInfo.pStages = shaderStages;

	pipelineInfo.pVertexInputState = &vertexInputInfo;
	pipelineInfo.pInputAssemblyState = &inputAssembly;
	pipelineInfo.pViewportState = &viewportState;
	pipelineInfo.pRasterizationState = &rasterizer;
	pipelineInfo.pMultisampleState = &multisampling;
	pipelineInfo.pColorBlendState = &colorBlending;
	pipelineInfo.layout = **spriteRenderPipeline._pipelineLayout;
	pipelineInfo.renderPass = **rendererComponent._renderPass;
	pipelineInfo.subpass = 0;
	pipelineInfo.basePipelineIndex = -1; // Optional

	spriteRenderPipeline._graphicsPipeline =
		std::make_unique<vk::raii::Pipeline>(*rendererComponent._device, nullptr, pipelineInfo);
}

void createVertexBuffer(RendererComponent& rendererComponent, SpriteRenderPipelineComponent& spriteRenderPipeline)
{
	const vk::DeviceSize bufferSize = sizeof(Vertex) * SpriteRenderPipelineComponent::MAX_SPRITES * 6;
	renderer::utilities::createBuffer(*rendererComponent._physicalDevice, *rendererComponent._device, bufferSize,
		vk::BufferUsageFlagBits::eVertexBuffer,
		vk::MemoryPropertyFlagBits::eHostVisible | vk::MemoryPropertyFlagBits::eHostCoherent,
		spriteRenderPipeline._vertexBuffer, spriteRenderPipeline._vertexBufferMemory, "Sprite vertex buffer");
	spriteRenderPipeline._vertexBufferCPUMap = spriteRenderPipeline._vertexBufferMemory->mapMemory(0, bufferSize, {});
}

std::unique_ptr<vk::raii::DescriptorSetLayout> createDescriptorSetLayout(vk::raii::Device& device)
{
	std::vector<vk::DescriptorSetLayoutBinding> layoutBindings;
	// Camera
	{
		vk::DescriptorSetLayoutBinding cameraLayoutBinding;
		cameraLayoutBinding.binding = 0;
		cameraLayoutBinding.descriptorType = vk::DescriptorType::eUniformBuffer;
		cameraLayoutBinding.descriptorCount = 1;
		cameraLayoutBinding.stageFlags = vk::ShaderStageFlagBits::eVertex;
		layoutBindings.emplace_back(cameraLayoutBinding);
	}

	vk::DescriptorSetLayoutCreateInfo create_info;
	create_info.bindingCount = layoutBindings.size();
	create_info.pBindings = layoutBindings.data();

	return std::make_unique<vk::raii::DescriptorSetLayout>(device, create_info);
}


void SpriteRenderSystem::Initialize(World& world)
{
	auto* pRenderer = world.AccessEntityManager().AccessFirstComponent<RendererComponent>();
	if (!pRenderer)
		return;
	RendererComponent& render = *pRenderer;

	ArchetypeMask mask = ArchetypeMaskFromComponents<SpriteRenderPipelineComponent, SpriteRenderSettingsComponent>();
	Entity entity = world.AccessEntityManager().CreateEntity(mask);
	auto& SpriteRenderPipeline =
		world.AccessEntityManager().AccessComponent<SpriteRenderPipelineComponent>(entity);

	{
		SpriteRenderPipeline._cameraDescriptorSetLayout = createDescriptorSetLayout(*render._device);
		createPipeline(render, SpriteRenderPipeline);
		createVertexBuffer(render, SpriteRenderPipeline);
		SpriteRenderPipeline._descriptorSets =
			createDescriptorSets(*render._device, *render._descriptorPool, render._swapChainImageViews.size(),
				*SpriteRenderPipeline._cameraDescriptorSetLayout, render._cameraBuffers);
	}

	{
		render.OnRecreateSwapChain.emplace_back([&](World&, RendererComponent& rendererComponent) {
			SpriteRenderPipeline._cameraDescriptorSetLayout = createDescriptorSetLayout(*rendererComponent._device);
			createPipeline(rendererComponent, SpriteRenderPipeline);
			SpriteRenderPipeline._descriptorSets = createDescriptorSets(*rendererComponent._device,
				*rendererComponent._descriptorPool, rendererComponent._swapChainImageViews.size(),
				*SpriteRenderPipeline._cameraDescriptorSetLayout, rendererComponent._cameraBuffers);
		});
	}
}

void SpriteRenderSystem::Update(World& world)
{
	auto* pSpriteRenderSettings = world.AccessEntityManager().AccessFirstComponent<SpriteRenderSettingsComponent>();
	if (!pSpriteRenderSettings)
	{
		return;
	}
	if (pSpriteRenderSettings->Render)
	{
		_Sprites.clear();

		if (pSpriteRenderSettings->RenderPheromones)
		{
			const PheromonMapComponent& PheromonMap =
				*world.GetEntityManager().GetFirstComponent<PheromonMapComponent>();
			for (int x = 0; x < MAP_SIZE_WIDTH_TILES; ++x)
			{
				for (int y = 0; y < MAP_SIZE_HEIGHT_TILES; ++y)
				{
					const auto& Food = PheromonMap.FoodPheromoneTiles[x][y];
					const auto& Home = PheromonMap.HomePheromoneTiles[x][y];
					if (Food > PheromoneStrength{ 0 } || Home > PheromoneStrength{ 0 })
					{
						auto renderPosition = glm::vec2(x * TILE_SIZE, y * TILE_SIZE);
						_Sprites.push_back(Sprite{ renderPosition, TILE_SIZE,
							glm::vec3(Food / MAX_FOOD_PHEROMONE_STRENGTH, Home / MAX_HOME_PHEROMONE_STRENGTH, 0.0f) });
					}
				}
			}
		}

		world.AccessEntityManager().ForeachSerial(world,
			std::function([&](ComponentUpdateParameters& Params, const ComponentBucket<PositionComponent>& Positions,
							  const ComponentBucket<FoodStorageComponent>&  /*Velocitys*/) {
				for (size_t i = Params.Start; i < Params.End; ++i)
				{
					const auto& Position = Positions.GetComponent(i);
					auto renderPosition = glm::vec2(Position.position.x * TILE_SIZE, Position.position.y * TILE_SIZE);
					_Sprites.push_back(Sprite{ renderPosition, TILE_SIZE, glm::vec3(1.0f, 1.0f, 1.0f) });
				}
			}));

		world.AccessEntityManager().ForeachSerial(world,
			std::function(
				[&](ComponentUpdateParameters& params, const ComponentBucket<PositionComponent>& PositionComponents,
					const ComponentBucket<VelocityComponent>& VelocityComponents,
					const ComponentBucket<FoodMoverComponent>& FoodMovers) {
					BuildSpriteList(params, PositionComponents, VelocityComponents, FoodMovers);
				}));


		auto* pRenderer = world.AccessEntityManager().AccessFirstComponent<RendererComponent>();
		if (!pRenderer)
			return;
		RendererComponent& render = *pRenderer;

		auto* pSpriteRenderPipeline =
			world.AccessEntityManager().AccessFirstComponent<SpriteRenderPipelineComponent>();
		if (!pSpriteRenderPipeline)
			return;
		SpriteRenderPipelineComponent& SpriteRenderPipeline = *pSpriteRenderPipeline;

		std::vector<Vertex> vertices;
		for (const auto& sprite : _Sprites)
		{
			auto x = sprite.pos.x;
			auto y = sprite.pos.y;
			vertices.emplace_back(Vertex{ glm::vec2(x, y), sprite.color });
			vertices.emplace_back(Vertex{ glm::vec2(x + sprite.size, y), sprite.color });
			vertices.emplace_back(Vertex{ glm::vec2(x, y + sprite.size), sprite.color });
			vertices.emplace_back(Vertex{ glm::vec2(x, y + sprite.size), sprite.color });
			vertices.emplace_back(Vertex{ glm::vec2(x + sprite.size, y), sprite.color });
			vertices.emplace_back(Vertex{ glm::vec2(x + sprite.size, y + sprite.size), sprite.color });
		}

		const vk::DeviceSize bufferSize = sizeof(Vertex) * vertices.size();
		memcpy(SpriteRenderPipeline._vertexBufferCPUMap, vertices.data(), (size_t)bufferSize);
		SpriteRenderPipeline._vertexCount = vertices.size();

		auto& buffer = render._commandBuffers[render._currentImageIndex];
		buffer.bindPipeline(vk::PipelineBindPoint::eGraphics, **SpriteRenderPipeline._graphicsPipeline);

		buffer.bindVertexBuffers(0, **SpriteRenderPipeline._vertexBuffer, { 0 });

		buffer.bindDescriptorSets(vk::PipelineBindPoint::eGraphics, **SpriteRenderPipeline._pipelineLayout, 0,
			*SpriteRenderPipeline._descriptorSets[render._currentImageIndex], nullptr);

		buffer.draw(SpriteRenderPipeline._vertexCount, 1, 0, 0);
	}
}

void SpriteRenderSystem::BuildSpriteList(ComponentUpdateParameters& params,
	const ComponentBucket<PositionComponent>& PositionComponents,
	const ComponentBucket<VelocityComponent>& VelocityComponents, const ComponentBucket<FoodMoverComponent>& FoodMovers)
{
	for (size_t i = params.Start; i < params.End; ++i)
	{
		const PositionComponent& PositionComponent = PositionComponents.GetComponent(i);
		const VelocityComponent& VelocityComponent = VelocityComponents.GetComponent(i);
		const FoodMoverComponent& FoodMover = FoodMovers.GetComponent(i);
		auto renderPosition =
			glm::vec2(PositionComponent.position.x * TILE_SIZE, PositionComponent.position.y * TILE_SIZE);
		// TODO account for diagonals
		renderPosition += glm::vec2(VelocityComponent.direction.x * static_cast<float>(TILE_SIZE),
							  VelocityComponent.direction.y * static_cast<float>(TILE_SIZE)) *
						  ((static_cast<float>(TILE_SUB_SIZE) - static_cast<float>(VelocityComponent.left)) /
							  static_cast<float>(TILE_SUB_SIZE));
		_Sprites.push_back(Sprite{ renderPosition, TILE_SIZE,
			/* glm::vec3((vel.direction.x + 1) / 2.0f, 0.1, (vel.direction.y + 1) / 2.0f) }); */
			FoodMover.FoodAmount > 0 ? CARRYING_ANT_COLOR : ANT_COLOR });
	}
}

ArchetypeReadWriteMaskPair SpriteRenderSystem::RegisterComponents(ComponentStorageRegistry& registry)
{
	registry.RegisterComponent<SpriteRenderPipelineComponent>();
	registry.RegisterComponent<SpriteRenderSettingsComponent>();
	// TODOJM Proper dependencies
	ArchetypeReadWriteMaskPair Pair = System::RegisterComponents(registry);
	Pair.ComponentReadMask |= ArchetypeMaskFromComponents<PositionComponent, RendererComponent,
		SpriteRenderPipelineComponent, PheromonMapComponent, FoodMoverComponent, FoodStorageComponent, TileMapComponent,
		VelocityComponent, CameraComponent, SpriteRenderSettingsComponent>();
	Pair.ComponentWriteMask |= ArchetypeMaskFromComponents<TileMapComponent, PheromonMapComponent>();

	return Pair;
}
