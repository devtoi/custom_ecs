#pragma once

#include <ecs/system/system.h>

class TileMapRenderSystem final : public System
{
public:
	static constexpr std::string_view NAME = "TileMapRenderSystem";
	TileMapRenderSystem();
	ArchetypeReadWriteMaskPair RegisterComponents(ComponentStorageRegistry& registry) override;
	void Initialize(World& world) override;
	void Update(World& world) override;
};
