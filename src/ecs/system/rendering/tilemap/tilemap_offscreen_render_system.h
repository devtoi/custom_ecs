#pragma once

#include <ecs/system/system.h>

class TileMapOffscreenRenderSystem final : public System
{
public:
	static constexpr std::string_view NAME = "TileMapOffscreenRenderSystem";
	TileMapOffscreenRenderSystem();
	ArchetypeReadWriteMaskPair RegisterComponents(ComponentStorageRegistry& registry) override;
	void Initialize(World& world) override;
	void Update(World& world) override;
};
