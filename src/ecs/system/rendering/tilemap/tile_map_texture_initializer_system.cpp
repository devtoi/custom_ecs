#include "tile_map_texture_initializer_system.h"

#include <spdlog/spdlog.h>

#include "ecs/component/tile_map_component.h"
#include "render/utilities.h"
#include "ecs/component/position_component.h"
#include "render/vertex.h"
#include <ecs/component/rendering/renderer_component.h>
#include <ecs/component/rendering/tile_map_texture_component.h>
#include <ecs/component/rendering/tile_map_render_pipeline_component.h>
#include <ecs/component/rendering/tile_map_offscreen_render_pipeline_component.h>
#include <ecs/component/rendering/sprite_render_pipeline_component.h>
#include <ecs/world.h>

namespace
{
	constexpr vk::Format RT_FORMAT = vk::Format::eB8G8R8A8Unorm;
}

TileMapTextureInitializerSystem::TileMapTextureInitializerSystem() : System(TileMapTextureInitializerSystem::NAME)
{
}

void SetupTexture(ComponentUpdateParameters& Params, const ComponentBucket<PositionComponent>& Positions,
	ComponentBucket<TileMapTextureComponent>& TileMapTextures)
{
	auto& World = Params.World;
	auto* pRenderer = World.AccessEntityManager().AccessFirstComponent<RendererComponent>();
	auto* pTMORender = World.AccessEntityManager().AccessFirstComponent<TilemapOffscreenRenderPipelineComponent>();
	auto* pTMRender = World.AccessEntityManager().AccessFirstComponent<TilemapRenderPipelineComponent>();
	if (!pRenderer || !pTMORender || !pTMRender)
	{
		return;
	}
	auto& render = *pRenderer;
	auto& tmoRender = *pTMORender;
	auto& tmRender = *pTMRender;

	for (size_t i = Params.Start; i < Params.End; ++i)
	{
		auto& texture = TileMapTextures.AccessComponent(i);
		const auto& pos = Positions.GetComponent(i);
		// Vertex Buffer
		{
			const vk::DeviceSize bufferSize = sizeof(Vertex) * SpriteRenderPipelineComponent::MAX_SPRITES * 6;
			renderer::utilities::createBuffer(*render._physicalDevice, *render._device, bufferSize,
				vk::BufferUsageFlagBits::eVertexBuffer,
				vk::MemoryPropertyFlagBits::eHostVisible | vk::MemoryPropertyFlagBits::eHostCoherent,
				texture._vertexBuffer, texture._vertexBufferMemory, "Tile map vertex buffer");
			texture._vertexBufferCPUMap = texture._vertexBufferMemory->mapMemory(0, bufferSize, {});
		}
		// Image
		{
			vk::ImageCreateInfo createInfo;
			createInfo.imageType = vk::ImageType::e2D;
			createInfo.format = RT_FORMAT;
			createInfo.extent = vk::Extent3D{ MAP_SIZE_WIDTH_PIXELS, MAP_SIZE_HEIGHT_PIXELS, 1 };
			createInfo.mipLevels = 1;
			createInfo.arrayLayers = 1;
			createInfo.samples = vk::SampleCountFlagBits::e1;
			createInfo.tiling = vk::ImageTiling::eOptimal;
			createInfo.usage = vk::ImageUsageFlagBits::eColorAttachment | vk::ImageUsageFlagBits::eSampled |
							   vk::ImageUsageFlagBits::eTransferDst | vk::ImageUsageFlagBits::eTransferSrc;
			// createInfo.sharingMode = vk::SharingMode::eExclusive;
			createInfo.queueFamilyIndexCount = {};
			createInfo.pQueueFamilyIndices = {};
			createInfo.initialLayout = vk::ImageLayout::eUndefined;

			texture._rtImage = std::make_unique<vk::raii::Image>(*render._device, createInfo);
			auto name = fmt::format("TileMapImage {},{}", pos.position.x, pos.position.y);
			renderer::utilities::SetDebugName(*render._device, *texture._rtImage, name);
		}
		// Image Memory
		{
			auto memRequirements = texture._rtImage->getMemoryRequirements();

			vk::MemoryAllocateInfo allocInfo{};
			allocInfo.allocationSize = memRequirements.size;
			allocInfo.memoryTypeIndex = renderer::utilities::findMemoryType(
				*render._physicalDevice, memRequirements.memoryTypeBits, vk::MemoryPropertyFlagBits::eDeviceLocal);

			texture._rtImageMemory = std::make_unique<vk::raii::DeviceMemory>(*render._device, allocInfo);
			auto name = fmt::format("TileMapImageMemory {},{}", pos.position.x, pos.position.y);

			texture._rtImage->bindMemory(**texture._rtImageMemory, 0);
			renderer::utilities::SetDebugName(*render._device, *texture._rtImageMemory, name);
		}
		// Image view
		{
			vk::ImageViewCreateInfo createInfo;
			createInfo.viewType = vk::ImageViewType::e2D;
			createInfo.format = RT_FORMAT;
			createInfo.subresourceRange.aspectMask = vk::ImageAspectFlagBits::eColor;
			createInfo.subresourceRange.baseMipLevel = 0;
			createInfo.subresourceRange.levelCount = 1;
			createInfo.subresourceRange.baseArrayLayer = 0;
			createInfo.subresourceRange.layerCount = 1;
			createInfo.image = **texture._rtImage;

			texture._rtImageView = std::make_unique<vk::raii::ImageView>(*render._device, createInfo);
			auto name = fmt::format("TileMapImageView {},{}", pos.position.x, pos.position.y);
			renderer::utilities::SetDebugName(*render._device, *texture._rtImageView, name);
		}
		// Sampler
		{
			vk::SamplerCreateInfo ci;
			ci.magFilter = vk::Filter::eLinear;
			ci.minFilter = vk::Filter::eLinear;
			ci.mipmapMode = vk::SamplerMipmapMode::eLinear;
			ci.addressModeU = vk::SamplerAddressMode::eClampToEdge;
			ci.addressModeV = ci.addressModeU;
			ci.addressModeW = ci.addressModeU;
			ci.mipLodBias = 0.0f;
			ci.maxAnisotropy = 1.0f;
			ci.minLod = 0.0f;
			ci.maxLod = 0.0f;
			ci.borderColor = vk::BorderColor::eFloatOpaqueWhite;

			texture._rtSampler = std::make_unique<vk::raii::Sampler>(*render._device, ci);
		}
		// Framebuffer
		{
			vk::FramebufferCreateInfo ci;
			ci.renderPass = **tmoRender.RenderPass;
			ci.attachmentCount = 1;
			ci.pAttachments = &**texture._rtImageView;
			ci.width = MAP_SIZE_WIDTH_PIXELS;
			ci.height = MAP_SIZE_HEIGHT_PIXELS;
			ci.layers = 1;

			texture._rtFrameBuffer = std::make_unique<vk::raii::Framebuffer>(*render._device, ci);
		}
		// Descriptor sets
		{
			std::vector<vk::DescriptorSetLayout> layouts(
				render._swapChainImageViews.size(), **tmRender.DescriptorSetLayout);

			vk::DescriptorSetAllocateInfo allocInfo{};
			allocInfo.descriptorPool = **render._descriptorPool;
			allocInfo.descriptorSetCount = static_cast<uint32_t>(layouts.size());
			allocInfo.pSetLayouts = layouts.data();

			texture._DescriptorSets = std::move(vk::raii::DescriptorSets(*render._device, allocInfo));
			for (auto& DescSet : texture._DescriptorSets)
			{
				renderer::utilities::SetDebugName(*render._device, DescSet, "Tile map descriptor set");
			}
		}
		{
			vk::FenceCreateInfo fenceInfo{};
			fenceInfo.flags = vk::FenceCreateFlagBits::eSignaled;
			texture.Fence = std::make_unique<vk::raii::Fence>(*render._device, fenceInfo);
			//		render._device->resetFences(**texture.Fence);
		}
		// Image Vertex Buffer
		{
			const vk::DeviceSize bufferSize = sizeof(TileMapTextureVertex) * 6;
			renderer::utilities::createBuffer(*render._physicalDevice, *render._device, bufferSize,
				vk::BufferUsageFlagBits::eVertexBuffer,
				vk::MemoryPropertyFlagBits::eHostVisible | vk::MemoryPropertyFlagBits::eHostCoherent,
				texture.OutputVertexBuffer, texture.OutputVertexBufferMemory, "Tilemap image output vertex buffer");
			void* cpuMap = texture.OutputVertexBufferMemory->mapMemory(0, bufferSize, {});
			std::array<TileMapTextureVertex, 6> vertices;
			vertices[0] = TileMapTextureVertex{ glm::vec2(pos.position.x, pos.position.y), glm::vec2(0.0f, 0.0f) };
			vertices[1] = TileMapTextureVertex{ glm::vec2(pos.position.x + MAP_SIZE_WIDTH_PIXELS, pos.position.y),
				glm::vec2(1.0f, 0.0f) };
			vertices[2] = TileMapTextureVertex{ glm::vec2(pos.position.x, pos.position.y + MAP_SIZE_HEIGHT_PIXELS),
				glm::vec2(0.0f, 1.0f) };
			vertices[3] = TileMapTextureVertex{ glm::vec2(pos.position.x, pos.position.y + MAP_SIZE_HEIGHT_PIXELS),
				glm::vec2(0.0f, 1.0f) };
			vertices[4] = TileMapTextureVertex{ glm::vec2(pos.position.x + MAP_SIZE_WIDTH_PIXELS, pos.position.y),
				glm::vec2(1.0f, 0.0f) };
			vertices[5] = TileMapTextureVertex{ glm::vec2(pos.position.x + MAP_SIZE_WIDTH_PIXELS,
													pos.position.y + MAP_SIZE_HEIGHT_PIXELS),
				glm::vec2(1.0f, 1.0f) };

			memcpy(cpuMap, vertices.data(), (size_t)bufferSize);
		}
	}
}

ArchetypeReadWriteMaskPair TileMapTextureInitializerSystem::RegisterComponents(ComponentStorageRegistry& Registry)
{
	// ArchetypeReadWriteMaskPair Pair = ArchetypeReadWriteMaskPairFromFunction(Registry, std::function(SetupTexture));
	// // Pair.ComponentReadMask |= ArchetypeMaskFromComponents<MySecondComponent>();
	// Pair.ComponentWriteMask |= ArchetypeMaskFromComponents<RendererComponent, TilemapRenderPipelineComponent,
	// 	TilemapOffscreenRenderPipelineComponent>();
	// return Pair;
	return System::RegisterComponents(Registry);
}

void TileMapTextureInitializerSystem::Initialize(World& World)
{
	World.AccessEntityManager().ForeachSerial(World, std::function(SetupTexture));
}

void TileMapTextureInitializerSystem::Update(World& World)
{
	// World.AccessEntityManager().ForeachParallell(World, std::function(UpdateComponents));
	// World.AccessEntityManager().ForeachSerial(World, std::function(UpdateComponents));
}

void TileMapTextureInitializerSystem::Uninitialize(World& World)
{
}
