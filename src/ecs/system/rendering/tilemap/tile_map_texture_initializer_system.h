#pragma once

#include <ecs/system/system.h>

class TileMapTextureInitializerSystem final : public System
{
public:
	static constexpr std::string_view NAME = "TileMapTextureInitializerSystem";
	TileMapTextureInitializerSystem();

	ArchetypeReadWriteMaskPair RegisterComponents(ComponentStorageRegistry& registry) override;
	void Initialize(World& world) override;
	void Update(World& world) override;
	void Uninitialize(World& world) override;
};
