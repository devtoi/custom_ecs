#include "tilemap_render_system.h"

#include <ecs/component/camera_component.h>
#include <ecs/component/position_component.h>
#include <ecs/component/rendering/renderer_component.h>
#include <ecs/component/rendering/tile_map_render_pipeline_component.h>
#include <ecs/component/rendering/tile_map_texture_component.h>
#include <ecs/component/tile_map_component.h>
#include <ecs/world.h>
#include <render/utilities.h>

#include "render/camera.h"

void updateDescriptorSets(TileMapTextureComponent& tilemapTexture, RendererComponent& render,
	TilemapRenderPipelineComponent& pipeline, vk::raii::ImageView* pTileMapImageView = nullptr,
	vk::raii::Sampler* pTileMapSampler = nullptr);

TileMapRenderSystem::TileMapRenderSystem() : System(TileMapRenderSystem::NAME)
{
}

void TileMapRenderSystem::Initialize(World& world)
{
	auto* pRendererComponent = world.AccessEntityManager().AccessFirstComponent<RendererComponent>();
	if (!pRendererComponent)
		return;

	RendererComponent& render = *pRendererComponent;

	ArchetypeMask PipeLineMask = ArchetypeMaskFromComponents<TilemapRenderPipelineComponent>();

	Entity pipelineEntity = world.AccessEntityManager().CreateEntity(PipeLineMask);

	auto& pipeline =
		world.AccessEntityManager().AccessComponent<TilemapRenderPipelineComponent>(pipelineEntity);

	// Bindings
	{
		std::vector<vk::DescriptorSetLayoutBinding> layoutBindings;

		//		{
		//			vk::DescriptorSetLayoutBinding cameraLayoutBinding;
		//			cameraLayoutBinding.binding = 0;
		//			cameraLayoutBinding.descriptorType = vk::DescriptorType::eUniformBuffer;
		//			cameraLayoutBinding.descriptorCount = 1;
		//			cameraLayoutBinding.stageFlags = vk::ShaderStageFlagBits::eVertex;
		//			layoutBindings.emplace_back(cameraLayoutBinding);
		//		}
		{
			vk::DescriptorSetLayoutBinding tilemapBinding;
			tilemapBinding.binding = 0;
			tilemapBinding.descriptorType = vk::DescriptorType::eCombinedImageSampler;
			tilemapBinding.descriptorCount = 1;
			tilemapBinding.stageFlags = vk::ShaderStageFlagBits::eFragment;
			layoutBindings.emplace_back(tilemapBinding);
		}

		vk::DescriptorSetLayoutCreateInfo create_info;
		create_info.bindingCount = layoutBindings.size();
		create_info.pBindings = layoutBindings.data();

		pipeline.DescriptorSetLayout = std::make_unique<vk::raii::DescriptorSetLayout>(*render._device, create_info);
		renderer::utilities::SetDebugName(
			*render._device, *pipeline.DescriptorSetLayout, "Tile map render pipeline descriptor set");
	}

	// Pipeline
	{
		auto vertShaderCode = renderer::utilities::readFile("shader/tilemap.vert.spv");
		auto fragShaderCode = renderer::utilities::readFile("shader/tilemap.frag.spv");

		std::unique_ptr<vk::raii::ShaderModule> vertShaderModule =
			renderer::utilities::createShaderModule(*render._device, vertShaderCode);
		std::unique_ptr<vk::raii::ShaderModule> fragShaderModule =
			renderer::utilities::createShaderModule(*render._device, fragShaderCode);

		vk::PipelineShaderStageCreateInfo vertShaderStageInfo{};
		vertShaderStageInfo.stage = vk::ShaderStageFlagBits::eVertex;
		vertShaderStageInfo.module = **vertShaderModule;
		vertShaderStageInfo.pName = "main";

		vk::PipelineShaderStageCreateInfo fragShaderStageInfo{};
		fragShaderStageInfo.stage = vk::ShaderStageFlagBits::eFragment;
		fragShaderStageInfo.module = **fragShaderModule;
		fragShaderStageInfo.pName = "main";

		vk::PipelineShaderStageCreateInfo shaderStages[] = { vertShaderStageInfo, fragShaderStageInfo };

		auto bindingDescription = TileMapTextureVertex::getBindingDescription();
		auto attributeDescriptions = TileMapTextureVertex::getAttributeDescriptions();

		vk::PipelineVertexInputStateCreateInfo vertexInputInfo{};
		vertexInputInfo.vertexBindingDescriptionCount = 1;
		vertexInputInfo.vertexAttributeDescriptionCount = static_cast<uint32_t>(attributeDescriptions.size());
		vertexInputInfo.pVertexBindingDescriptions = &bindingDescription;
		vertexInputInfo.pVertexAttributeDescriptions = attributeDescriptions.data();

		vk::PipelineInputAssemblyStateCreateInfo inputAssembly{};
		inputAssembly.topology = vk::PrimitiveTopology::eTriangleList;
		inputAssembly.primitiveRestartEnable = false;

		vk::Viewport viewport{};
		viewport.x = 0.0f;
		viewport.y = 0.0f;
		viewport.width = (float)render._swapChainExtent.width;
		viewport.height = (float)render._swapChainExtent.height;
		viewport.minDepth = 0.0f;
		viewport.maxDepth = 1.0f;

		vk::Rect2D scissor{};
		scissor.offset = vk::Offset2D{ 0, 0 };
		scissor.extent = render._swapChainExtent;

		vk::PipelineViewportStateCreateInfo viewportState{};
		viewportState.viewportCount = 1;
		viewportState.pViewports = &viewport;
		viewportState.scissorCount = 1;
		viewportState.pScissors = &scissor;

		vk::PipelineRasterizationStateCreateInfo rasterizer{};
		rasterizer.depthClampEnable = false;
		rasterizer.rasterizerDiscardEnable = false;
		rasterizer.polygonMode = vk::PolygonMode::eFill;
		rasterizer.lineWidth = 1.0f;
		rasterizer.cullMode = vk::CullModeFlagBits::eBack;
		rasterizer.frontFace = vk::FrontFace::eClockwise;
		rasterizer.depthBiasEnable = false;
		rasterizer.depthBiasConstantFactor = 0.0f; // Optional
		rasterizer.depthBiasClamp = 0.0f;		   // Optional
		rasterizer.depthBiasSlopeFactor = 0.0f;	   // Optional

		vk::PipelineMultisampleStateCreateInfo multisampling{};
		multisampling.sampleShadingEnable = false;
		multisampling.rasterizationSamples = vk::SampleCountFlagBits::e1;
		multisampling.minSampleShading = 1.0f;		 // Optional
		multisampling.alphaToCoverageEnable = false; // Optional
		multisampling.alphaToOneEnable = false;		 // Optional

		vk::PipelineColorBlendAttachmentState colorBlendAttachment{};
		colorBlendAttachment.colorWriteMask = vk::ColorComponentFlagBits::eR | vk::ColorComponentFlagBits::eG |
											  vk::ColorComponentFlagBits::eB | vk::ColorComponentFlagBits::eA;
		colorBlendAttachment.blendEnable = true;
		colorBlendAttachment.srcColorBlendFactor = vk::BlendFactor::eSrcAlpha;
		colorBlendAttachment.dstColorBlendFactor = vk::BlendFactor::eOneMinusSrcAlpha;
		colorBlendAttachment.colorBlendOp = vk::BlendOp::eAdd;
		colorBlendAttachment.srcAlphaBlendFactor = vk::BlendFactor::eOne;
		colorBlendAttachment.dstAlphaBlendFactor = vk::BlendFactor::eZero;
		colorBlendAttachment.alphaBlendOp = vk::BlendOp::eAdd;

		vk::PipelineColorBlendStateCreateInfo colorBlending{};
		colorBlending.logicOpEnable = false;
		colorBlending.logicOp = vk::LogicOp::eCopy; // Optional
		colorBlending.attachmentCount = 1;
		colorBlending.pAttachments = &colorBlendAttachment;
		colorBlending.blendConstants[0] = 0.0f; // Optional
		colorBlending.blendConstants[1] = 0.0f; // Optional
		colorBlending.blendConstants[2] = 0.0f; // Optional
		colorBlending.blendConstants[3] = 0.0f; // Optional

		vk::DynamicState dynamicStates[] = { vk::DynamicState::eViewport, vk::DynamicState::eLineWidth };

		vk::PipelineDynamicStateCreateInfo dynamicState{};
		dynamicState.dynamicStateCount = 2;
		dynamicState.pDynamicStates = dynamicStates;

		vk::PushConstantRange cameraPushConstants{};
		cameraPushConstants.stageFlags = vk::ShaderStageFlagBits::eVertex;
		cameraPushConstants.size = sizeof(CameraComponent);

		vk::PipelineLayoutCreateInfo pipelineLayoutInfo{};
		pipelineLayoutInfo.setLayoutCount = 1;
		pipelineLayoutInfo.pSetLayouts = &**pipeline.DescriptorSetLayout; // Optional
		pipelineLayoutInfo.pPushConstantRanges = &cameraPushConstants;
		pipelineLayoutInfo.pushConstantRangeCount = 1; // Optional

		pipeline.PipelineLayout = std::make_unique<vk::raii::PipelineLayout>(*render._device, pipelineLayoutInfo);

		vk::GraphicsPipelineCreateInfo pipelineInfo{};
		pipelineInfo.stageCount = 2;
		pipelineInfo.pStages = shaderStages;

		pipelineInfo.pVertexInputState = &vertexInputInfo;
		pipelineInfo.pInputAssemblyState = &inputAssembly;
		pipelineInfo.pViewportState = &viewportState;
		pipelineInfo.pRasterizationState = &rasterizer;
		pipelineInfo.pMultisampleState = &multisampling;
		pipelineInfo.pColorBlendState = &colorBlending;
		pipelineInfo.layout = **pipeline.PipelineLayout;
		pipelineInfo.renderPass = **render._renderPass;
		pipelineInfo.subpass = 0;
		pipelineInfo.basePipelineIndex = -1; // Optional

		pipeline.Pipeline = std::make_unique<vk::raii::Pipeline>(*render._device, nullptr, pipelineInfo);
	}
}

void UpdateComponentBuckets(ComponentUpdateParameters& Parameters, const ComponentBucket<PositionComponent>& Poses,
	const ComponentBucket<TileMapComponent>& Tilemaps, ComponentBucket<TileMapTextureComponent>& TilemapTextures)
{
	auto* pRenderer = Parameters.World.AccessEntityManager().AccessFirstComponent<RendererComponent>();
	if (!pRenderer)
		return;

	RendererComponent& render = *pRenderer;

	auto* pPipeline =
		Parameters.World.AccessEntityManager().AccessFirstComponent<TilemapRenderPipelineComponent>();
	if (!pPipeline)
		return;

	auto* pCamera = Parameters.World.AccessEntityManager().AccessFirstComponent<CameraComponent>();
	if (!pCamera)
		return;

	auto& buffer = render._commandBuffers[render._currentImageIndex];

	bool Inited = false;

	for (size_t i = Parameters.Start; i < Parameters.End; ++i)
	{
		const PositionComponent& pos = Poses.GetComponent(i);
		const TileMapComponent& tm = Tilemaps.GetComponent(i);
		TileMapTextureComponent& texture = TilemapTextures.AccessComponent(i);
		if (!texture.RenderedTo)
		{
			continue;
		}
		auto result = render._device->waitForFences(**texture.Fence, true, std::numeric_limits<uint64_t>::max());
		if (result != vk::Result::eSuccess)
		{
			continue;
		}

		if (!Inited)
		{
			vk::CommandBufferBeginInfo beginInfo{};
			buffer.pushConstants<CameraComponent>(
				**pPipeline->PipelineLayout, vk::ShaderStageFlagBits::eVertex, 0, *pCamera);
			buffer.bindPipeline(vk::PipelineBindPoint::eGraphics, **pPipeline->Pipeline);
			Inited = true;
			pPipeline->Used = true;
		}

		updateDescriptorSets(texture, render, *pPipeline, texture._rtImageView.get(), texture._rtSampler.get());

		buffer.bindVertexBuffers(0, **texture.OutputVertexBuffer, { 0 });

		buffer.bindDescriptorSets(vk::PipelineBindPoint::eGraphics, **pPipeline->PipelineLayout, 0,
			*texture._DescriptorSets.at(render._currentImageIndex), nullptr);

		buffer.draw(6, 1, 0, 0);
	}
}

ArchetypeReadWriteMaskPair TileMapRenderSystem::RegisterComponents(ComponentStorageRegistry& registry)
{
	ArchetypeReadWriteMaskPair Pair =
		ArchetypeReadWriteMaskPairFromFunction(registry, std::function(UpdateComponentBuckets));
	Pair.ComponentWriteMask |=
		ArchetypeMaskFromComponents<TilemapRenderPipelineComponent, CameraComponent, RendererComponent>();
	registry.RegisterComponent<TilemapRenderPipelineComponent>();
	return Pair;
}

void TileMapRenderSystem::Update(World& world)
{
	world.AccessEntityManager().ForeachSerial(world, std::function(UpdateComponentBuckets));
}

void updateDescriptorSets(TileMapTextureComponent& tilemapTexture, RendererComponent& render,
	TilemapRenderPipelineComponent&  /*pipeline*/, vk::raii::ImageView* pTileMapImageView,
	vk::raii::Sampler* pTileMapSampler)
{
	std::vector<vk::WriteDescriptorSet> writeDescriptorSet;

	{
		vk::DescriptorImageInfo imageInfo;
		imageInfo.imageView = pTileMapImageView ? (**pTileMapImageView) : nullptr;
		imageInfo.sampler = pTileMapSampler ? (**pTileMapSampler) : nullptr;
		imageInfo.imageLayout = vk::ImageLayout::eShaderReadOnlyOptimal;

		vk::WriteDescriptorSet descriptorWrite;
		descriptorWrite.dstSet = *tilemapTexture._DescriptorSets.at(render._currentImageIndex);
		descriptorWrite.dstBinding = 0;
		descriptorWrite.dstArrayElement = 0;
		descriptorWrite.descriptorType = vk::DescriptorType::eCombinedImageSampler;
		descriptorWrite.descriptorCount = 1;
		descriptorWrite.pImageInfo = pTileMapImageView ? &imageInfo : nullptr;
		descriptorWrite.pTexelBufferView = nullptr;

		writeDescriptorSet.emplace_back(descriptorWrite);
	}

	render._device->updateDescriptorSets(writeDescriptorSet, nullptr);
}
