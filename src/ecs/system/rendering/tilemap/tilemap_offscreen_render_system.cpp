#include "tilemap_offscreen_render_system.h"
#include "render/sprite.h"

#include <vulkan/vulkan_raii.hpp>

#include <ecs/archetype_bucket.h>
#include <ecs/component/position_component.h>
#include <ecs/component/rendering/renderer_component.h>
#include <ecs/component/rendering/tile_map_texture_component.h>
#include <ecs/component/rendering/tile_map_offscreen_render_pipeline_component.h>
#include <ecs/component/tile_map_component.h>
#include <ecs/world.h>
#include <render/utilities.h>
#include <render/vertex.h>

namespace
{
	constexpr vk::Format RT_FORMAT = vk::Format::eB8G8R8A8Unorm;
	constexpr glm::vec3 TILE_IMPASSABLE_COLOR = { 0.2f, 0.2f, 0.2f };
	constexpr glm::vec3 TILE_FOOD_COLOR = { 0, 1, 0 };

	struct MapData
	{
		int32_t MAP_WIDTH_PIXELS = MAP_SIZE_WIDTH_PIXELS;
		int32_t MAP_HEIGHT_PIXELS = MAP_SIZE_HEIGHT_PIXELS;
	};
}

TileMapOffscreenRenderSystem::TileMapOffscreenRenderSystem() : System(TileMapOffscreenRenderSystem::NAME)
{
}

void setupPipeline(RendererComponent& render, TilemapOffscreenRenderPipelineComponent& tmoRender)
{
	// Renderpass
	{
		vk::AttachmentDescription ad;
		ad.format = RT_FORMAT;
		ad.samples = vk::SampleCountFlagBits::e1;
		ad.loadOp = vk::AttachmentLoadOp::eClear;
		ad.storeOp = vk::AttachmentStoreOp::eStore;
		ad.stencilLoadOp = vk::AttachmentLoadOp::eDontCare;
		ad.stencilStoreOp = vk::AttachmentStoreOp::eDontCare;
		ad.initialLayout = vk::ImageLayout::eUndefined;
		ad.finalLayout = vk::ImageLayout::eTransferSrcOptimal;

		vk::AttachmentReference colorAttachmentRef{};
		colorAttachmentRef.attachment = 0;
		colorAttachmentRef.layout = vk::ImageLayout::eColorAttachmentOptimal;

		vk::SubpassDescription sd;
		sd.pipelineBindPoint = vk::PipelineBindPoint::eGraphics;
		sd.colorAttachmentCount = 1;
		sd.pColorAttachments = &colorAttachmentRef;

		vk::SubpassDependency dependency;
		dependency.srcSubpass = VK_SUBPASS_EXTERNAL;
		dependency.dstSubpass = 0;
		dependency.srcStageMask = vk::PipelineStageFlagBits::eFragmentShader;
		dependency.dstStageMask = vk::PipelineStageFlagBits::eColorAttachmentOutput;
		dependency.srcAccessMask = vk::AccessFlagBits::eShaderRead;
		dependency.dstAccessMask = vk::AccessFlagBits::eColorAttachmentWrite;
		dependency.dependencyFlags = vk::DependencyFlagBits::eByRegion;

		vk::RenderPassCreateInfo createInfo;
		createInfo.attachmentCount = 1;
		createInfo.pAttachments = &ad;
		createInfo.subpassCount = 1;
		createInfo.pSubpasses = &sd;
		createInfo.dependencyCount = 1;
		createInfo.pDependencies = &dependency;

		tmoRender.RenderPass = std::make_unique<vk::raii::RenderPass>(*render._device, createInfo);
	}
	{
		vk::DescriptorSetLayoutBinding mapDataLayoutBinding;
		mapDataLayoutBinding.binding = 0;
		mapDataLayoutBinding.descriptorType = vk::DescriptorType::eUniformBuffer;
		mapDataLayoutBinding.descriptorCount = 1;
		mapDataLayoutBinding.stageFlags = vk::ShaderStageFlagBits::eVertex;

		vk::DescriptorSetLayoutCreateInfo create_info;
		create_info.bindingCount = 1;
		create_info.pBindings = &mapDataLayoutBinding;

		tmoRender.MapDataDescriptorSetLayout =
			std::make_unique<vk::raii::DescriptorSetLayout>(*render._device, create_info);
	}
	// Pipeline
	{
		auto vertShaderCode = renderer::utilities::readFile("shader/tilemap_offscreen.vert.spv");
		auto fragShaderCode = renderer::utilities::readFile("shader/tilemap_offscreen.frag.spv");

		std::unique_ptr<vk::raii::ShaderModule> vertShaderModule =
			renderer::utilities::createShaderModule(*render._device, vertShaderCode);
		std::unique_ptr<vk::raii::ShaderModule> fragShaderModule =
			renderer::utilities::createShaderModule(*render._device, fragShaderCode);

		vk::PipelineShaderStageCreateInfo vertShaderStageInfo{};
		vertShaderStageInfo.stage = vk::ShaderStageFlagBits::eVertex;
		vertShaderStageInfo.module = **vertShaderModule;
		vertShaderStageInfo.pName = "main";

		vk::PipelineShaderStageCreateInfo fragShaderStageInfo{};
		fragShaderStageInfo.stage = vk::ShaderStageFlagBits::eFragment;
		fragShaderStageInfo.module = **fragShaderModule;
		fragShaderStageInfo.pName = "main";

		vk::PipelineShaderStageCreateInfo shaderStages[] = { vertShaderStageInfo, fragShaderStageInfo };

		auto bindingDescription = Vertex::getBindingDescription();
		auto attributeDescriptions = Vertex::getAttributeDescriptions();

		vk::PipelineVertexInputStateCreateInfo vertexInputInfo{};
		vertexInputInfo.vertexBindingDescriptionCount = 1;
		vertexInputInfo.vertexAttributeDescriptionCount = static_cast<uint32_t>(attributeDescriptions.size());
		vertexInputInfo.pVertexBindingDescriptions = &bindingDescription;
		vertexInputInfo.pVertexAttributeDescriptions = attributeDescriptions.data();

		vk::PipelineInputAssemblyStateCreateInfo inputAssembly{};
		inputAssembly.topology = vk::PrimitiveTopology::eTriangleList;
		inputAssembly.primitiveRestartEnable = false;

		vk::Viewport viewport{};
		viewport.x = 0.0f;
		viewport.y = 0.0f;
		viewport.width = (float)MAP_SIZE_WIDTH_PIXELS;
		viewport.height = (float)MAP_SIZE_HEIGHT_PIXELS;
		viewport.minDepth = 0.0f;
		viewport.maxDepth = 1.0f;

		vk::Rect2D scissor{};
		scissor.offset = vk::Offset2D{ 0, 0 };
		scissor.extent = vk::Extent2D{ MAP_SIZE_WIDTH_PIXELS, MAP_SIZE_HEIGHT_PIXELS };

		vk::PipelineViewportStateCreateInfo viewportState{};
		viewportState.viewportCount = 1;
		viewportState.pViewports = &viewport;
		viewportState.scissorCount = 1;
		viewportState.pScissors = &scissor;

		vk::PipelineRasterizationStateCreateInfo rasterizer{};
		rasterizer.depthClampEnable = false;
		rasterizer.rasterizerDiscardEnable = false;
		rasterizer.polygonMode = vk::PolygonMode::eFill;
		rasterizer.lineWidth = 1.0f;
		rasterizer.cullMode = vk::CullModeFlagBits::eBack;
		rasterizer.frontFace = vk::FrontFace::eClockwise;
		rasterizer.depthBiasEnable = false;
		rasterizer.depthBiasConstantFactor = 0.0f; // Optional
		rasterizer.depthBiasClamp = 0.0f;		   // Optional
		rasterizer.depthBiasSlopeFactor = 0.0f;	   // Optional

		vk::PipelineMultisampleStateCreateInfo multisampling{};
		multisampling.sampleShadingEnable = false;
		multisampling.rasterizationSamples = vk::SampleCountFlagBits::e1;
		multisampling.minSampleShading = 1.0f;		 // Optional
		multisampling.alphaToCoverageEnable = false; // Optional
		multisampling.alphaToOneEnable = false;		 // Optional

		vk::PipelineColorBlendAttachmentState colorBlendAttachment{};
		colorBlendAttachment.colorWriteMask = vk::ColorComponentFlagBits::eR | vk::ColorComponentFlagBits::eG |
											  vk::ColorComponentFlagBits::eB | vk::ColorComponentFlagBits::eA;
		colorBlendAttachment.blendEnable = true;
		colorBlendAttachment.srcColorBlendFactor = vk::BlendFactor::eSrcAlpha;
		colorBlendAttachment.dstColorBlendFactor = vk::BlendFactor::eOneMinusSrcAlpha;
		colorBlendAttachment.colorBlendOp = vk::BlendOp::eAdd;
		colorBlendAttachment.srcAlphaBlendFactor = vk::BlendFactor::eOne;
		colorBlendAttachment.dstAlphaBlendFactor = vk::BlendFactor::eZero;
		colorBlendAttachment.alphaBlendOp = vk::BlendOp::eAdd;

		vk::PipelineColorBlendStateCreateInfo colorBlending{};
		colorBlending.logicOpEnable = false;
		colorBlending.logicOp = vk::LogicOp::eCopy; // Optional
		colorBlending.attachmentCount = 1;
		colorBlending.pAttachments = &colorBlendAttachment;
		colorBlending.blendConstants[0] = 0.0f; // Optional
		colorBlending.blendConstants[1] = 0.0f; // Optional
		colorBlending.blendConstants[2] = 0.0f; // Optional
		colorBlending.blendConstants[3] = 0.0f; // Optional

		vk::DynamicState dynamicStates[] = { vk::DynamicState::eViewport, vk::DynamicState::eLineWidth };

		vk::PipelineDynamicStateCreateInfo dynamicState{};
		dynamicState.dynamicStateCount = 2;
		dynamicState.pDynamicStates = dynamicStates;

		vk::PipelineLayoutCreateInfo pipelineLayoutInfo{};
		pipelineLayoutInfo.setLayoutCount = 1;
		pipelineLayoutInfo.pSetLayouts = &**tmoRender.MapDataDescriptorSetLayout; // Optional
		pipelineLayoutInfo.pushConstantRangeCount = 0;							  // Optional

		tmoRender.PipelineLayout = std::make_unique<vk::raii::PipelineLayout>(*render._device, pipelineLayoutInfo);

		vk::GraphicsPipelineCreateInfo pipelineInfo{};
		pipelineInfo.stageCount = 2;
		pipelineInfo.pStages = shaderStages;

		pipelineInfo.pVertexInputState = &vertexInputInfo;
		pipelineInfo.pInputAssemblyState = &inputAssembly;
		pipelineInfo.pViewportState = &viewportState;
		pipelineInfo.pRasterizationState = &rasterizer;
		pipelineInfo.pMultisampleState = &multisampling;
		pipelineInfo.pColorBlendState = &colorBlending;
		pipelineInfo.layout = **tmoRender.PipelineLayout;
		pipelineInfo.renderPass = **tmoRender.RenderPass;
		pipelineInfo.subpass = 0;
		pipelineInfo.basePipelineIndex = -1; // Optional

		tmoRender.Pipeline = std::make_unique<vk::raii::Pipeline>(*render._device, nullptr, pipelineInfo);
	}
	// MapData memory
	{
		vk::BufferCreateInfo bufferInfo{};
		bufferInfo.size = sizeof(MapData);
		bufferInfo.usage = vk::BufferUsageFlagBits::eUniformBuffer;
		bufferInfo.sharingMode = vk::SharingMode::eExclusive;

		tmoRender.MapDataBuffer = std::make_unique<vk::raii::Buffer>(vk::raii::Buffer(*render._device, bufferInfo));

		renderer::utilities::SetDebugName(*render._device, *tmoRender.MapDataBuffer, "MapDataBuffer");

		vk::MemoryRequirements memRequirements = tmoRender.MapDataBuffer->getMemoryRequirements();

		vk::MemoryAllocateInfo allocInfo{};
		allocInfo.allocationSize = memRequirements.size;
		allocInfo.memoryTypeIndex =
			renderer::utilities::findMemoryType(*render._physicalDevice, memRequirements.memoryTypeBits,
				vk::MemoryPropertyFlagBits::eHostVisible | vk::MemoryPropertyFlagBits::eHostCoherent);

		tmoRender.MapDataBufferMemory = std::make_unique<vk::raii::DeviceMemory>(*render._device, allocInfo);

		MapData mapData{};

		void* data = tmoRender.MapDataBufferMemory->mapMemory(0, sizeof(MapData));
		memcpy(data, &mapData, sizeof(MapData));
		tmoRender.MapDataBufferMemory->unmapMemory();

		tmoRender.MapDataBuffer->bindMemory(**tmoRender.MapDataBufferMemory, 0);
	}
	// Descriptor sets
	{
		vk::DescriptorSetAllocateInfo allocInfo{};
		allocInfo.descriptorPool = **render._descriptorPool;
		allocInfo.descriptorSetCount = 1;
		allocInfo.pSetLayouts = &**tmoRender.MapDataDescriptorSetLayout;

		tmoRender.DescriptorSet = std::make_unique<vk::raii::DescriptorSet>(
			std::move(vk::raii::DescriptorSets(*render._device, allocInfo).front()));

		vk::DescriptorBufferInfo bufferInfo;
		bufferInfo.buffer = **tmoRender.MapDataBuffer;
		bufferInfo.offset = 0;
		bufferInfo.range = sizeof(MapData);

		vk::WriteDescriptorSet descriptorWrite;
		descriptorWrite.dstSet = **tmoRender.DescriptorSet;
		descriptorWrite.dstBinding = 0;
		descriptorWrite.dstArrayElement = 0;
		descriptorWrite.descriptorType = vk::DescriptorType::eUniformBuffer;
		descriptorWrite.descriptorCount = 1;
		descriptorWrite.pBufferInfo = &bufferInfo;
		descriptorWrite.pImageInfo = nullptr;
		descriptorWrite.pTexelBufferView = nullptr;

		render._device->updateDescriptorSets(descriptorWrite, nullptr);
	}
	{
		vk::CommandBufferAllocateInfo allocInfo{};
		allocInfo.commandPool = **render._commandPool;
		allocInfo.level = vk::CommandBufferLevel::ePrimary;
		allocInfo.commandBufferCount = 1;

		tmoRender.CommandBuffer = std::make_unique<vk::raii::CommandBuffer>(
			std::move(vk::raii::CommandBuffers(*render._device, allocInfo).front()));
	}
}

void buildRenderTargetBuffer(RendererComponent& render, TilemapOffscreenRenderPipelineComponent& tmoRender,
	const std::vector<Sprite>& sprites, const TileMapTextureComponent& texture)
{
	auto& buffer = *tmoRender.CommandBuffer;

	std::vector<Vertex> vertices;
	for (const auto& sprite : sprites)
	{
		auto x = sprite.pos.x;
		auto y = sprite.pos.y;
		vertices.emplace_back(Vertex{ glm::vec2(x, y), sprite.color });
		vertices.emplace_back(Vertex{ glm::vec2(x + sprite.size, y), sprite.color });
		vertices.emplace_back(Vertex{ glm::vec2(x, y + sprite.size), sprite.color });
		vertices.emplace_back(Vertex{ glm::vec2(x, y + sprite.size), sprite.color });
		vertices.emplace_back(Vertex{ glm::vec2(x + sprite.size, y), sprite.color });
		vertices.emplace_back(Vertex{ glm::vec2(x + sprite.size, y + sprite.size), sprite.color });
	}

	const vk::DeviceSize bufferSize = sizeof(Vertex) * vertices.size();
	memcpy(texture._vertexBufferCPUMap, vertices.data(), (size_t)bufferSize);

	vk::CommandBufferBeginInfo beginInfo{};

	buffer.begin(beginInfo);

	vk::RenderPassBeginInfo renderPassInfo{};
	renderPassInfo.renderPass = **tmoRender.RenderPass;
	renderPassInfo.framebuffer = **texture._rtFrameBuffer;

	renderPassInfo.renderArea.offset = vk::Offset2D{ 0, 0 };
	renderPassInfo.renderArea.extent = vk::Extent2D{ MAP_SIZE_WIDTH_PIXELS, MAP_SIZE_HEIGHT_PIXELS };

	vk::ClearValue clearColor = vk::ClearColorValue{ std::array<float, 4>{ 0.0f, 0.0f, 0.0f, 0.0f } };
	renderPassInfo.clearValueCount = 1;
	renderPassInfo.pClearValues = &clearColor;

	buffer.beginRenderPass(renderPassInfo, vk::SubpassContents::eInline);

	buffer.bindPipeline(vk::PipelineBindPoint::eGraphics, **tmoRender.Pipeline);

	buffer.bindVertexBuffers(0, **texture._vertexBuffer, { 0 });

	buffer.bindDescriptorSets(
		vk::PipelineBindPoint::eGraphics, **tmoRender.PipelineLayout, 0, **tmoRender.DescriptorSet, nullptr);

	buffer.draw(sprites.size() * 6, 1, 0, 0);

	buffer.endRenderPass();
	buffer.end();

	vk::PipelineStageFlags waitStages[] = { vk::PipelineStageFlagBits::eAllCommands };
	vk::SubmitInfo submitInfo{};
	submitInfo.pWaitDstStageMask = waitStages;
	submitInfo.commandBufferCount = 1;
	submitInfo.pCommandBuffers = &**tmoRender.CommandBuffer;
	render._device->resetFences(**texture.Fence);
	tmoRender.InFlightFence = texture.Fence.get();
	render._graphicsQueue->submit(submitInfo, **texture.Fence);
}

void TileMapOffscreenRenderSystem::Initialize(World& world)
{
	auto* pRendererComponent = world.AccessEntityManager().AccessFirstComponent<RendererComponent>();
	if (!pRendererComponent)
		return;

	ArchetypeMask PipeLineMask = ArchetypeMaskFromComponents<TilemapOffscreenRenderPipelineComponent,
		TileMapTextureComponent, PositionComponent>();

	Entity pipelineEntity = world.AccessEntityManager().CreateEntity(PipeLineMask);

	auto& tmoRender =
		world.AccessEntityManager().AccessComponent<TilemapOffscreenRenderPipelineComponent>(pipelineEntity);

	setupPipeline(*pRendererComponent, tmoRender);
}

void UpdateComponents(ComponentUpdateParameters& Params, const ComponentBucket<PositionComponent>& Positions,
	ComponentBucket<TileMapComponent>& TileMaps, ComponentBucket<TileMapTextureComponent>& TileMapTextures)
{
	World& world = Params.World;
	auto* pRendererComponent = world.AccessEntityManager().AccessFirstComponent<RendererComponent>();
	if (!pRendererComponent)
		return;

	Entity tmoRenderEntity =
		world.AccessEntityManager().GetFirstEntityWithComponent<TilemapOffscreenRenderPipelineComponent>();
	if (!tmoRenderEntity.is_valid())
	{
		return;
	}

	auto& TMORender =
		world.AccessEntityManager().AccessComponent<TilemapOffscreenRenderPipelineComponent>(tmoRenderEntity);
	auto& TMORenderTexture = world.AccessEntityManager().AccessComponent<TileMapTextureComponent>(tmoRenderEntity);

	for (size_t i = Params.Start; i < Params.End; ++i)
	{
		if (TMORender.InFlightFence)
		{
			auto result = pRendererComponent->_device->waitForFences(**TMORender.InFlightFence, true, 0);
			if (result != vk::Result::eSuccess)
			{
				return;
			}
		}

		TileMapTextureComponent& TileMapTexture = TileMapTextures.AccessComponent(i);
		TileMapComponent& TileMap = TileMaps.AccessComponent(i);

		if (!TileMap._Dirty)
			continue;

		if (TMORender.CurrentlyRenderTo != Entity::invalid())
		{
			vk::ImageCopy imageCopyRegion{};
			imageCopyRegion.srcSubresource.aspectMask = vk::ImageAspectFlagBits::eColor;
			imageCopyRegion.srcSubresource.layerCount = 1;
			imageCopyRegion.dstSubresource.aspectMask = vk::ImageAspectFlagBits::eColor;
			imageCopyRegion.dstSubresource.layerCount = 1;
			imageCopyRegion.extent.width = MAP_SIZE_WIDTH_PIXELS;
			imageCopyRegion.extent.height = MAP_SIZE_HEIGHT_PIXELS;
			imageCopyRegion.extent.depth = 1;

			auto& Buffer = TMORender.CommandBuffer;
			vk::CommandBufferBeginInfo BeginInfo;
			vk::ImageMemoryBarrier imageMemoryBarrier;
			imageMemoryBarrier.subresourceRange.aspectMask = vk::ImageAspectFlagBits::eColor;
			imageMemoryBarrier.subresourceRange.baseMipLevel = 0;
			imageMemoryBarrier.subresourceRange.levelCount = 1;
			imageMemoryBarrier.subresourceRange.baseArrayLayer = 0;
			imageMemoryBarrier.subresourceRange.layerCount = 1;
			imageMemoryBarrier.srcQueueFamilyIndex = VK_QUEUE_FAMILY_IGNORED;
			imageMemoryBarrier.dstQueueFamilyIndex = VK_QUEUE_FAMILY_IGNORED;

			Buffer->begin(BeginInfo);
			{
				imageMemoryBarrier.oldLayout = vk::ImageLayout::eUndefined;
				imageMemoryBarrier.newLayout = vk::ImageLayout::eTransferDstOptimal;
				imageMemoryBarrier.image = **TileMapTexture._rtImage;
				//					imageMemoryBarrier.srcAccessMask = vk::AccessFlagBits::eShaderRead;
				//					imageMemoryBarrier.dstAccessMask = vk::AccessFlagBits::eTransferWrite;

				Buffer->pipelineBarrier(vk::PipelineStageFlagBits::eTopOfPipe, vk::PipelineStageFlagBits::eTransfer,
					vk::DependencyFlagBits::eDeviceGroup, nullptr, nullptr, imageMemoryBarrier);
			}
			Buffer->copyImage(**TMORenderTexture._rtImage, vk::ImageLayout::eTransferSrcOptimal,
				**TileMapTexture._rtImage, vk::ImageLayout::eTransferDstOptimal, imageCopyRegion);
			{
				imageMemoryBarrier.oldLayout = vk::ImageLayout::eTransferDstOptimal;
				imageMemoryBarrier.newLayout = vk::ImageLayout::eShaderReadOnlyOptimal;
				imageMemoryBarrier.image = **TileMapTexture._rtImage;
				//					imageMemoryBarrier.srcAccessMask = vk::AccessFlagBits::eTransferWrite;
				//					imageMemoryBarrier.dstAccessMask = vk::AccessFlagBits::eTransferRead;

				Buffer->pipelineBarrier(vk::PipelineStageFlagBits::eTransfer, vk::PipelineStageFlagBits::eBottomOfPipe,
					vk::DependencyFlagBits::eDeviceGroup, nullptr, nullptr, imageMemoryBarrier);
			}
			Buffer->end();

			{
				vk::PipelineStageFlags waitStages[] = { vk::PipelineStageFlagBits::eAllCommands };
				vk::SubmitInfo submitInfo{};
				submitInfo.pWaitDstStageMask = waitStages;
				submitInfo.commandBufferCount = 1;
				submitInfo.pCommandBuffers = &**TMORender.CommandBuffer;
				pRendererComponent->_device->resetFences(**TileMapTexture.Fence);
				pRendererComponent->_graphicsQueue->submit(submitInfo, **TileMapTexture.Fence);
			}

			TMORender.CurrentlyRenderTo = Entity::invalid();
			TileMapTexture.RenderedTo = true;
			TileMap._Dirty = false;

			return;
		}

		const PositionComponent& pos = Positions.GetComponent(i);

		std::vector<Sprite> walls;
		for (int x = 0; x < TileMap.map.size(); ++x)
		{
			for (int y = 0; y < TileMap.map[0].size(); ++y)
			{
				auto tt = TileMap.map[x][y];

				if (tt == TileType::Impassable)
				{
					walls.push_back(
						Sprite{ glm::vec2(x * TILE_SIZE, y * TILE_SIZE), TILE_SIZE, TILE_IMPASSABLE_COLOR });
				}
				else if (tt == TileType::Food)
				{
					walls.push_back(Sprite{ glm::vec2(x * TILE_SIZE, y * TILE_SIZE), TILE_SIZE, TILE_FOOD_COLOR });
				}
			}
		}
		TMORender.CurrentlyRenderTo = Params.Bucket.LookupEntityForIndex(i);
		buildRenderTargetBuffer(*pRendererComponent, TMORender, walls, TMORenderTexture);
	}
}

ArchetypeReadWriteMaskPair TileMapOffscreenRenderSystem::RegisterComponents(ComponentStorageRegistry& registry)
{
	ArchetypeReadWriteMaskPair Pair = ArchetypeReadWriteMaskPairFromFunction(registry, std::function(UpdateComponents));
	Pair.ComponentWriteMask |= ArchetypeMaskFromComponents<RendererComponent, TilemapOffscreenRenderPipelineComponent,
		TileMapTextureComponent>();
	registry.RegisterComponent<TileMapTextureComponent>();
	registry.RegisterComponent<TilemapOffscreenRenderPipelineComponent>();
	return Pair;
}

void TileMapOffscreenRenderSystem::Update(World& world)
{
	world.AccessEntityManager().ForeachSerial(world, std::function(UpdateComponents));
}
