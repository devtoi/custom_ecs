#include "system.h"
#include <string/string_database.h>

System::System(std::string_view Name) : m_Name(Name)
{
	m_NameID = StringDatabase::AddString(Name);
}

void System::SetupComponentDependencies(ComponentStorageRegistry& registry)
{
	ComponentMask = RegisterComponents(registry);
}

ArchetypeReadWriteMaskPair System::RegisterComponents(ComponentStorageRegistry& /*registry*/)
{
	ArchetypeReadWriteMaskPair Pair;
	Pair.ComponentReadMask.set();
	Pair.ComponentWriteMask.set();
	return Pair;
}

void System::Update(World& /*world*/)
{
	// Systems without an update has no dependencies once initialization is done
	ComponentMask.ComponentWriteMask.reset();
	ComponentMask.ComponentReadMask.reset();
}

const std::string& System::GetName() const
{
	return m_Name;
}

[[nodiscard]] ArchetypeMask System::GetComponentReadMask() const
{
	return ComponentMask.ComponentReadMask;
}

[[nodiscard]] ArchetypeMask System::GetComponentWriteMask() const
{
	return ComponentMask.ComponentWriteMask;
}

StringID System::GetNameID() const
{
	return m_NameID;
}
