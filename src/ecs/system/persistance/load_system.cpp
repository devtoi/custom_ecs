#include "load_system.h"

#include <fstream>

#include <ecs/world.h>
#include "ecs/component/persistance/load_component.h"
#include "serializer/json/json_text_reader.h"

LoadSystem::LoadSystem() : System(LoadSystem::NAME)
{
}

void UpdateComponents(ComponentUpdateParameters& Parameters, ComponentBucket<LoadComponent>& LoadComponents)
{
	for (auto i = Parameters.Start; i < Parameters.End; ++i)
	{
		auto& LoadComponent = LoadComponents.AccessComponent(i);
		if (LoadComponent.RunLoad)
		{
			std::ifstream FileRead("derp.save");
			auto Json = nlohmann::json::parse(FileRead);
			JsonTextReader reader(Json);
			reader.serialize("world", Parameters.World);
			LoadComponent.RunLoad = false;
		}
	}
}

ArchetypeReadWriteMaskPair LoadSystem::RegisterComponents(ComponentStorageRegistry& Registry)
{
	ArchetypeReadWriteMaskPair Pair = ArchetypeReadWriteMaskPairFromFunction(Registry, std::function(UpdateComponents));
	return Pair;
}

void LoadSystem::Initialize(World& World)
{
	auto Mask = ArchetypeMaskFromComponents<LoadComponent>();
	World.AccessEntityManager().CreateEntity(Mask);
}

void LoadSystem::Update(World& World)
{
	World.AccessEntityManager().ForeachSerial(World, std::function(UpdateComponents));
}

void LoadSystem::Uninitialize(World& World)
{
}
