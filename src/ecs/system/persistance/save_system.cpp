#include "save_system.h"

#include <fstream>

#include <ecs/world.h>
#include <ecs/component/persistance/save_component.h>
#include <serializer/json/json_text_writer.h>
#include <serializer/json/json_text_reader.h>

nlohmann::json g_TempJsonTest;

SaveSystem::SaveSystem() : System(SaveSystem::NAME)
{
}

void SaveSystem::Initialize(World& World)
{
	auto Mask = ArchetypeMaskFromComponents<SaveComponent>();
	World.AccessEntityManager().CreateEntity(Mask);
}

void UpdateComponents(ComponentUpdateParameters& Parameters, ComponentBucket<SaveComponent>& SaveComponents)
{
	for (int i = Parameters.Start; i < Parameters.End; ++i)
	{
		auto& Save = SaveComponents.AccessComponent(i);
		if (Save.Save)
		{
			Save.Save = false;

			JsonTextWriter writer;
			writer.serialize("world", Parameters.World);
			std::ofstream file("derp.save");
			file << writer.GetJson().dump(4);
			file.close();
		}
	}
}

ArchetypeReadWriteMaskPair SaveSystem::RegisterComponents(ComponentStorageRegistry& Registry)
{
	ArchetypeReadWriteMaskPair Pair = ArchetypeReadWriteMaskPairFromFunction(Registry, std::function(UpdateComponents));
	return Pair;
}

void SaveSystem::Update(World& World)
{
	World.AccessEntityManager().ForeachSerial(World, std::function(UpdateComponents));
}
