#pragma once

#include <ecs/system/system.h>

class SaveSystem final : public System
{
public:
	static constexpr std::string_view NAME = "SaveSystem";
	SaveSystem();

	ArchetypeReadWriteMaskPair RegisterComponents(ComponentStorageRegistry& registry) override;
	void Initialize(World& world) override;
	void Update(World& world) override;
};
