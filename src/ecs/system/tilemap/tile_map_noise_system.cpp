#include "tile_map_noise_system.h"

#include "ecs/component/position_component.h"
#include "ecs/component/tile_map_component.h"
#include "glm/geometric.hpp"

#include <ecs/world.h>

namespace
{
	constexpr size_t NUMBER_OF_PERLIN_POINTS_WIDTH = 38;
	constexpr size_t NUMBER_OF_PERLIN_POINTS_HEIGHT = 20;
	constexpr float PERLIN_CELL_WIDTH = (float)MAP_SIZE_WIDTH_TILES / NUMBER_OF_PERLIN_POINTS_WIDTH;
	constexpr float PERLIN_CELL_HEIGHT = (float)MAP_SIZE_HEIGHT_TILES / NUMBER_OF_PERLIN_POINTS_HEIGHT;

}

TileMapNoiseSystem::TileMapNoiseSystem() : System(TileMapNoiseSystem::NAME)
{
}


void GenerateTilemapsWithNoise(ComponentUpdateParameters& Params, ComponentBucket<TileMapComponent>& TileMaps,
	const ComponentBucket<PositionComponent>& Positions)
{
	std::uniform_int_distribution<uint16_t> randomTileType(0, 10);

	for (size_t i = Params.Start; i < Params.End; ++i)
	{
		const PositionComponent& Pos = Positions.GetComponent(i);
		TileMapComponent& TM = TileMaps.AccessComponent(i);

		std::mt19937 Generator(Pos.position.x * 1000 + Pos.position.y);

		std::uniform_real_distribution<float> RandomFloat(-1, 1);
		std::vector<std::vector<glm::vec2>> Perlin;
		for (size_t x = 0; x < NUMBER_OF_PERLIN_POINTS_WIDTH + 1; ++x)
		{
			Perlin.emplace_back();
			for (size_t y = 0; y < NUMBER_OF_PERLIN_POINTS_HEIGHT + 1; ++y)
			{
				Perlin[x].emplace_back(glm::normalize(glm::vec2(RandomFloat(Generator), RandomFloat(Generator))));
			}
		}

		for (size_t TileX = 0; TileX < MAP_SIZE_WIDTH_TILES; ++TileX)
		{
			for (size_t TileY = 0; TileY < MAP_SIZE_HEIGHT_TILES; ++TileY)
			{
				float x = (float)TileX / PERLIN_CELL_WIDTH;
				float y = (float)TileY / PERLIN_CELL_HEIGHT;

				int PerlinLeft = (int)x;
				int PerlinRight = PerlinLeft + 1;
				int PerlinTop = (int)y;
				int PerlinBottom = PerlinTop + 1;

				glm::vec2 Cell = glm::vec2(x, y);

				glm::vec2 PerlinTopLeft = glm::vec2(PerlinLeft, PerlinTop);
				glm::vec2 PerlinTopRight = glm::vec2(PerlinRight, PerlinTop);
				glm::vec2 PerlinBottomLeft = glm::vec2(PerlinLeft, PerlinBottom);
				glm::vec2 PerlinBottomRight = glm::vec2(PerlinRight, PerlinBottom);

				glm::vec2 OffsetTopLeft = glm::normalize(Cell - PerlinTopLeft);
				glm::vec2 OffsetTopRight = glm::normalize(Cell - PerlinTopRight);
				glm::vec2 OffsetBottomLeft = glm::normalize(Cell - PerlinBottomLeft);
				glm::vec2 OffsetBottomRight = glm::normalize(Cell - PerlinBottomRight);

				glm::vec2 TopLeftGradient = Perlin[PerlinLeft][PerlinTop];
				glm::vec2 TopRightGradient = Perlin[PerlinRight][PerlinTop];
				glm::vec2 BottomLeftGradient = Perlin[PerlinLeft][PerlinBottom];
				glm::vec2 BottomRightGradient = Perlin[PerlinRight][PerlinBottom];

				float TopLeftDot = glm::dot(TopLeftGradient, OffsetTopLeft);
				float TopRightDot = glm::dot(TopRightGradient, OffsetTopRight);
				float BottomLeftDot = glm::dot(BottomLeftGradient, OffsetBottomLeft);
				float BottomRightDot = glm::dot(BottomRightGradient, OffsetBottomRight);

				float a = glm::mix(TopLeftDot, TopRightDot, Cell.x - PerlinTopLeft.x);
				float b = glm::mix(BottomLeftDot, BottomRightDot, Cell.x - PerlinTopLeft.x);

				float Final = glm::mix(a, b, Cell.y - PerlinTopLeft.y);

				if (Final > 1.2f)
				{
					TM.map.at(TileX).at(TileY) = TileType::Impassable;
				}
			}
		}
	}
}

ArchetypeReadWriteMaskPair TileMapNoiseSystem::RegisterComponents(ComponentStorageRegistry& Registry)
{
	ArchetypeReadWriteMaskPair Pair =
		ArchetypeReadWriteMaskPairFromFunction(Registry, std::function(GenerateTilemapsWithNoise));
	return Pair;
}

void TileMapNoiseSystem::Initialize(World& World)
{
	World.AccessEntityManager().ForeachSerial(World, std::function(GenerateTilemapsWithNoise));
}
