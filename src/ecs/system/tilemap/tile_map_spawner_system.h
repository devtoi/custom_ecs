#pragma once

#include "ecs/system/system.h"

class TileMapSpawnerSystem final : public System
{
public:
	static constexpr std::string_view NAME = "TileMapSpawnerSystem";
	TileMapSpawnerSystem();

	ArchetypeReadWriteMaskPair RegisterComponents(ComponentStorageRegistry& registry) override;
	void Initialize(World& world) override;
};
