#pragma once

#include <ecs/system/system.h>

class TileMapNoiseSystem final : public System
{
public:
	static constexpr std::string_view NAME = "TileMapNoiseSystem";
	TileMapNoiseSystem();

	ArchetypeReadWriteMaskPair RegisterComponents(ComponentStorageRegistry& registry) override;
	void Initialize(World& world) override;
};
