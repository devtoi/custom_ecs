#include "tile_map_spawner_system.h"
#include "render/vertex.h"
#include "render/utilities.h"

#include <filesystem>
#include <fstream>

#include <spdlog/spdlog.h>

#include <ecs/archetype_bucket.h>
#include <ecs/component/position_component.h>
#include <ecs/component/rendering/renderer_component.h>
#include <ecs/component/rendering/tile_map_texture_component.h>
#include <ecs/component/rendering/tile_map_render_pipeline_component.h>
#include <ecs/component/rendering/tile_map_offscreen_render_pipeline_component.h>
#include <ecs/component/rendering/sprite_render_pipeline_component.h>
#include <ecs/component/game/pheromone_map_component.h>
#include <ecs/component/tile_map_component.h>
#include <ecs/component/velocity_component.h>
#include <ecs/component/camera_component.h>
#include <ecs/world.h>

TileMapSpawnerSystem::TileMapSpawnerSystem() : System(TileMapSpawnerSystem::NAME)
{
}

void RandomizeMap(TileMapComponent& tileMapComponent)
{
	std::mt19937 generator(123);
	std::uniform_int_distribution<uint16_t> randomTileType(0, 10);
	auto& map = tileMapComponent.map;

	for (size_t x = 0; x < MAP_SIZE_WIDTH_TILES; ++x)
	{
		for (size_t y = 0; y < MAP_SIZE_HEIGHT_TILES; ++y)
		{
			map[x][y] = static_cast<TileType>(
				randomTileType(generator) > std::sin(x / 10.0) * 3 + std::sin(y / 10.0) * 3 + 0 ? TileType::Passable
																								: TileType::Impassable);
		}
	}
}

void LoadMap(const std::filesystem::path& path, TileMapComponent& tileMapComponent)
{
	std::ifstream file(path);
	if (file.is_open())
	{
		std::string line;
		// Type
		getline(file, line);
		// Height
		getline(file, line);
		// Width
		getline(file, line);
		// Map
		getline(file, line);
		size_t y = 0;
		while (getline(file, line))
		{
			size_t x = 0;
			for (char c : line)
			{
				tileMapComponent.map[x][y] = (c == '.' ? TileType::Passable : TileType::Impassable);
				x++;
			}
			y++;
		}
		file.close();
	}
	else
	{
		spdlog::error("Failed to load file {0}", path.generic_string().data());
	}
}

ArchetypeReadWriteMaskPair TileMapSpawnerSystem::RegisterComponents(ComponentStorageRegistry& registry)
{
	registry.RegisterComponent<TileMapComponent>();
	registry.RegisterComponent<TileMapTextureComponent>();
	registry.RegisterComponent<PositionComponent>();
	registry.RegisterComponent<PheromonMapComponent>();
	return System::RegisterComponents(registry);
}

void TileMapSpawnerSystem::Initialize(World& world)
{
	ArchetypeMask mask = ArchetypeMaskFromComponents<PositionComponent, TileMapComponent, TileMapTextureComponent,
		PheromonMapComponent>();

	world.AccessEntityManager().CreateEntity(mask);
	//	world.AccessEntityManager().CreateEntity(mask);
	//	world.AccessEntityManager().CreateEntity(mask);
	//	world.AccessEntityManager().CreateEntity(mask);

	world.AccessEntityManager().ForeachSerial(
		world, std::function([](ComponentUpdateParameters& Params, ComponentBucket<TileMapComponent>& TileMaps,
								 ComponentBucket<PositionComponent>& Positions) {
			for (size_t i = Params.Start; i < Params.End; ++i)
			{
				PositionComponent& pos = Positions.AccessComponent(i);
				pos.position.x = i * MAP_SIZE_WIDTH_PIXELS;
				pos.position.y = 0;

				TileMapComponent& tm = TileMaps.AccessComponent(i);

				//				RandomizeMap(tm);
				// LoadMap("../assets/maps/lt_hangedman_n.map", tm);
				/* LoadMap("../assets/maps/w_encounter2_camp.map", tm); */
			}
		}));
}
