#include "simulation_tick_system.h"

#include <fstream>

#include "ecs/component/core/simulation_input_component.h"
#if USE_NAKAMA
#include "ecs/component/network/network_match_component.h"
#include "ecs/component/network/network_player_component.h"
#include "ecs/simulation_input/network/tick_input.h"
#endif
#include "ecs/component/position_component.h"
#include "ecs/component/velocity_component.h"
#include "ecs/utils/simulation_io.h"
#include "ecs/world.h"
#include "serializer/json/json_text_writer.h"
#include "spdlog/spdlog.h"

SimulationTickSystem::SimulationTickSystem() : System(SimulationTickSystem::NAME)
{
}

ArchetypeReadWriteMaskPair SimulationTickSystem::RegisterComponents(ComponentStorageRegistry& Registry)
{
	// ArchetypeReadWriteMaskPairFromFunction(Registry, std::function(BuildTickInput));
#if USE_NAKAMA
	SimulationInputFactory::RegisterCreationFunction(TickInput::ID, []() { return std::make_unique<TickInput>(); });
#endif
	return System::RegisterComponents(Registry);
}

void SimulationTickSystem::Initialize(World& World)
{
}

void SimulationTickSystem::Update(World& World)
{
#if USE_NAKAMA
	auto* pNetworkMatch = World.AccessEntityManager().AccessFirstComponent<NetworkMatchComponent>();
	if (pNetworkMatch && !pNetworkMatch->_Match.matchId.empty())
	{
		int32_t Checksum = 0;
		World.AccessEntityManager().ForeachSerial(World,
			std::function(
				[&Checksum](ComponentUpdateParameters& Parameters, const ComponentBucket<PositionComponent>& Positions,
					const ComponentBucket<VelocityComponent>& Velocities) {
					for (size_t i = Parameters.Start; i < Parameters.End; ++i)
					{
						const PositionComponent& Position = Positions.GetComponent(i);
						const VelocityComponent& Velocity = Velocities.GetComponent(i);
						constexpr uint32_t Totalbits = 32;
						const uint32_t Shift = static_cast<int32_t>(i) % Totalbits;
						const uint32_t MaskedShift = Shift & 31;
						Checksum +=
							(Position.position.x << MaskedShift) | (Position.position.x >> (Totalbits - MaskedShift));
						Checksum +=
							(Position.position.y << MaskedShift) | (Position.position.y >> (Totalbits - MaskedShift));
						Checksum += (Velocity.left << MaskedShift) | (Velocity.left >> (Totalbits - MaskedShift));
						Checksum +=
							(Velocity.direction.x << MaskedShift) | (Velocity.direction.x >> (Totalbits - MaskedShift));
						Checksum +=
							(Velocity.direction.y << MaskedShift) | (Velocity.direction.y >> (Totalbits - MaskedShift));
					}
				}));

		auto pTick = std::make_unique<TickInput>();
		pTick->Frame = World.GetSimulationFrameNumber();
		pTick->Checksum = Checksum;
		// Tick->Checksum = Tick->Frame;

		auto& LocalPlayer =
			World.AccessEntityManager().AccessComponent<NetworkToSimulationInputComponent>(pNetworkMatch->_LocalPlayer);
		LocalPlayer._LastFinishedFrame = pTick->Frame;
		LocalPlayer._FrameHashes.at(pTick->Frame % NETWORK_FRAME_BUFFER_SIZE) = Checksum;

		World.AccessEntityManager().ForeachSerial(World,
			std::function([&World = World, &Checksum = Checksum](ComponentUpdateParameters& Parameters,
							  const ComponentBucket<NetworkToSimulationInputComponent>& NetworkToSimulationInputs) {
				for (auto i = Parameters.Start; i < Parameters.End; ++i)
				{
					const auto& Input = NetworkToSimulationInputs.GetComponent(i);

					auto Frame = World.GetSimulationFrameNumber();
					if (Input._LastFinishedFrame >= Frame && Frame != 0 &&
						Input._LastFinishedFrame != std::numeric_limits<decltype(Input._LastFinishedFrame)>::max())
					{
						auto Expected = Input._FrameHashes.at(Frame % NETWORK_FRAME_BUFFER_SIZE);
						if (Expected != Checksum)
						{
							spdlog::error("Frame {} got checksum {} expected {}", Frame, Checksum, Expected);
							// JsonTextWriter writer;
							// writer.serialize("world", Parameters.World);
							// const auto* pMatch = World.GetEntityManager().GetFirstComponent<NetworkMatchComponent>();
							// const auto& LocalPlayer =
							// 	World.GetEntityManager().GetComponent<NetworkPlayerComponent>(pMatch->_LocalPlayer);
							// std::ofstream file(std::to_string(LocalPlayer.SessionID.GetID()));
							// file << writer.GetJson().dump(4);
							// file.close();
							// assert(false);
						}
					}
				}
			}));

		// LocalPlayer._FrameHashes.at(Tick->Frame % NETWORK_FRAME_BUFFER_SIZE) = Tick->Frame;

		// Record to send
		pNetworkMatch->_OutboundInputs.emplace_back(std::move(pTick));
	}
	else
	{
#endif
		World.IncreaseTargetSimulationFrameNumber();
#if USE_NAKAMA
	}
#endif
}

void SimulationTickSystem::Uninitialize(World& World)
{
}
