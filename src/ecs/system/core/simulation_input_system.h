#pragma once

#include <ecs/system/system.h>

class SimulationInputSystem final : public System
{
public:
	static constexpr std::string_view NAME = "SimulationInputSystem";
	SimulationInputSystem();

	ArchetypeReadWriteMaskPair RegisterComponents(ComponentStorageRegistry& registry) override;
	void Initialize(World& world) override;
	void Update(World& world) override;
	void Uninitialize(World& world) override;
	[[nodiscard]] bool RunWhenPaused() override
	{
		return true;
	}
};
