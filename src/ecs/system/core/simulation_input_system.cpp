#include "simulation_input_system.h"
#include "ecs/component/core/simulation_input_component.h"
#include "ecs/simulation_input/core/set_simulation_pause_state_input.h"
#include "ecs/simulation_input/network/tick_input.h"
#include "ecs/utils/simulation_io.h"

#include <ecs/world.h>

SimulationInputSystem::SimulationInputSystem() : System(SimulationInputSystem::NAME)
{
}

void UpdateComponents(
	ComponentUpdateParameters& Parameters, ComponentBucket<SimulationInputComponent>& SimulationInputs)
{
	for (size_t i = Parameters.Start; i < Parameters.End; ++i)
	{
		auto& Inputs = SimulationInputs.AccessComponent(i)._SimulationInputs;
		if (!Parameters.World.IsSimulating())
		{
			// for (auto it = Inputs.begin(); it != Inputs.end();)
			// {
			// 	if ((*it)->GetID() == SetSimulationPauseStateInput::ID)
			// 	{
			// 		(*it)->Execute(Parameters.World);
			// 		it = Inputs.erase(it);
			// 	}
			// 	else
			// 	{
			// 		++it;
			// 	}
			// }
		}
		else
		{
			while (!Inputs.empty())
			{
				auto& Input = Inputs.front();
				if (Input->IsValid(Parameters.World))
				{
					Input->Execute(Parameters.World);
				}
				Inputs.erase(Inputs.begin());
			}
		}
	}
}

ArchetypeReadWriteMaskPair SimulationInputSystem::RegisterComponents(ComponentStorageRegistry& Registry)
{
	ArchetypeReadWriteMaskPairFromFunction(Registry, std::function(UpdateComponents));
	SimulationInputFactory::RegisterCreationFunction(
		PauseSimulationInput::ID, std::function([]() { return std::make_unique<PauseSimulationInput>(); }));
	SimulationInputFactory::RegisterCreationFunction(
		UnpauseSimulationInput::ID, std::function([]() { return std::make_unique<UnpauseSimulationInput>(); }));
	return System::RegisterComponents(Registry);
}

void SimulationInputSystem::Initialize(World& World)
{
	World.AccessEntityManager().CreateEntity(ArchetypeMaskFromComponents<SimulationInputComponent>());
	// SimulationIO::RecordInput(World, TickInput{});
}

void SimulationInputSystem::Update(World& World)
{
	World.AccessEntityManager().ForeachSerial(World, std::function(UpdateComponents));
}

void SimulationInputSystem::Uninitialize(World& World)
{
}
