#include "menu_system.h"

// AUTO INSERT SYSTEM HERE
#include <ecs/system/core/simulation_tick_system.h>
#if USE_NAKAMA
#include <ecs/system/network/simulation_to_network_system.h>
#include <ecs/system/network/network_to_simulation_system.h>
#endif
#include <ecs/system/core/simulation_input_system.h>
#if USE_IMGUI
#include "ecs/system/imgui/imgui_camera_mover_system.h"
#include "ecs/system/imgui/imgui_home_info_system.h"
#include "ecs/system/imgui/imgui_movement_settings_system.h"
#include "ecs/system/imgui/imgui_save_system.h"
#include "ecs/system/imgui/imgui_sprite_render_settings_system.h"
#include "ecs/system/imgui/imgui_tilemap_editor_system.h"
#include "ecs/system/imgui/imgui_time_control_system.h"
#endif
#if USE_VULKAN
#include "ecs/system/rendering/sprite_render_system.h"
#include "ecs/system/rendering/tilemap/tilemap_offscreen_render_system.h"
#include "ecs/system/rendering/tilemap/tilemap_render_system.h"
#include <ecs/system/rendering/tilemap/tile_map_texture_initializer_system.h>
#endif
#include "ecs/system/movement_system.h"
#include "ecs/system/tilemap/tile_map_spawner_system.h"
#include "ecs/system/tilemap/tile_map_noise_system.h"
#include "ecs/system/game/ant_spawner_system.h"
#include "ecs/system/game/food_mover_system.h"
#include "ecs/system/game/food_spawner_system.h"
#include "ecs/system/game/home_spawner_system.h"
#include "ecs/system/game/pheromone_disappater_system.h"
#include "ecs/system/game/pheromone_spawner_system.h"

#include <ecs/world.h>
#include <ecs/component/core/menu_component.h>
#include <ecs/component/persistance/load_component.h>

MenuSystem::MenuSystem() : System(MenuSystem::NAME)
{
}

ArchetypeReadWriteMaskPair MenuSystem::RegisterComponents(ComponentStorageRegistry& Registry)
{
	Registry.RegisterComponent<MenuComponent>();
	return System::RegisterComponents(Registry);
}

void MenuSystem::Initialize(World& World)
{
	World.AccessEntityManager().CreateEntity(ArchetypeMaskFromComponents<MenuComponent>());
	auto* pMenu = World.AccessEntityManager().AccessFirstComponent<MenuComponent>();
	// pMenu->SwitchToNextMode = true;
	auto& SystemsBank = World.AccessSystemsBank();

#if USE_VULKAN
	{
		auto pCollection = std::make_unique<SystemCollection>();

		pCollection->AddSystem<SpriteRenderSystem>(SystemsBank, -50);
		pCollection->AddSystem<TileMapOffscreenRenderSystem>(SystemsBank, -50);
		pCollection->AddSystem<TileMapRenderSystem>(SystemsBank, -50);
		pCollection->AddSystem<TileMapTextureInitializerSystem>(SystemsBank, -25);

		pMenu->VisualizationSystemsForNextMode.AddOtherCollection(
			SystemsBank.AddSystemCollectionTemplate("AntRender", std::move(pCollection)));
	}
#endif
	{
		auto pCollection = std::make_unique<SystemCollection>();

		pCollection->AddSystem<TileMapSpawnerSystem>(SystemsBank, -200);
		pCollection->AddSystem<TileMapNoiseSystem>(SystemsBank, -120);
		pCollection->AddSystem<HomeSpawnerSystem>(SystemsBank, 200);
		pCollection->AddSystem<FoodSpawnerSystem>(SystemsBank, 200);
		pCollection->AddSystem<AntSpawnerSystem>(SystemsBank, 200);

		SystemsBank.AddSystemCollectionTemplate("AntSpawners", std::move(pCollection));
	}

	{
		auto pCollection = std::make_unique<SystemCollection>();
		// AUTO INSERT SYSTEM CLASS HERE

		pCollection->AddSystem<SimulationInputSystem>(SystemsBank, -950);
		pCollection->AddSystem<PheromoneSpawnerSystem>(SystemsBank);
		pCollection->AddSystem<PheromoneDisappaterSystem>(SystemsBank);
		pCollection->AddSystem<MovementSystem>(SystemsBank);
		pCollection->AddSystem<FoodMoverSystem>(SystemsBank);
		pCollection->AddSystem<SimulationTickSystem>(SystemsBank, 1900);
#if USE_NAKAMA
		pCollection->AddSystem<NetworkToSimulationSystem>(SystemsBank, -953);
		pCollection->AddSystem<SimulationToNetworkSystem>(SystemsBank, 2000);
#endif

		pMenu->SimulationSystemsForNextMode.AddOtherCollection(
			SystemsBank.AddSystemCollectionTemplate("AntCore", std::move(pCollection)));
	}

#if USE_VULKAN
#if USE_IMGUI
	{
		auto pCollection = std::make_unique<SystemCollection>();

		pCollection->AddSystem<ImguiSaveSystem>(SystemsBank, 90);
		pCollection->AddSystem<ImguiCameraMoverSystem>(SystemsBank, 90);
		pCollection->AddSystem<ImguiTileMapEditorSystem>(SystemsBank, 90);
		pCollection->AddSystem<ImguiHomeInfoSystem>(SystemsBank, 90);
		pCollection->AddSystem<ImguiTimeControlSystem>(SystemsBank, 90);
		pCollection->AddSystem<ImguiMovementSettingsSystem>(SystemsBank, 90);
		pCollection->AddSystem<ImguiSpriteRenderSettingsSystem>(SystemsBank, 90);

		pMenu->VisualizationSystemsForNextMode.AddOtherCollection(
			SystemsBank.AddSystemCollectionTemplate("AntImgui", std::move(pCollection)));
	}
#endif
#endif
}

void MenuSystem::Update(World& World)
{
	auto* pMenu = World.AccessEntityManager().AccessFirstComponent<MenuComponent>();
	auto* pLoad = World.AccessEntityManager().AccessFirstComponent<LoadComponent>();
	if (pMenu->SwitchToNextMode)
	{
		SystemCollection SimulationCopy = World.GetSimulationSystemCollection();
		SimulationCopy.AddOtherCollection(World.AccessSystemsBank().AccessSystemCollectionTemplate("AntSpawners"));
		SimulationCopy.AddOtherCollection(&pMenu->SimulationSystemsForNextMode);

		SystemCollection VisualizationCopy = World.GetVisualizationSystemCollection();
		VisualizationCopy.AddOtherCollection(&pMenu->VisualizationSystemsForNextMode);

		World.SetNextSystemCollection(std::move(SimulationCopy), std::move(VisualizationCopy));
		pMenu->SwitchToNextMode = false;
	}
}

void MenuSystem::Uninitialize(World& World)
{
}
