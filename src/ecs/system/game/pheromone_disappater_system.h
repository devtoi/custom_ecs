#pragma once

#include <ecs/system/system.h>

class PheromoneDisappaterSystem : public System
{
public:
	static constexpr std::string_view NAME = "PheromoneDisappaterSystem";
	PheromoneDisappaterSystem();

	void Update(World& world);
};
