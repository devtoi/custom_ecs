#include "pheromone_spawner_system.h"

#include <ecs/world.h>
#include <ecs/component/game/pheromone_map_component.h>
#include <ecs/component/game/food_mover_component.h>
#include <ecs/component/velocity_component.h>
#include <ecs/component/position_component.h>

namespace
{
}

PheromoneSpawnerSystem::PheromoneSpawnerSystem() : System(PheromoneSpawnerSystem::NAME)
{
}

void UpdatePheromones(ComponentUpdateParameters& Parameters, const ComponentBucket<PositionComponent>& Positions,
	const ComponentBucket<FoodMoverComponent>& FoodMovers, const ComponentBucket<FoodMoverComponent>& /*HomeTrackers*/,
	const ComponentBucket<VelocityComponent>& Velocitys)
{
	auto* pPheromoneMap = Parameters.World.AccessEntityManager().AccessFirstComponent<PheromonMapComponent>();
	if (!pPheromoneMap)
	{
		return;
	}
	for (size_t i = Parameters.Start; i < Parameters.End; ++i)
	{
		const auto& Position = Positions.GetComponent(i).position;
		const auto& FoodMover = FoodMovers.GetComponent(i);
		const auto& Velocity = Velocitys.GetComponent(i);
		//		if (Velocity.JustSwitchedTile)
		{
			if (Position.x > 0 && Position.x < pPheromoneMap->FoodPheromoneTiles.size() && Position.y > 0 &&
				Position.y < pPheromoneMap->FoodPheromoneTiles[0].size())
			{
				if (FoodMover.FoodAmount > 0)
				{
					auto& Food = pPheromoneMap->FoodPheromoneTiles.at(Position.x).at(Position.y);
					Food = std::min(MAX_FOOD_PHEROMONE_STRENGTH,
						Food + static_cast<PheromoneStrength>(
								   FOOD_PHEROMONE_STRENGTH * Parameters.World.GetSimulationTimeStep()));
				}
				else
				{
					auto& Home = pPheromoneMap->HomePheromoneTiles[Position.x][Position.y];
					Home = std::min(MAX_HOME_PHEROMONE_STRENGTH,
						Home + static_cast<PheromoneStrength>(
								   HOME_PHEROMONE_STRENGTH * Parameters.World.GetSimulationTimeStep()));
				}
			}
		}
	}
}

ArchetypeReadWriteMaskPair PheromoneSpawnerSystem::RegisterComponents(ComponentStorageRegistry& registry)
{
	ArchetypeReadWriteMaskPair Pair = ArchetypeReadWriteMaskPairFromFunction(registry, std::function(UpdatePheromones));
	Pair.ComponentReadMask |= ArchetypeMaskFromComponents<PheromonMapComponent>();
	return Pair;
}

void PheromoneSpawnerSystem::Update(World& world)
{
	world.AccessEntityManager().ForeachSerial(world, std::function(UpdatePheromones));
}
