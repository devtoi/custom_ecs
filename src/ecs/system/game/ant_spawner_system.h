#pragma once

#include <ecs/system/system.h>

class AntSpawnerSystem : public System
{
public:
	static constexpr std::string_view NAME = "AntSpawnerSystem";
	AntSpawnerSystem();

	ArchetypeReadWriteMaskPair RegisterComponents(ComponentStorageRegistry& registry) override;
	void Initialize(World& world) override;
};
