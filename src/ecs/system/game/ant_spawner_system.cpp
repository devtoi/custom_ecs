#include "ant_spawner_system.h"
#include "ecs/simulation_input/game/spawn_ant_input.h"
#include <ecs/world.h>
#include <ecs/component/velocity_component.h>
#include <ecs/component/position_component.h>
#include <ecs/component/game/food_mover_component.h>
#include <ecs/component/game/food_storage_component.h>
#include <ecs/component/game/home_tracker_component.h>
#include <ecs/component/tile_map_component.h>

namespace
{
	static constexpr glm::ivec2 SPAWN_AREA_X{ (MAP_SIZE_WIDTH_TILES / 2) - (MAP_SIZE_WIDTH_TILES / 10),
		(MAP_SIZE_WIDTH_TILES / 2) + (MAP_SIZE_WIDTH_TILES / 10) };
	static constexpr glm::ivec2 SPAWN_AREA_Y{ (MAP_SIZE_HEIGHT_TILES / 2) - (MAP_SIZE_HEIGHT_TILES / 10),
		(MAP_SIZE_HEIGHT_TILES / 2) + (MAP_SIZE_HEIGHT_TILES / 10) };
}

AntSpawnerSystem::AntSpawnerSystem() : System(AntSpawnerSystem::NAME)
{
}

void SpawnRandom(World& world)
{
	ArchetypeMask mask =
		ArchetypeMaskFromComponents<PositionComponent, VelocityComponent, FoodMoverComponent, HomeTrackerComponent>();

	for (int i = 0; i < 1000; ++i)
	{
		world.AccessEntityManager().CreateEntity(mask);
	}

	{
		std::mt19937 generator(123);
		std::uniform_int_distribution<int32_t> random_position_x(SPAWN_AREA_X.x, SPAWN_AREA_X.y);
		std::uniform_int_distribution<int32_t> random_position_y(SPAWN_AREA_Y.x, SPAWN_AREA_Y.y);
		std::uniform_int_distribution<int32_t> random_velocity(-1, 1);

		Entity homeEntity = world.GetEntityManager().GetFirstEntityWithComponent<FoodStorageComponent>();
		const auto& PositionOfHome = world.GetEntityManager().GetComponent<PositionComponent>(homeEntity);

		ArchetypeBucket* bucket = world.AccessEntityManager().FindArchetypeBucket(mask);
		assert(bucket);
		for (size_t i = 0; i < bucket->GetSize(); ++i)
		{
			auto& pos = bucket->AccessComponent<PositionComponent>(i);
			pos.position.x = random_position_x(generator);
			pos.position.y = random_position_y(generator);

			auto& vel = bucket->AccessComponent<VelocityComponent>(i);
			vel.direction.x = random_velocity(generator);
			vel.direction.y = random_velocity(generator);

			// TODO: Make diagonal not cost 2
			vel.left = (std::abs(vel.direction.x) + std::abs(vel.direction.y)) * TILE_SUB_SIZE;

			auto& home = bucket->AccessComponent<HomeTrackerComponent>(i);
			home.HomeEntity = homeEntity;
			home.PositionOfHome = PositionOfHome.position;
		}
	}
}


ArchetypeReadWriteMaskPair AntSpawnerSystem::RegisterComponents(ComponentStorageRegistry& registry)
{
	ArchetypeReadWriteMaskPair Pair = System::RegisterComponents(registry);
	registry.RegisterComponent<PositionComponent>();
	registry.RegisterComponent<VelocityComponent>();
	registry.RegisterComponent<FoodMoverComponent>();
	SimulationInputFactory::RegisterCreationFunction(
		SpawnAntInput::ID, std::function([]() { return std::make_unique<SpawnAntInput>(); }));
	return Pair;
}

void AntSpawnerSystem::Initialize(World& world)
{
	SpawnRandom(world);
}
