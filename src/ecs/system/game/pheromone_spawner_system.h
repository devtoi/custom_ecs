#pragma once

#include <ecs/system/system.h>

class PheromoneSpawnerSystem : public System
{
public:
	static constexpr std::string_view NAME = "PheromoneSpawnerSystem";
	PheromoneSpawnerSystem();

	ArchetypeReadWriteMaskPair RegisterComponents(ComponentStorageRegistry& registry) override;
	void Update(World& world) override;
};
