#pragma once

#include <ecs/system/system.h>

class HomeSpawnerSystem : public System
{
public:
	static constexpr std::string_view NAME = "HomeSpawnerSystem";
	HomeSpawnerSystem();

	ArchetypeReadWriteMaskPair RegisterComponents(ComponentStorageRegistry& registry) override;
	void Initialize(World& world) override;
};
