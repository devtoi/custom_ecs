#pragma once

#include <ecs/system/system.h>

class FoodSpawnerSystem : public System
{
public:
	static constexpr std::string_view NAME = "FoodSpawnerSystem";
	FoodSpawnerSystem();

	void Initialize(World& world) override;
};
