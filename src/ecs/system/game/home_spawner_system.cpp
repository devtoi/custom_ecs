#include "home_spawner_system.h"

#include <ecs/component/game/home_tracker_component.h>
#include <ecs/component/game/food_mover_component.h>
#include <ecs/component/game/food_storage_component.h>
#include <ecs/component/position_component.h>
#include <ecs/component/tile_map_component.h>
#include <ecs/entity_manager.h>
#include <ecs/world.h>

HomeSpawnerSystem::HomeSpawnerSystem() : System(HomeSpawnerSystem::NAME)
{
}

ArchetypeReadWriteMaskPair HomeSpawnerSystem::RegisterComponents(ComponentStorageRegistry& registry)
{
	registry.RegisterComponent<HomeTrackerComponent>();
	registry.RegisterComponent<FoodStorageComponent>();
	return System::RegisterComponents(registry);
}

void HomeSpawnerSystem::Initialize(World& world)
{
	ArchetypeMask mask = ArchetypeMaskFromComponents<PositionComponent, FoodStorageComponent>();

	Entity entity = world.AccessEntityManager().CreateEntity(mask);

	auto& pos = world.AccessEntityManager().AccessComponent<PositionComponent>(entity);
	pos.position.x = MAP_SIZE_WIDTH_TILES / 2;
	pos.position.y = MAP_SIZE_HEIGHT_TILES / 2;
}
