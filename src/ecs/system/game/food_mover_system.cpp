#include "food_mover_system.h"

#include <ecs/component/game/food_mover_component.h>
#include <ecs/component/game/food_storage_component.h>
#include <ecs/component/position_component.h>
#include <ecs/component/tile_map_component.h>
#include <ecs/component/game/home_tracker_component.h>
#include <ecs/world.h>

FoodMoverSystem::FoodMoverSystem() : System(FoodMoverSystem::NAME)
{
}

void UpdateFoodMoving(ComponentUpdateParameters& params, ComponentBucket<FoodMoverComponent>& FoodMoverBucket,
	ComponentBucket<HomeTrackerComponent>& HomeTrackerBucket, ComponentBucket<PositionComponent>& PositionBucket)
{
	auto* pTileMap = params.World.AccessEntityManager().AccessFirstComponent<TileMapComponent>();
	if (pTileMap == nullptr)
	{
		return;
	}

	for (size_t i = params.Start; i < params.End; ++i)
	{
		PositionComponent& PositionComponent = PositionBucket.AccessComponent(i);
		HomeTrackerComponent& HomeTracker = HomeTrackerBucket.AccessComponent(i);
		FoodMoverComponent& FoodMover = FoodMoverBucket.AccessComponent(i);
		glm::ivec2 Position = PositionComponent.position;

		if (Position == HomeTracker.PositionOfHome)
		{
			params.World.AccessEntityManager()
				.AccessComponent<FoodStorageComponent>(HomeTracker.HomeEntity)
				.FoodAmount += FoodMover.FoodAmount;
			FoodMover.FoodAmount = 0;
		}

		if (Position.x >= 0 && Position.x < MAP_SIZE_WIDTH_TILES && Position.y >= 0 &&
			Position.y < MAP_SIZE_HEIGHT_TILES)
		{
			auto& Tile = pTileMap->map[Position.x][Position.y];
			if (Tile == TileType::Food)
			{
				FoodMover.FoodAmount += 1;
				if (pTileMap->_TileNumber[Position.x][Position.y]-- <= 0)
				{
					Tile = TileType::Passable;
					pTileMap->_Dirty = true;
				}
			}
		}
	}
}

ArchetypeReadWriteMaskPair FoodMoverSystem::RegisterComponents(ComponentStorageRegistry& registry)
{
	ArchetypeReadWriteMaskPair Pair = ArchetypeReadWriteMaskPairFromFunction(registry, std::function(UpdateFoodMoving));
	Pair.ComponentWriteMask |= ArchetypeMaskFromComponents<TileMapComponent>();
	return Pair;
}

void FoodMoverSystem::Update(World& world)
{
	world.AccessEntityManager().ForeachSerial(world, std::function(UpdateFoodMoving));
}
