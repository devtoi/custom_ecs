#include "ecs/component/tile_map_component.h"
#define NOMINMAX
#include "pheromone_disappater_system.h"
#include <algorithm>

#include <ecs/component/game/pheromone_map_component.h>
#include <ecs/world.h>

namespace
{
	constexpr PheromoneStrength HOME_PHEROMONE_DISSAPPATION{ 0.3 };
	constexpr PheromoneStrength FOOD_PHEROMONE_DISSAPPATION{ 0.1 };
	constexpr PheromoneStrength PHEROMONE_KILLOFF{ 0.001 };
}

PheromoneDisappaterSystem::PheromoneDisappaterSystem() : System(PheromoneDisappaterSystem::NAME)
{
}

void DissapatePheromones(ComponentUpdateParameters& Parameters, ComponentBucket<PheromonMapComponent>& PheromonMaps)
{
	if (Parameters.World.GetSimulationTimeStep() == World::SimulationTimeUnit{ 0 })
	{
		return;
	}
	const int Axis = Parameters.World.GetSimulationFrameNumber() % 2;
	const int XIncrease = Axis == 0 ? 2 : 1;
	const int YIncrease = Axis == 0 ? 1 : 2;
	for (size_t i = Parameters.Start; i < Parameters.End; ++i)
	{
		auto& Map = PheromonMaps.AccessComponent(i);
		for (int x = 0; x < MAP_SIZE_WIDTH_TILES; x += 1)
		{
			for (int y = 0; y < MAP_SIZE_HEIGHT_TILES; y += 1)
			// for (int x = OffsetX; x < MAP_SIZE_WIDTH_TILES; x += 3)
			// {
			// 	for (int y = OffsetY; y < MAP_SIZE_HEIGHT_TILES; y += 3)
			{
				auto& Food = Map.FoodPheromoneTiles[x][y];
				PheromoneStrength Min{ 0 };
				Food =
					std::max(Min, Food - Food * FOOD_PHEROMONE_DISSAPPATION * Parameters.World.GetSimulationTimeStep());
				if (Food < PHEROMONE_KILLOFF)
				{
					Food = Min;
				}

				auto& Home = Map.HomePheromoneTiles[x][y];
				Home =
					std::max(Min, Home - Home * HOME_PHEROMONE_DISSAPPATION * Parameters.World.GetSimulationTimeStep());
				if (Home < PHEROMONE_KILLOFF)
				{
					Home = Min;
				}
			}
		}
		for (int x = 0; x < MAP_SIZE_WIDTH_TILES; x += XIncrease)
		{
			for (int y = 0; y < MAP_SIZE_HEIGHT_TILES; y += YIncrease)
			{
				auto& SpreaderFood = Map.FoodPheromoneTiles[x][y];
				auto& SpreaderHome = Map.HomePheromoneTiles[x][y];
				// if (SpreaderFood < 10 && SpreaderHome < 10)
				// {
				// 	continue;
				// }

				for (int dx = std::max(x - XIncrease + 1, 0);
					 dx <= std::min(x + XIncrease - 1, static_cast<int>(MAP_SIZE_WIDTH_TILES) - 1); ++dx)
				{
					for (int dy = std::max(y - YIncrease + 1, 0);
						 dy <= std::min(y + YIncrease - 1, static_cast<int>(MAP_SIZE_HEIGHT_TILES) - 1); ++dy)
					{
						{
							if (dx != x && dy != y)
							{
								if (SpreaderFood > PheromoneStrength{ 0 })
								{
									auto& Food = Map.FoodPheromoneTiles[dx][dy];
									const auto IncreaseFood =
										SpreaderFood *
										(FOOD_PHEROMONE_DISSAPPATION * Parameters.World.GetSimulationTimeStep()) / 3;
									Food = std::min(Food + IncreaseFood, MAX_FOOD_PHEROMONE_STRENGTH);
								}

								if (SpreaderHome > PheromoneStrength{ 0 })
								{
									auto& Home = Map.HomePheromoneTiles[dx][dy];
									const auto IncreaseHome = SpreaderHome * HOME_PHEROMONE_DISSAPPATION *
															  Parameters.World.GetSimulationTimeStep() / 3;
									Home = std::min(Home + IncreaseHome, MAX_HOME_PHEROMONE_STRENGTH);
								}
							}
						}
					}
				}
			}
		}
	}
}

void PheromoneDisappaterSystem::Update(World& world)
{
	world.AccessEntityManager().ForeachParallell(world, std::function(DissapatePheromones));
}
