#include "food_spawner_system.h"
#include <ecs/world.h>
#include <ecs/component/tile_map_component.h>

namespace
{
	constexpr int NR_OF_FOOD_TO_SPAWN = 600;
	constexpr int MEAN_NR_OF_FOOD_IN_CLUSTER = 100;
}

FoodSpawnerSystem::FoodSpawnerSystem() : System(FoodSpawnerSystem::NAME)
{
}

void SpawnFoodOnTileMap(ComponentUpdateParameters& Parameters, ComponentBucket<TileMapComponent>& TileMaps)
{
	for (size_t IndexInBucket = Parameters.Start; IndexInBucket < Parameters.End; ++IndexInBucket)
	{
		TileMapComponent& TileMap = TileMaps.AccessComponent(IndexInBucket);
		std::mt19937 generator(12);
		std::uniform_int_distribution<int> RandomTileX(0, MAP_SIZE_WIDTH_TILES);
		std::uniform_int_distribution<int> RandomTileY(0, MAP_SIZE_HEIGHT_TILES);
		std::poisson_distribution<size_t> RandomAmount(MEAN_NR_OF_FOOD_IN_CLUSTER);
		std::uniform_int_distribution<int> RandomNeighbour(-8, 8);
		for (int i = 0; i < NR_OF_FOOD_TO_SPAWN;)
		{
			auto ClusterAmount = RandomAmount(generator);
			int x = RandomTileX(generator);
			int y = RandomTileY(generator);

			for (int j = 0; j < ClusterAmount && i < NR_OF_FOOD_TO_SPAWN; ++j, ++i)
			{
				int xNeighbour = x + RandomNeighbour(generator);
				xNeighbour = std::clamp(xNeighbour, 0, static_cast<int>(MAP_SIZE_WIDTH_TILES) - 1);

				int yNeighbour = y + RandomNeighbour(generator);
				yNeighbour = std::clamp(yNeighbour, 0, static_cast<int>(MAP_SIZE_HEIGHT_TILES) - 1);
				auto& Tile = TileMap.map.at(xNeighbour).at(yNeighbour);
				if (Tile != TileType::Impassable && Tile != TileType::Food)
				{
					Tile = TileType::Food;
					TileMap._TileNumber.at(xNeighbour).at(yNeighbour) = 10;
				}
				else
				{
					--i;
				}
			}
		}
	}
}

void FoodSpawnerSystem::Initialize(World& world)
{
	world.AccessEntityManager().ForeachSerial(world, std::function(SpawnFoodOnTileMap));
}
