#pragma once

#include <ecs/system/system.h>

class FoodMoverSystem : public System
{
public:
	static constexpr std::string_view NAME = "FoodMoverSystem";
	FoodMoverSystem();

	ArchetypeReadWriteMaskPair RegisterComponents(ComponentStorageRegistry& registry) override;
	void Update(World& world) override;
};
