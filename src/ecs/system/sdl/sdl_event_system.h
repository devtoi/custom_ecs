#pragma once

#include "../system.h"

class SDLEventSystem final : public System
{
public:
	static constexpr std::string_view NAME = "SDLEventSystem";
	SDLEventSystem();

	void Update(World& world) override;
};
