#include "sdl_event_system.h"

#include <SDL2/SDL.h>
#if USE_VULKAN
#if USE_IMGUI
#include <imgui/misc/single_file/imgui_single_file.h>
#include <imgui/backends/imgui_impl_sdl.h>
#endif
#endif

#include "../../world.h"

SDLEventSystem::SDLEventSystem() : System(SDLEventSystem::NAME)
{
}


void SDLEventSystem::Update(World& world)
{
	SDL_Event event;
	while (SDL_PollEvent(&event))
	{
#if USE_VULKAN
#if USE_IMGUI
		ImGui_ImplSDL2_ProcessEvent(&event);
#endif
#endif
		switch (event.type)
		{

		case SDL_WINDOWEVENT: {
			switch (event.window.event)
			{
			case SDL_WINDOWEVENT_CLOSE: {
				event.type = SDL_QUIT;
				SDL_PushEvent(&event);
				break;
			}
			}
			break;
		}

		case SDL_QUIT: {
			world.MarkForShutdown();
		}
		}
	}
}
