#pragma once

#include "system.h"

struct FoodMoverComponent;
struct HomeTrackerComponent;
struct PositionComponent;
struct TileMapComponent;
struct PheromonMapComponent;
struct VelocityComponent;

class MovementSystem final : public System
{
public:
	static constexpr std::string_view NAME = "MovementSystem";
	static constexpr size_t NeighbourCount = 9;

	MovementSystem();

	static std::array<int64_t, NeighbourCount> DetermineProbabilities(const FoodMoverComponent& FoodMover,
		const HomeTrackerComponent& HomeTracker, const PositionComponent& Position, const TileMapComponent& TileMap,
		const PheromonMapComponent& PheromonMap, const VelocityComponent& Velocity, World& World);

	ArchetypeReadWriteMaskPair RegisterComponents(ComponentStorageRegistry& registry) override;
	void Initialize(World& World) override;
	void Update(World& world) override;
};
