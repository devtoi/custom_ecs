#pragma once

#include <string>
#include "../archetype_bucket.h"
#include "string/string_id.h"

class World;
class ComponentStorageRegistry;

class System
{
public:
	virtual ~System() = default;
	System(std::string_view Name);
	void SetupComponentDependencies(ComponentStorageRegistry& registry);

	template <typename ComponentBucket> void RegisterComponent(ComponentStorageRegistry& Registry)
	{
		Registry.RegisterComponent<typename ComponentBucket::ComponentType>();
	}

	template <typename... ComponentBuckets>
	ArchetypeReadWriteMaskPair ArchetypeReadWriteMaskPairFromFunction(ComponentStorageRegistry& Registry,
		std::function<void(ComponentUpdateParameters&, ComponentBuckets&...)> /*func*/)
	{
		(RegisterComponent<ComponentBuckets>(Registry), ...);
		ArchetypeReadWriteMaskPair Pair;
		Pair.ComponentReadMask = ArchetypeMaskFromComponentBucketsRead<ComponentBuckets...>();
		Pair.ComponentWriteMask = ArchetypeMaskFromComponentBucketsWrite<ComponentBuckets...>();
		return Pair;
	}

	virtual ArchetypeReadWriteMaskPair RegisterComponents(ComponentStorageRegistry& registry);
	virtual void Initialize(World& world){};
	virtual void Update(World& world);
	virtual void Uninitialize(World& world){};

	[[nodiscard]] const std::string& GetName() const;

	[[nodiscard]] ArchetypeMask GetComponentReadMask() const;
	[[nodiscard]] ArchetypeMask GetComponentWriteMask() const;
	[[nodiscard]] StringID GetNameID() const;
	[[nodiscard]] virtual bool RunWhenPaused()
	{
		return false;
	}

private:
	ArchetypeReadWriteMaskPair ComponentMask;
	const std::string m_Name;
	StringID m_NameID;
};
