#pragma once

#include <memory>
#include <vector>

#include "component_bucket_storage_base.h"
#include "component_bucket_storage.h"

class ComponentStorageRegistry
{
public:
	ComponentStorageRegistry() = default;
	ComponentStorageRegistry(const ComponentStorageRegistry&) = default;
	ComponentStorageRegistry(ComponentStorageRegistry&&) = delete;
	ComponentStorageRegistry& operator=(const ComponentStorageRegistry&) = default;
	ComponentStorageRegistry& operator=(ComponentStorageRegistry&&) = delete;
	~ComponentStorageRegistry();

	void serialize(SerializerBase& serializer);

	template <typename Component> void RegisterComponent()
	{
		ComponentTypeHandle handle = TypeSequence<Component>::index();
		if (HasStorageFor(handle))
			return;

		auto index = handle.get_underlaying_value();
		if (index >= m_ComponentToStorage.size())
			m_ComponentToStorage.resize(index + 1);

		std::unique_ptr<ComponentBucketStorageBase> storage = std::make_unique<ComponentBucketStorage<Component>>();
		m_ComponentToStorage[index] = std::move(storage);
	}

	bool HasStorageFor(ComponentTypeHandle handle) const;
	ComponentBucketStorageBase& AccessComponentStorage(ComponentTypeHandle handle);

private:
	std::vector<std::unique_ptr<ComponentBucketStorageBase>> m_ComponentToStorage;
};
