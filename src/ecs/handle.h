#pragma once

#include <functional>

template <typename Tag, typename _Implementation, _Implementation _InvalidValue> class Handle
{
public:
	using Implementation = _Implementation;
	static constexpr Implementation InvalidValue = _InvalidValue;

	static Handle invalid()
	{
		return Handle();
	}

	constexpr Handle() : m_Val(_InvalidValue)
	{
	}

	// Explicit constructor:
	constexpr explicit Handle(Implementation val) : m_Val(val)
	{
	}

	~Handle() = default;
	constexpr Handle(const Handle& other) = default;
	constexpr Handle(Handle&&) noexcept = default;
	constexpr Handle& operator=(Handle&&) noexcept = default;
	Handle& operator=(const Handle& other) = default;

	// Explicit conversion to get back the Implementation
	explicit operator Implementation() const
	{
		return m_Val;
	}

	[[nodiscard]] bool is_valid() const
	{
		return m_Val != InvalidValue;
	}

	friend bool operator==(Handle a, Handle b)
	{
		return a.m_Val == b.m_Val;
	}

	friend bool operator!=(Handle a, Handle b)
	{
		return a.m_Val != b.m_Val;
	}

	Implementation get_underlaying_value() const
	{
		return m_Val;
	}

	Implementation& access_underlaying_value()
	{
		return m_Val;
	}

private:
	Implementation m_Val;
};

namespace std
{
	template <typename Tag, typename _Implementation, _Implementation _InvalidValue>
	struct hash<Handle<Tag, _Implementation, _InvalidValue>>
	{
		size_t operator()(const Handle<Tag, _Implementation, _InvalidValue>& handle) const
		{
			return std::hash<_Implementation>()(static_cast<_Implementation>(handle));
		}
	};
}
