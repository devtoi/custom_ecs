#include "tick_input.h"

#include <fstream>

#include "ecs/component/network/network_match_component.h"
#include "ecs/component/network/network_player_component.h"
#include "ecs/component/network/network_to_simulation_input_component.h"
#include "ecs/world.h"
#include "serializer/json/json_text_writer.h"
#include "spdlog/spdlog.h"

TickInput::TickInput() : SimulationInput(ID_NAME)
{
}

bool TickInput::IsValid(World& /*World*/)
{
	return true;
}

void TickInput::Execute(World& World)
{
	World.AccessEntityManager().ForeachSerial(World,
		std::function(
			[&World = World, &SessionID = FromSessionID, &Frame = Frame, &Checksum = Checksum](
				ComponentUpdateParameters& Parameters, const ComponentBucket<NetworkPlayerComponent>& NetworkPlayers,
				ComponentBucket<NetworkToSimulationInputComponent>& NetworkToSimulationInputs) {
				const auto* pMatch = World.GetEntityManager().GetFirstComponent<NetworkMatchComponent>();

				const NetworkToSimulationInputComponent* pLocalPlayer = nullptr;
				if (pMatch->_LocalPlayer.is_valid())
				{
					pLocalPlayer =
						&World.GetEntityManager().GetComponent<NetworkToSimulationInputComponent>(pMatch->_LocalPlayer);
				}

				size_t LowestFrame = std::numeric_limits<size_t>::max();
				for (auto i = Parameters.Start; i < Parameters.End; ++i)
				{
					const auto& Player = NetworkPlayers.GetComponent(i);
					auto& Input = NetworkToSimulationInputs.AccessComponent(i);
					if (Player.SessionID == SessionID)
					{
						Input._LastFinishedFrame = Frame;
						auto& FrameHash = Input._FrameHashes.at(Frame % NETWORK_FRAME_BUFFER_SIZE);
						FrameHash = Checksum;
						if (pLocalPlayer && pLocalPlayer->_LastFinishedFrame >= Frame)
						{
							auto Expected = pLocalPlayer->_FrameHashes.at(Frame % NETWORK_FRAME_BUFFER_SIZE);
							if (Expected != FrameHash)
							{
								spdlog::error("Frame {} got checksum {} expected {}", Frame, FrameHash, Expected);
								JsonTextWriter writer;
								writer.serialize("world", Parameters.World);
								std::ofstream file(std::to_string(Player.SessionID.GetID()));
								file << writer.GetJson().dump(4);
								file.close();
							}
						}
					}
					LowestFrame = std::min(Input._LastFinishedFrame, LowestFrame);
				}
				World.SetTargetSimulationFrameNumber(LowestFrame + NETWORK_BUFFERED_FRAMES);
			}));
}

void TickInput::Serialize(SerializerBase& Serializer)
{
	Serializer.serialize("frame", Frame);
	Serializer.serialize("checksum", Checksum);
	SimulationInput::Serialize(Serializer);
}
