#pragma once

#include "ecs/simulation_input/simulation_input.h"

class TickInput : public SimulationInput
{
public:
	static constexpr std::string_view ID_NAME = "TickInput";
	static constexpr auto ID = StringID{ ID_NAME };
	TickInput();

	bool IsValid(World& World) override;
	void Execute(World& World) override;
	void Serialize(SerializerBase& Serializer) override;

	StringID FromSessionID;

	size_t Frame = 0;
	int32_t Checksum = 0;
};
