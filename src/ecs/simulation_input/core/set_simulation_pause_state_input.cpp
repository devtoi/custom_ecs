#include "set_simulation_pause_state_input.h"

#include "ecs/simulation_input/network/tick_input.h"
#include "ecs/utils/simulation_io.h"
#include "ecs/world.h"

PauseSimulationInput::PauseSimulationInput() : SimulationInput(ID_NAME)
{
}

bool PauseSimulationInput::IsValid(World& World)
{
	return !World.IsSimulationPaused();
}

void PauseSimulationInput::Execute(World& World)
{
	/* bool SwitchedState =  */ World.SetSimulationPauseState(true);
	// auto* pMatch = World.AccessEntityManager().AccessFirstComponent<NetworkMatchComponent>();
	// if (!_PauseState && SwitchedState)
	// {
	// 	if (pMatch && !pMatch->_Match.matchId.empty())
	// 	{
	// 		SimulationIO::RecordInput(World, TickInput{});
	// 	}
	// 	else
	// 	{
	// 		// World.IncreaseTargetSimulationFrameNumber();
	// 	}
	// }
}

UnpauseSimulationInput::UnpauseSimulationInput() : SimulationInput(ID_NAME)
{
}

bool UnpauseSimulationInput::IsValid(World& World)
{
	return World.IsSimulationPaused();
}

void UnpauseSimulationInput::Execute(World& World)
{
	/* bool SwitchedState =  */ World.SetSimulationPauseState(false);
	// auto* pMatch = World.AccessEntityManager().AccessFirstComponent<NetworkMatchComponent>();
	// if (!_UnpauseState && SwitchedState)
	// {
	// 	if (pMatch && !pMatch->_Match.matchId.empty())
	// 	{
	// 		SimulationIO::RecordInput(World, TickInput{});
	// 	}
	// 	else
	// 	{
	// 		// World.IncreaseTargetSimulationFrameNumber();
	// 	}
	// }
}
