#pragma once

#include "ecs/simulation_input/simulation_input.h"

class PauseSimulationInput : public SimulationInput
{
public:
	static constexpr std::string_view ID_NAME = "PauseSimulationInput";
	static constexpr auto ID = StringID{ ID_NAME };
	PauseSimulationInput();

	bool IsValid(World& World) override;
	void Execute(World& World) override;
};

class UnpauseSimulationInput : public SimulationInput
{
public:
	static constexpr std::string_view ID_NAME = "UnpauseSimulationInput";
	static constexpr auto ID = StringID{ ID_NAME };
	UnpauseSimulationInput();

	bool IsValid(World& World) override;
	void Execute(World& World) override;
};
