#pragma once

#include "simulation_input.h"
#include <memory>

namespace SimulationInputFactory
{
	void RegisterCreationFunction(StringID ID, std::function<std::unique_ptr<SimulationInput>()>&& CreationFunction);
	std::unique_ptr<SimulationInput> CreateInput(StringID ID);
};
