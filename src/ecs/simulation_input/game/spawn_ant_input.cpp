#include "spawn_ant_input.h"

#include "ecs/component/game/food_mover_component.h"
#include "ecs/component/game/food_storage_component.h"
#include "ecs/component/game/home_tracker_component.h"
#include "ecs/component/position_component.h"
#include "ecs/component/velocity_component.h"
#include "ecs/world.h"

SpawnAntInput::SpawnAntInput() : SimulationInput(ID_NAME)
{
}

bool SpawnAntInput::IsValid(World& /*World*/)
{
	return true;
}

void SpawnAntInput::Execute(World& World)
{
	ArchetypeMask mask =
		ArchetypeMaskFromComponents<PositionComponent, VelocityComponent, FoodMoverComponent, HomeTrackerComponent>();

	auto AntEntity = World.AccessEntityManager().CreateEntity(mask);

	std::mt19937 generator(World.GetVisualizationFrameNumber());
	std::uniform_int_distribution<int32_t> random_velocity(-1, 1);

	Entity homeEntity = World.GetEntityManager().GetFirstEntityWithComponent<FoodStorageComponent>();
	const auto& PositionOfHome = World.GetEntityManager().GetComponent<PositionComponent>(homeEntity);

	auto& pos = World.AccessEntityManager().AccessComponent<PositionComponent>(AntEntity);
	pos.position = PositionOfHome.position;

	auto& vel = World.AccessEntityManager().AccessComponent<VelocityComponent>(AntEntity);
	pos.position = PositionOfHome.position;
	vel.direction.x = random_velocity(generator);
	vel.direction.y = random_velocity(generator);

	// TODO: Make diagonal not cost 2
	vel.left = (std::abs(vel.direction.x) + std::abs(vel.direction.y)) * TILE_SUB_SIZE;

	auto& home = World.AccessEntityManager().AccessComponent<HomeTrackerComponent>(AntEntity);
	home.HomeEntity = homeEntity;
	home.PositionOfHome = PositionOfHome.position;
}

void SpawnAntInput::Serialize(SerializerBase& Serializer)
{
	SimulationInput::Serialize(Serializer);
}
