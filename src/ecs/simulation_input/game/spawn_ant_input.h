#pragma once

#include "ecs/simulation_input/simulation_input.h"

class SpawnAntInput : public SimulationInput
{
public:
	static constexpr std::string_view ID_NAME = "SpawnAntInput";
	static constexpr auto ID = StringID{ ID_NAME };
	SpawnAntInput();

	bool IsValid(World& World) override;
	void Execute(World& World) override;
	void Serialize(SerializerBase& Serializer) override;
};
