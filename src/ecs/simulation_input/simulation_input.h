#pragma once

#include "serializer/serializer_base.h"
#include "string/string_id.h"

class World;

class SimulationInput
{
public:
	SimulationInput(std::string_view ID) : _ID(ID)
	{
	}
	virtual ~SimulationInput() = default;
	SimulationInput(const SimulationInput&) = default;
	SimulationInput(SimulationInput&&) = default;
	SimulationInput& operator=(const SimulationInput&) = default;
	SimulationInput& operator=(SimulationInput&&) = default;

	virtual void Serialize(SerializerBase& Serializer)
	{
		Serializer.serialize("id", _ID.AccessUnderlayingValue());
		Serializer.serialize("target_frame", _TargetFrame);
	}
	virtual bool IsValid(World& /*World*/)
	{
		return true;
	};
	virtual void Execute(World& World) = 0;

	[[nodiscard]] StringID GetID() const
	{
		return _ID;
	}

	[[nodiscard]] size_t GetTargetFrame() const
	{
		return _TargetFrame;
	}
	size_t _TargetFrame = 0;

private:
	StringID _ID;
};
