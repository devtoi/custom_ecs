#include "simulation_input_factory.h"
#include "spdlog/spdlog.h"

namespace
{
	std::unordered_map<StringID, std::function<std::unique_ptr<SimulationInput>()>, StringIDHasher> _CreationFunctions;
}

void SimulationInputFactory::RegisterCreationFunction(
	StringID ID, std::function<std::unique_ptr<SimulationInput>()>&& CreationFunction)
{
	_CreationFunctions.emplace(ID, CreationFunction);
}

std::unique_ptr<SimulationInput> SimulationInputFactory::CreateInput(StringID ID)
{
	auto it = _CreationFunctions.find(ID);
	if (it != _CreationFunctions.end())
	{
		return it->second();
	}
	spdlog::error("Failed to create simulation input from invalid id {}", ID.GetID());
	return nullptr;
}
