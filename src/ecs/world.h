#pragma once

#include <chrono>
#include "ecs/simulation_input/simulation_input_factory.h"
#include "entity_manager.h"
#include "../serializer/serializer_base.h"
#include "../job/job_engine.h"
#include <ecs/core/system_collection.h>
#include <ecs/core/systems_bank.h>
#include <fpm/fixed.hpp>

class System;

class World final
{
public:
	using SimulationTimeUnit = fpm::fixed_16_16;

	World();
	~World();
	World(World&) = delete;
	World(World&&) = delete;
	World& operator=(World&) = delete;
	World& operator=(World&&) = delete;

	void InitializeSystems(SystemCollection& Collection);

	void Update();

	const EntityManager& GetEntityManager() const;
	EntityManager& AccessEntityManager();

	void serialize(SerializerBase& serializer);

	float GetRealTimeDelta() const;

	SimulationTimeUnit GetSimulationTimeStep() const;
	void SetSimulationTimeStep(SimulationTimeUnit TimeStep);
	int64_t GetSimulationStepsPerFrame() const;
	void SetSimulationStepsPerFrame(int64_t Steps);
	void SetSimulationSpeed(size_t Speed);
	size_t GetSimulationSpeed() const;
	// Returns true if switched state
	bool SetSimulationPauseState(bool PauseState);
	// Returns what the new pause state is
	bool TogglePauseSimulation();
	bool IsSimulationPaused() const;

	const JobEngine* GetJobEngine() const;
	JobEngine* AccessJobEngine();

	bool ShouldShutdown() const;
	void MarkForShutdown();

	size_t GetVisualizationFrameNumber() const noexcept;
	size_t GetSimulationFrameNumber() const noexcept;
	size_t GetTargetSimulationFrameNumber() const noexcept;
	void SetTargetSimulationFrameNumber(size_t FrameTarget) noexcept;
	void IncreaseTargetSimulationFrameNumber() noexcept;
	std::chrono::high_resolution_clock::time_point GetPreviousTimePoint() const noexcept;
	std::chrono::high_resolution_clock::time_point GetLastFrameStart() const noexcept;

	void SetNextSystemCollection(SystemCollection&& SimulationCollection, SystemCollection&& VisualizationCollection);
	const SystemCollection& GetSimulationSystemCollection() const;
	SystemCollection& AccessSimulationSystemCollection();
	const SystemCollection& GetVisualizationSystemCollection() const;
	SystemCollection& AccessVisualizationSystemCollection();

	SystemsBank& AccessSystemsBank();
	const SystemsBank& GetSystemsBank() const;

	bool IsSimulating() const;

private:
	EntityManager m_EntityManager;
	SimulationTimeUnit _SimulationTimeStep{ 0.020 };
	int64_t _SimulationStepsPerFrame = 1;
	size_t _SimulationSpeed = 1;
	bool _IsSimulating = false;
	bool _PauseSimulation = true;
	float m_RealTimeDelta = 0.0f;
	std::chrono::high_resolution_clock::time_point m_LastVisualizationFrameStart;
	std::chrono::high_resolution_clock::time_point m_LastSimulationFrameStart;
	std::chrono::high_resolution_clock::time_point m_PreviousTimePoint;
	bool m_Shutdown = false;
	size_t m_VisualizationFrameNumber = 0;
	size_t m_SimulationFrameNumber = 0;
	size_t m_SimulationTargetFrameNumber = 1;

	SystemsBank _SystemsBank;
	SystemCollection _SimulationSystemCollection;
	SystemCollection _NextSimulationSystemCollection;
	SystemCollection _VisualizationSystemCollection;
	SystemCollection _NextVisualizationSystemCollection;
	bool _SwitchToNextSystemCollection = false;

	std::unique_ptr<JobEngine> m_JobEngine = nullptr;
};
