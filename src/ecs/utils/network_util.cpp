#include "network_util.h"

#include "ecs/component/network/nakama_network_component.h"
#include "ecs/component/network/network_match_component.h"
#include "ecs/component/network/network_player_component.h"
#include "ecs/component/network/network_to_simulation_input_component.h"
#include "ecs/world.h"
#include "spdlog/spdlog.h"

namespace NetworkUtil
{
	Entity AddPlayer(World& World, const NUserPresence& Presence, NetworkMatchComponent& NetworkMatch)
	{
		Entity PlayerEntity = World.AccessEntityManager().CreateEntity(
			ArchetypeMaskFromComponents<NetworkPlayerComponent, NetworkToSimulationInputComponent>());
		auto& Player = World.AccessEntityManager().AccessComponent<NetworkPlayerComponent>(PlayerEntity);
		Player.Presence = Presence;
		Player.SessionID = StringID{ Presence.sessionId };
		if (NetworkMatch._Match.self.sessionId == Presence.sessionId)
		{
			NetworkMatch._LocalPlayer = PlayerEntity;
			spdlog::info("Added local player \"{}\" with userId: {} sessionID: {}", Presence.username, Presence.userId,
				Presence.sessionId);
		}
		else
		{
			spdlog::info("Added network player \"{}\" with userID: {} sessionID: {}", Presence.username,
				Presence.userId, Presence.sessionId);
		}
		NetworkMatch.PlayerEntities.emplace_back(PlayerEntity);
		return PlayerEntity;
	}

	Entity CreateMatchEntity(World& World, const NMatch& Match)
	{
		Entity MatchEntity =
			World.AccessEntityManager().CreateEntity(ArchetypeMaskFromComponents<NetworkMatchComponent>());
		auto& NetworkMatch = World.AccessEntityManager().AccessComponent<NetworkMatchComponent>(MatchEntity);
		NetworkMatch._Match = Match;
		spdlog::info("Created network match Entity");

		for (const auto& Presence : Match.presences)
		{
			AddPlayer(World, Presence, NetworkMatch);
		}
		return MatchEntity;
	}

	void CreateMatch(World& World)
	{
		auto* pNakamaNetwork = World.AccessEntityManager().AccessFirstComponent<NakamaNetworkComponent>();
		pNakamaNetwork->_RealtimeClient->createMatch([&World = World](const NMatch& Match) {
			CreateMatchEntity(World, Match);
			spdlog::info("Created network match with ID: {}", Match.matchId);
		});
	}

	void JoinMatch(World& World, const std::string& MatchID)
	{
		auto* pNakamaNetwork = World.AccessEntityManager().AccessFirstComponent<NakamaNetworkComponent>();
		pNakamaNetwork->_RealtimeClient->joinMatch(MatchID, {}, [&World = World](const NMatch& Match) {
			CreateMatchEntity(World, Match);
			spdlog::info("Joined network match with ID: {}", Match.matchId);
		});
	}
}
