#pragma once

#include "ecs/component/core/simulation_input_component.h"
#if USE_NAKAMA
#include "ecs/component/network/network_match_component.h"
#include "ecs/component/network/network_to_simulation_input_component.h"
#endif
#include "ecs/simulation_input/simulation_input.h"
#include <memory>

#include "ecs/world.h"

namespace SimulationIO
{
	template <typename T> void RecordInput(World& World, const T& Input)
	{
#if USE_NAKAMA
		auto* pMatch = World.AccessEntityManager().AccessFirstComponent<NetworkMatchComponent>();

		if (pMatch)
		{
			auto& Local =
				World.AccessEntityManager().AccessComponent<NetworkToSimulationInputComponent>(pMatch->_LocalPlayer);
			auto TargetFrame = Local._LastFinishedFrame + NETWORK_BUFFERED_FRAMES;
			{
				auto pInputCopy = std::make_unique<T>(Input);
				pInputCopy->_TargetFrame = TargetFrame;
				// Record local
				Local._NetworkToSimulationInputs.at(TargetFrame % NETWORK_FRAME_BUFFER_SIZE)
					.emplace_back(std::move(pInputCopy));
			}
			{
				auto pInputCopy = std::make_unique<T>(Input);
				pInputCopy->_TargetFrame = TargetFrame;
				// Record to send
				pMatch->_OutboundInputs.emplace_back(std::move(pInputCopy));
			}
		}
		else
		{
#endif
			auto* pSimulationInput = World.AccessEntityManager().AccessFirstComponent<SimulationInputComponent>();
			pSimulationInput->_SimulationInputs.emplace_back(std::make_unique<T>(Input));
#if USE_NAKAMA
		}
#endif
	}
}
