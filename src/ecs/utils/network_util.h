#pragma once

#include "ecs/entity.h"
#include <string>
#include "nakama-cpp-c-wrapper/NakamaWrapper.h"

using namespace NAKAMA_NAMESPACE;
class World;
struct NetworkMatchComponent;

namespace NetworkUtil
{
	Entity AddPlayer(World& World, const NUserPresence& Presence, NetworkMatchComponent& NetworkMatch);
	void CreateMatch(World& World);
	void JoinMatch(World& World, const std::string& MatchID);
}
