#include "world.h"

#include <iostream>

#include <glm/vec2.hpp>
#include <spdlog/spdlog.h>

#include "archetype_bucket.h"
#include <string/string_database.h>
#include <profiler/profiler.h>
#include <profiler/profiler_scope.h>

World::World()
{
	m_PreviousTimePoint = std::chrono::high_resolution_clock::now();

	m_JobEngine = std::make_unique<JobEngine>();

	m_LastSimulationFrameStart = std::chrono::high_resolution_clock::now();
}

World::~World()
{
	{
		auto& Systems = _SimulationSystemCollection.AccessSystems();
		for (auto it = Systems.rbegin(); it != Systems.rend(); ++it)
		{
			(*it).pSystem->Uninitialize(*this);
		}
		Systems.clear();
	}
	{
		auto& Systems = _VisualizationSystemCollection.AccessSystems();
		for (auto it = Systems.rbegin(); it != Systems.rend(); ++it)
		{
			(*it).pSystem->Uninitialize(*this);
		}
		Systems.clear();
	}
	m_JobEngine.reset();
}

void World::InitializeSystems(SystemCollection& Collection)
{
	Collection.SortSystems();

	for (auto& system : Collection.AccessSystems())
	{
		if (system.Initialized)
		{
			continue;
		}
		system.pSystem->SetupComponentDependencies(AccessEntityManager().AccessComponentStorageRegistry());
	}

	for (auto& system : Collection.AccessSystems())
	{
		if (system.Initialized)
		{
			continue;
		}
		system.pSystem->Initialize(*this);
		system.Initialized = true;
	}
}

void World::Update()
{
	if (_SwitchToNextSystemCollection)
	{
		_SwitchToNextSystemCollection = false;
		_SimulationSystemCollection = _NextSimulationSystemCollection;
		_VisualizationSystemCollection = _NextVisualizationSystemCollection;
		InitializeSystems(_SimulationSystemCollection);
		InitializeSystems(_VisualizationSystemCollection);
	}
	std::chrono::high_resolution_clock::time_point now = std::chrono::high_resolution_clock::now();
	m_RealTimeDelta =
		std::chrono::duration_cast<std::chrono::nanoseconds>((now - m_PreviousTimePoint)).count() / 1'000'000'000.0f;
	m_LastVisualizationFrameStart = m_PreviousTimePoint;
	m_PreviousTimePoint = now;

	Profiler::NewFrame();

	m_RealTimeDelta = std::min(m_RealTimeDelta, 1.0f);
	SimulationTimeUnit SimulationDelta{
		std::chrono::duration_cast<std::chrono::nanoseconds>((now - m_LastSimulationFrameStart)).count() /
		1'000'000'000.0f
	};

	for (int i = 0; i < _SimulationStepsPerFrame; ++i)
	{
		if (!_PauseSimulation && m_SimulationFrameNumber <= m_SimulationTargetFrameNumber &&
			SimulationDelta >= _SimulationTimeStep / _SimulationSpeed)
		{
			m_LastSimulationFrameStart = std::chrono::high_resolution_clock::now();
			_IsSimulating = true;
			PROFILE_SCOPE("Simulation");
			for (auto& System : _SimulationSystemCollection.AccessSystems())
			{
				PROFILE_SCOPE(System.pSystem->GetName());
				System.pSystem->Update(*this);
			}
			++m_SimulationFrameNumber;
			_IsSimulating = false;
		}
		else
		{
			PROFILE_SCOPE("Paused Simulation");
			for (auto& System : _SimulationSystemCollection.AccessSystems())
			{
				if (System.pSystem->RunWhenPaused())
				{
					PROFILE_SCOPE(System.pSystem->GetName());
					System.pSystem->Update(*this);
				}
			}
		}
	}

	{
		PROFILE_SCOPE("Visualization");
		for (auto& System : _VisualizationSystemCollection.AccessSystems())
		{
			PROFILE_SCOPE(System.pSystem->GetName());
			System.pSystem->Update(*this);
		}
	}
	m_EntityManager.KillMarkedEntities();

	++m_VisualizationFrameNumber;
}

const EntityManager& World::GetEntityManager() const
{
	return m_EntityManager;
}

EntityManager& World::AccessEntityManager()
{
	return m_EntityManager;
}

void World::serialize(SerializerBase& serializer)
{
	serializer.serialize("m_EntityManager", m_EntityManager);
	serializer.serialize("frame", m_SimulationFrameNumber);
	serializer.serialize("target_frame", m_SimulationTargetFrameNumber);
}

World::SimulationTimeUnit World::GetSimulationTimeStep() const
{
	return _SimulationTimeStep;
}

float World::GetRealTimeDelta() const
{
	return m_RealTimeDelta;
}

void World::SetSimulationTimeStep(World::SimulationTimeUnit TimeStep)
{
	_SimulationTimeStep = TimeStep;
}

int64_t World::GetSimulationStepsPerFrame() const
{
	return _SimulationStepsPerFrame;
}

void World::SetSimulationStepsPerFrame(int64_t Steps)
{
	_SimulationStepsPerFrame = Steps;
}

void World::SetSimulationSpeed(size_t Speed)
{
	_SimulationSpeed = Speed;
}

size_t World::GetSimulationSpeed() const
{
	return _SimulationSpeed;
}

bool World::SetSimulationPauseState(bool PauseState)
{
	if (_PauseSimulation == PauseState)
	{
		return false;
	}
	_PauseSimulation = PauseState;
	return true;
}

bool World::TogglePauseSimulation()
{
	_PauseSimulation = !_PauseSimulation;
	return _PauseSimulation;
}

bool World::IsSimulationPaused() const
{
	return _PauseSimulation;
}

const JobEngine* World::GetJobEngine() const
{
	return m_JobEngine.get();
}

JobEngine* World::AccessJobEngine()
{
	return m_JobEngine.get();
}

bool World::ShouldShutdown() const
{
	return m_Shutdown;
}

void World::MarkForShutdown()
{
	m_Shutdown = true;
}

size_t World::GetVisualizationFrameNumber() const noexcept
{
	return m_VisualizationFrameNumber;
}

size_t World::GetSimulationFrameNumber() const noexcept
{
	return m_SimulationFrameNumber;
}

size_t World::GetTargetSimulationFrameNumber() const noexcept
{
	return m_SimulationTargetFrameNumber;
}

void World::SetTargetSimulationFrameNumber(size_t FrameTarget) noexcept
{
	m_SimulationTargetFrameNumber = FrameTarget;
}

void World::IncreaseTargetSimulationFrameNumber() noexcept
{
	m_SimulationTargetFrameNumber++;
}

std::chrono::high_resolution_clock::time_point World::GetPreviousTimePoint() const noexcept
{
	return m_PreviousTimePoint;
}

std::chrono::high_resolution_clock::time_point World::GetLastFrameStart() const noexcept
{
	return m_LastVisualizationFrameStart;
}

void World::SetNextSystemCollection(SystemCollection&& SimulationCollection, SystemCollection&& VisualizationCollection)
{
	_NextSimulationSystemCollection = SimulationCollection;
	_NextVisualizationSystemCollection = VisualizationCollection;
	_SwitchToNextSystemCollection = true;
}

const SystemCollection& World::GetSimulationSystemCollection() const
{
	return _SimulationSystemCollection;
}

SystemCollection& World::AccessSimulationSystemCollection()
{
	return _SimulationSystemCollection;
}

const SystemCollection& World::GetVisualizationSystemCollection() const
{
	return _VisualizationSystemCollection;
}

SystemCollection& World::AccessVisualizationSystemCollection()
{
	return _VisualizationSystemCollection;
}

SystemsBank& World::AccessSystemsBank()
{
	return _SystemsBank;
}

const SystemsBank& World::GetSystemsBank() const
{
	return _SystemsBank;
}

bool World::IsSimulating() const
{
	return _IsSimulating;
}
