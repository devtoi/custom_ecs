#include "component_storage_registry.h"
#include <cassert>

ComponentStorageRegistry::~ComponentStorageRegistry()
{
	// Reverse destruction order because some components may have dependencies (and therefore have to be constructed in
	// a certain order)
	while (!m_ComponentToStorage.empty())
	{
		m_ComponentToStorage.pop_back();
	}
}

void ComponentStorageRegistry::serialize(SerializerBase& serializer)
{
	size_t size = m_ComponentToStorage.size();
	serializer.push_array("component_to_storage", size, 0);
	for (auto& storage : m_ComponentToStorage)
	{
		serializer.push_element_object(0);
		storage->serialize(serializer);
		serializer.pop_object();
	}
	serializer.pop_array();
}

bool ComponentStorageRegistry::HasStorageFor(ComponentTypeHandle handle) const
{
	return m_ComponentToStorage.size() > handle.get_underlaying_value() &&
		   m_ComponentToStorage[handle.get_underlaying_value()] != nullptr;
}

ComponentBucketStorageBase& ComponentStorageRegistry::AccessComponentStorage(ComponentTypeHandle handle)
{
	assert(HasStorageFor(handle));
	return *m_ComponentToStorage[handle.get_underlaying_value()];
}
