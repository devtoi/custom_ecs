#include "archetype_bucket.h"

#include <algorithm>
#include <cassert>

ArchetypeBucket::ArchetypeBucket(const ArchetypeMask& componentsInArchetype, ArchetypeHandle archetypeHandle,
	ComponentStorageRegistry* componentStorageRegistry)
	: m_ComponentTypesInThisBucket(componentsInArchetype), m_ArchetypeHandle(archetypeHandle),
	  m_ComponentStorageRegistry(componentStorageRegistry)
{
	FetchBuckets();
}

void ArchetypeBucket::serialize(SerializerBase& serializer, ComponentStorageRegistry* componentStorageRegistry)
{
	m_ComponentStorageRegistry = componentStorageRegistry;
	serializer.serialize("m_ArchetypeHandle", m_ArchetypeHandle);
	serializer.serialize("m_NextIndex", m_NextIndex);
	serializer.serialize("m_IndexToEntity", m_IndexToEntity);
	serializer.serialize("m_EntityToIndex", m_EntityToIndex);

	if (serializer.is_reader())
		FetchBuckets();
}

const ArchetypeMask& ArchetypeBucket::GetComponentTypesInThisBucket() const
{
	return m_ComponentTypesInThisBucket;
}

size_t ArchetypeBucket::GetSize() const
{
	return m_NextIndex;
}

size_t ArchetypeBucket::LookupIndexForEntity(Entity entity) const
{
	auto it = m_EntityToIndex.find(entity);
	assert(it != m_EntityToIndex.end());
	return it->second;
}

Entity ArchetypeBucket::LookupEntityForIndex(size_t index) const
{
	return m_IndexToEntity[index];
}

ArchetypeHandle ArchetypeBucket::GetHandle() const
{
	return m_ArchetypeHandle;
}

void ArchetypeBucket::RemoveEntity(Entity entity)
{
	auto entityToIndexIt = m_EntityToIndex.find(entity);
	assert(entityToIndexIt != m_EntityToIndex.end());
	size_t index = entityToIndexIt->second;
	m_EntityToIndex.erase(entityToIndexIt);

	if (index == m_NextIndex - 1)
	{
		m_IndexToEntity[index] = Entity::invalid();
	}
	else
	{
		Entity lastEntity = LookupEntityForIndex(m_NextIndex - 1);
		m_IndexToEntity[index] = lastEntity;
		m_IndexToEntity[m_NextIndex - 1] = Entity::invalid();
		m_EntityToIndex[lastEntity] = index;
	}
	for (ComponentTypeHandle::Implementation i = 0; i < m_ComponentTypesInThisBucket.size(); ++i)
	{
		if (m_ComponentTypesInThisBucket.test(i))
		{
			m_ComponentStorageRegistry->AccessComponentStorage(ComponentTypeHandle{ i })
				.AccessBucket(m_ArchetypeHandle)
				.DestroyComponent(index);
		}
	}
	m_NextIndex--;
}

void ArchetypeBucket::FetchBuckets()
{
	for (ComponentTypeHandle::Implementation i = 0; i < m_ComponentTypesInThisBucket.size(); ++i)
	{
		if (m_ComponentTypesInThisBucket.test(i))
		{
			m_ComponentStorageRegistry->AccessComponentStorage(ComponentTypeHandle{ i })
				.CreateBucket(m_ArchetypeHandle);
		}
	}
}

void ArchetypeBucket::CreateEntity(Entity entity)
{
	for (ComponentTypeHandle::Implementation i = 0; i < m_ComponentTypesInThisBucket.size(); ++i)
	{
		if (m_ComponentTypesInThisBucket.test(i))
		{
			size_t index = CreateComponent(ComponentTypeHandle{ i });
			assert(index == m_NextIndex);
		}
	}

	if (m_NextIndex >= m_IndexToEntity.size())
		m_IndexToEntity.resize(m_IndexToEntity.size() * 2);

	m_EntityToIndex.emplace(entity, m_NextIndex);
	m_IndexToEntity[m_NextIndex] = entity;

	m_NextIndex++;
}

size_t ArchetypeBucket::CreateComponent(ComponentTypeHandle handle)
{
	return m_ComponentStorageRegistry->AccessComponentStorage(ComponentTypeHandle{ handle })
		.AccessBucket(m_ArchetypeHandle)
		.CreateComponent();
}
