#pragma once

#include <vector>
#include "component_bucket_base.h"

template <typename Component> class ComponentBucket : public ComponentBucketBase
{
public:
	using ComponentType = Component;

	const Component& GetComponent(size_t index) const
	{
		return m_Storage[index];
	}

	Component& AccessComponent(size_t index)
	{
		return const_cast<Component&>(GetComponent(index));
	}

	size_t CreateComponent() override
	{
		m_Storage.emplace_back(Component{});
		return m_Storage.size() - 1;
	}

	void DestroyComponent(size_t index) override
	{
		std::swap(m_Storage[index], m_Storage[m_Storage.size() - 1]);
		m_Storage.pop_back();
	}

	void serialize(SerializerBase& serializer) override
	{
		serializer.serialize("m_Storage", m_Storage);
	}

private:
	std::vector<Component> m_Storage;
};
