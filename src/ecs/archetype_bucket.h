#pragma once

#include <bitset>
#include <unordered_map>

#include "component_storage_registry.h"
#include "component_bucket.h"

class ComponentBucketBase;

class EntityManager;

class ArchetypeBucket
{
public:
	ArchetypeBucket() = default;
	ArchetypeBucket(const ArchetypeMask& componentsInArchetype, ArchetypeHandle archetypeHandle,
		ComponentStorageRegistry* componentStorageRegistry);

	void serialize(SerializerBase& serializer, ComponentStorageRegistry* componentStorageRegistry);

	template <class Component> const Component& GetComponent(size_t index) const
	{
		return GetComponentBucket<Component>().GetComponent(index);
	}

	template <class Component> const Component& GetComponentForEntity(Entity entity) const
	{
		size_t index = LookupIndexForEntity(entity);
		return GetComponent<Component>(index);
	}

	template <class Component> Component& AccessComponent(size_t index)
	{
		return const_cast<Component&>(GetComponent<Component>(index));
	}

	template <class Component> Component& AccessComponentForEntity(Entity entity)
	{
		return const_cast<Component&>(GetComponentForEntity<Component>(entity));
	}

	template <class Component> const ComponentBucket<Component>& GetComponentBucket() const
	{
		ComponentTypeHandle handle = TypeSequence<Component>::index();
		auto& bucket = m_ComponentStorageRegistry->AccessComponentStorage(handle).AccessBucket(m_ArchetypeHandle);
		return static_cast<ComponentBucket<Component>&>(bucket);
	}

	template <class Component> ComponentBucket<Component>& AccessComponentBucket()
	{
		return const_cast<ComponentBucket<Component>&>(GetComponentBucket<Component>());
	}

	template <class Component, std::enable_if_t<!std::is_const<Component>::value, bool> = true>
	ComponentBucket<Component>& GetOrAccessComponentBucket()
	{
		return const_cast<ComponentBucket<Component>&>(GetComponentBucket<Component>());
	}

	template <class Component, std::enable_if_t<std::is_const<Component>::value, bool> = true>
	const ComponentBucket<typename std::remove_const<Component>::type>& GetOrAccessComponentBucket()
	{
		return GetComponentBucket<Component>();
	}

	const ArchetypeMask& GetComponentTypesInThisBucket() const;
	size_t GetSize() const;
	size_t LookupIndexForEntity(Entity entity) const;
	Entity LookupEntityForIndex(size_t index) const;
	ArchetypeHandle GetHandle() const;

private:
	void CreateEntity(Entity entity);
	size_t CreateComponent(ComponentTypeHandle handle);
	void RemoveEntity(Entity entity);
	void FetchBuckets();

	friend EntityManager;

	ArchetypeMask m_ComponentTypesInThisBucket;
	ArchetypeHandle m_ArchetypeHandle;
	size_t m_NextIndex = 0;
	std::unordered_map<Entity, size_t> m_EntityToIndex;
	// Start with some elements then grow exponentially when needed
	std::vector<Entity> m_IndexToEntity{ 8 };
	ComponentStorageRegistry* m_ComponentStorageRegistry = nullptr;
};
