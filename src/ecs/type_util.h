#pragma once
#include "entity.h"

#include <limits>
#include <bitset>
#include <functional>

using ArchetypeMask = std::bitset<100>;

using ArchetypeHandle = Handle<struct tag_ArchetypeHandle, uint32_t, std::numeric_limits<uint32_t>::max()>;

using ComponentTypeHandle = Handle<struct tag_ComponentTypeHandle, uint32_t, std::numeric_limits<uint32_t>::max()>;

class World;
class ArchetypeBucket;

struct ComponentUpdateParameters
{
	ComponentUpdateParameters(World& world, ArchetypeBucket& bucket, size_t start, size_t end);

	World& World;
	ArchetypeBucket& Bucket;
	size_t Start = 0;
	size_t End = 0;
};

struct ArchetypeReadWriteMaskPair
{
	ArchetypeMask ComponentReadMask;
	ArchetypeMask ComponentWriteMask;
};

namespace hidden
{
	struct TypeSequenceBase
	{
		[[nodiscard]] static ComponentTypeHandle next_index()
		{
			static ComponentTypeHandle entity{ 0 };
			ComponentTypeHandle handle = entity;
			ComponentTypeHandle new_val{ static_cast<ComponentTypeHandle::Implementation>(entity) + 1 };
			entity = new_val;
			return handle;
		}
	};
}

template <typename T> struct TypeSequence
{
	[[nodiscard]] static ComponentTypeHandle index()
	{
		static const ComponentTypeHandle val = hidden::TypeSequenceBase::next_index();
		return val;
	}
};

template <typename... Components> ArchetypeMask ArchetypeMaskFromComponents()
{
	ComponentTypeHandle handles[] = { TypeSequence<Components>::index()... };
	ArchetypeMask mask;
	for (ComponentTypeHandle& cth : handles)
	{
		mask.set(cth.get_underlaying_value());
	}
	return mask;
}

template <typename ComponentBucket, std::enable_if_t<std::is_const<ComponentBucket>::value, bool> = true>
void ComponentTypeHandleFromBucketRead(ArchetypeMask& mask)
{
	mask.set(TypeSequence<typename ComponentBucket::ComponentType>::index().get_underlaying_value());
}

template <typename ComponentBucket, std::enable_if_t<!std::is_const<ComponentBucket>::value, bool> = true>
void ComponentTypeHandleFromBucketRead(ArchetypeMask& mask)
{
}

template <typename ComponentBucket, std::enable_if_t<std::is_const<ComponentBucket>::value, bool> = true>
void ComponentTypeHandleFromBucketWrite(ArchetypeMask& mask)
{
}

template <typename ComponentBucket, std::enable_if_t<!std::is_const<ComponentBucket>::value, bool> = true>
void ComponentTypeHandleFromBucketWrite(ArchetypeMask& mask)
{
	mask.set(TypeSequence<typename ComponentBucket::ComponentType>::index().get_underlaying_value());
}

template <typename... ComponentBuckets> ArchetypeMask ArchetypeMaskFromComponentBucketsRead()
{
	ArchetypeMask mask;
	(ComponentTypeHandleFromBucketRead<ComponentBuckets>(mask), ...);
	return mask;
}

template <typename... ComponentBuckets> ArchetypeMask ArchetypeMaskFromComponentBucketsWrite()
{
	ArchetypeMask mask;
	(ComponentTypeHandleFromBucketWrite<ComponentBuckets>(mask), ...);
	return mask;
}

template <typename... ComponentBuckets> ArchetypeMask ArchetypeMaskFromComponentBucketsReadWrite()
{
	return ArchetypeMaskFromComponentBucketsRead<ComponentBuckets...>() |
		   ArchetypeMaskFromComponentBucketsWrite<ComponentBuckets...>();
}

template <typename... ComponentBuckets>
ArchetypeMask ArchetypeMaskFromFunctionWrite(
	std::function<void(ComponentUpdateParameters&, ComponentBuckets&...)> /*func*/)
{
	return ArchetypeMaskFromComponentBucketsWrite<ComponentBuckets...>();
}

template <typename... ComponentBuckets>
ArchetypeMask ArchetypeMaskFromFunctionRead(
	std::function<void(ComponentUpdateParameters&, ComponentBuckets&...)> /*func*/)
{
	return ArchetypeMaskFromComponentBucketsRead<ComponentBuckets...>();
}
