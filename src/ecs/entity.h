#pragma once

#include "handle.h"
#include <limits>
#include <cstdint>

using Entity = Handle<struct entity_tag, uint32_t, std::numeric_limits<uint32_t>::max()>;
