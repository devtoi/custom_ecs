#pragma once

#include "type_util.h"
#include "component_bucket.h"

class ComponentBucketStorageBase
{
public:
	virtual ~ComponentBucketStorageBase()
	{
	}
	virtual void serialize(SerializerBase& serializer) = 0;
	virtual void CreateBucket(ArchetypeHandle archetype) = 0;
	virtual ComponentBucketBase& AccessBucket(ArchetypeHandle archetype) = 0;
};
