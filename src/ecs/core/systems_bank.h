#pragma once

#include <memory>

#include <ecs/system/system.h>
#include <string/string_id.h>

class ComponentStorageRegistry;
class SystemCollection;

class SystemsBank
{
public:
	SystemsBank() = default;
	template <typename SystemType> System* RegisterSystem()
	{
		return RegisterSystem(std::make_unique<SystemType>());
	}
	System* RegisterSystem(std::unique_ptr<System>&& System);

	template <typename T> const System* GetSystem() const
	{
		return GetSystem(T::NAME);
	}
	template <typename T> System* AccessSystem()
	{
		return AccessSystem(T::NAME);
	}
	const System* GetSystem(std::string_view SystemName) const;
	System* AccessSystem(std::string_view SystemName);
	void SetupComponentDependencies(ComponentStorageRegistry& Registry);

	SystemCollection* AddSystemCollectionTemplate(
		std::string_view Name, std::unique_ptr<SystemCollection>&& Collection);
	SystemCollection* AccessSystemCollectionTemplate(std::string_view Name);
	SystemCollection* AccessSystemCollectionTemplate(StringID NameID);

private:
	std::unordered_map<StringID, std::unique_ptr<System>, StringIDHasher> _Systems;
	std::unordered_map<StringID, std::unique_ptr<SystemCollection>, StringIDHasher> _SystemCollectionTemplates;
};
