#include "system_collection.h"

#include "spdlog/spdlog.h"

void SystemCollection::AddSystem(System* pSystem, int Priority, bool IgnoreDuplicates)
{
	StringID ID = pSystem->GetNameID();
	auto Added = _AddedSystems.find(ID);
	if (Added != _AddedSystems.end())
	{
		for (auto& pAddedSystem : _Systems)
		{
			if (pAddedSystem.pSystem->GetNameID() == ID && pAddedSystem.Priority == Priority)
			{
				if (IgnoreDuplicates)
				{
					SPDLOG_ERROR("System {} already added with same priority: {}", pAddedSystem.pSystem->GetName(),
						pAddedSystem.Priority);
					return;
				}
			}
		}
		SPDLOG_DEBUG(
			"System {} already added, but with different priority, this may be fine", pAddedSystem.pSystem->GetName());
	}
	SystemUpdateInfo UpdateInfo;
	UpdateInfo.pSystem = pSystem;
	UpdateInfo.Priority = Priority;
	_Systems.emplace_back(UpdateInfo);
	_AddedSystems.emplace(ID);
}

void SystemCollection::SortSystems()
{
	std::stable_sort(
		_Systems.begin(), _Systems.end(), [](const auto& lhs, const auto& rhs) { return lhs.Priority < rhs.Priority; });
}

void SystemCollection::AddOtherCollection(SystemCollection* pOther)
{
	for (const auto& pOtherEntry : pOther->_Systems)
	{
		AddSystem(pOtherEntry.pSystem, pOtherEntry.Priority, true);
	}
	SortSystems();
}

std::vector<SystemUpdateInfo>& SystemCollection::AccessSystems()
{
	return _Systems;
}
