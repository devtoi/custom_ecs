#pragma once

#include <unordered_set>

#include <ecs/system/system.h>

#include "systems_bank.h"

struct SystemUpdateInfo
{
public:
	int Priority = 0;
	System* pSystem = nullptr;
	bool Initialized = false;
};

class SystemCollection
{
public:
	template <typename SystemType> void AddSystem(SystemsBank& Bank, int Priority = 0, bool IgnoreDuplicates = false)
	{
		auto* pSystem = Bank.AccessSystem<SystemType>();
		if (pSystem)
		{
			AddSystem(pSystem, Priority, IgnoreDuplicates);
		}
		else
		{
			AddSystem(Bank.RegisterSystem<SystemType>(), Priority, IgnoreDuplicates);
		}
	}
	void AddSystem(System* pSystem, int Priority = 0, bool IgnoreDuplicates = false);
	void SortSystems();
	void AddOtherCollection(SystemCollection* pOther);

	std::vector<SystemUpdateInfo>& AccessSystems();

private:
	std::vector<SystemUpdateInfo> _Systems;
	std::unordered_set<StringID, StringIDHasher> _AddedSystems;
};
