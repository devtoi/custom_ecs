#include "systems_bank.h"
#include "spdlog/spdlog.h"
#include <ecs/core/system_collection.h>

System* SystemsBank::RegisterSystem(std::unique_ptr<System>&& System)
{
	auto ID = StringID{ System->GetName() };
	auto it = _Systems.find(ID);
	if (it != _Systems.end())
	{
		SPDLOG_ERROR("Registered duplicate system {} in systems banks", System->GetName());
		return nullptr;
	}
	return _Systems.emplace(ID, std::move(System)).first->second.get();
}

void SystemsBank::SetupComponentDependencies(ComponentStorageRegistry& Registry)
{
	for (auto& Pair : _Systems)
	{
		Pair.second->SetupComponentDependencies(Registry);
	}
}

const System* SystemsBank::GetSystem(std::string_view SystemName) const
{
	auto it = _Systems.find(StringID{ SystemName });
	if (it != _Systems.end())
	{
		return it->second.get();
	}
	return nullptr;
}

System* SystemsBank::AccessSystem(std::string_view SystemName)
{
	return const_cast<System*>(GetSystem(SystemName));
}

SystemCollection* SystemsBank::AddSystemCollectionTemplate(
	std::string_view Name, std::unique_ptr<SystemCollection>&& Collection)
{
	StringID ID{ Name };
	auto Found = _SystemCollectionTemplates.find(ID);
	if (Found != _SystemCollectionTemplates.end())
	{
		SPDLOG_ERROR("Duplicate system collection template {}", Name);
		return nullptr;
	}
	return _SystemCollectionTemplates.emplace(ID, std::move(Collection)).first->second.get();
}

SystemCollection* SystemsBank::AccessSystemCollectionTemplate(std::string_view Name)
{
	StringID ID{ Name };
	return AccessSystemCollectionTemplate(ID);
}

SystemCollection* SystemsBank::AccessSystemCollectionTemplate(StringID NameID)
{
	auto Found = _SystemCollectionTemplates.find(NameID);
	if (Found != _SystemCollectionTemplates.end())
	{
		return Found->second.get();
	}
	return nullptr;
}
