#pragma once

#include <unordered_map>
#include <vector>
#include <cassert>
#include "component_bucket_storage_base.h"
#include "component_bucket.h"

template <typename Component> class ComponentBucketStorage : public ComponentBucketStorageBase
{
public:
	void serialize(SerializerBase& serializer) override
	{
		serializer.serialize("m_ArchetypeToComponents", m_ArchetypeToComponents);
	}

	void CreateBucket(ArchetypeHandle archetype) override
	{
		if (archetype.get_underlaying_value() >= m_ArchetypeToComponents.size())
		{
			m_ArchetypeToComponents.resize(archetype.get_underlaying_value() + 1);
		}
	}

	ComponentBucketBase& AccessBucket(ArchetypeHandle archetype) override
	{
		assert(archetype.get_underlaying_value() < m_ArchetypeToComponents.size());
		return m_ArchetypeToComponents[archetype.get_underlaying_value()];
	}

	std::vector<ComponentBucket<Component>> m_ArchetypeToComponents;
};
