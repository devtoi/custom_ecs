#!/bin/bash
mkdir -p $2
FILES="$1"/*
for f in $FILES
do
filename=$(basename -- "$f")
extension="${filename##*.}"
filename="${filename%.*}"
outfile=$2/$filename.$extension.spv
if  [[ ! (-f "$outfile") || ($f -nt $outfile)]]; then
echo $f is newer than $outfile. Recompiling.
glslc $f -o $outfile
fi
done
